/*
 * Electric Ears Selection System
 * Copyright (C) 1998 - 2000 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include "plugins.h"
#include "metadefs.h"
#include "selection_system.h"
#include "eears.h"

extern EEars *eears;

/**************************************************************************/

gboolean init_plugin (PluginData *pd);

void get_selection(GnomeMDI *mdi, glong *begin, glong *end);
void set_selection(GnomeMDI *mdi, glong begin, glong end);
static void sel_normalize(Selection *sel);


/**************************************************************************
 */

Selection *sel_new(void)
{
	Selection *new;

	new = g_new(Selection, 1);
	new -> selection_begin = -1;
	new -> selection_end = -1;

	new -> low_bound = 0;
	new -> high_bound = 0;

	return new;
}

void sel_free(Selection *sel)
{
	g_assert(sel);

	g_free(sel);
}


void sel_get_selection(Selection *sel, glong *begin, glong *end)
{
	g_assert(sel);
	g_assert(begin);
	g_assert(end);
	
	*begin = sel -> selection_begin; 
	*end = sel -> selection_end; 
}

void sel_set_selection(Selection *sel, glong begin, glong end)
{	
	g_assert(sel);
	sel -> selection_begin = MIN( begin, end); 
	sel -> selection_end = MAX( begin, end);
	
	sel_normalize(sel);
}

void sel_get_bounds(Selection *sel, glong *begin, glong *end)
{
	g_assert(sel);
	g_assert(begin);
	g_assert(end);
	
	*begin = sel -> low_bound; 
	*end = sel -> high_bound; 
}

void sel_set_bounds(Selection *sel, glong begin, glong end)
{	
	g_assert(sel);
	sel -> low_bound = MIN(begin , end); 
	sel -> high_bound = MAX( begin, end);
	
	sel_normalize(sel);
}

void sel_normalize(Selection *sel)
{
	g_assert(sel);

	/* special case: no selection */
	if (sel -> selection_begin == -1 && sel -> selection_end == -1) return;
	
	sel -> selection_begin = MAX( MIN( sel -> selection_begin, sel -> high_bound), 
				      sel -> low_bound);
	sel -> selection_end = MAX( MIN( sel -> selection_end, sel -> high_bound), 
				    sel -> low_bound);
}


/**************************************************************************/

static PluginIdentifier ident[] = { 
	{ 0x00000100, "selection_system"},
	{ 0, NULL}
};

static PluginIdentifier deps[] = { 
	{ 0, NULL}
};

static PluginGate gates[] = {
	{ "new",  sel_new  },
	{ "free",  sel_free  },
	{ "get_selection",  sel_get_selection  },
	{ "set_selection",  sel_set_selection  },
	{ "get_bounds",  sel_get_bounds  },
	{ "set_bounds",  sel_set_bounds  }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Default Selection Manager";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}


