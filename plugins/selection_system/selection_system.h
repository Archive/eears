/* Electric Ears Selection System
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _SELECTION_SYSTEM_H_
#define _SELECTION_SYSTEM_H_

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _Selection     	        Selection;

struct _Selection
{
	/* the selections are now in frame units */
        glong selection_begin;
	glong selection_end;         

	glong low_bound;
	glong high_bound;
};

void sel_set_bounds             (Selection *sel, glong begin, glong end);

void sel_get_bounds             (Selection *sel, glong *begin, glong *end);

void sel_set_selection          (Selection *sel, glong begin, glong end);

void sel_get_selection          (Selection *sel, glong *begin, glong *end);

Selection *sel_new              (void);

void sel_free                   (Selection *);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SELECTION_SYSTEM_H_ */
