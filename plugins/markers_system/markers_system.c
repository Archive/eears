/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"
#include "markers_system.h"

gboolean init_plugin (PluginData *pd);


static gboolean cut_func(gpointer key, gpointer	value, gpointer	user_data);
static void insert_func(gpointer key, gpointer value, gpointer range);
static void get_func(gpointer key, gpointer value, gpointer range);

typedef struct _marker marker;

struct _marker {
	gchar *name;
	glong pos;
};

/**************************************************************************/

gboolean mar_set_marker(Markers *mar, gchar *name, glong pos)
{
	marker *m;
	g_assert(mar);
	g_assert(name);
	
	/* simply change value */
	m = (marker*)g_hash_table_lookup(mar->hash_table, name);
	if (m) {
		m->pos = pos;
		return TRUE;
	}

	/* this is reserved for unique markers */
	if (name[0]=='<') return FALSE;

	m = g_new(marker,1);
	
	m->name = g_strdup(name);
	m->pos = pos;
	
	g_hash_table_insert(mar->hash_table, m->name, m);

	return TRUE;
}


glong mar_get_marker(Markers *mar, gchar *name)
{
	marker *m;
	g_assert(mar);
	g_assert(name);

	m = (marker*)g_hash_table_lookup(mar->hash_table, name);

	/* does not exist */
	if (m == NULL) return -1;
	
	return m->pos;
}

const gchar* mar_set_unique_marker(Markers *mar, glong pos)
{
	gchar name[20];
	marker *m;
	g_assert(mar);

	snprintf(name,20,"<%d>", mar->unique_id++);

	m = (marker*)g_hash_table_lookup(mar->hash_table, name);
	/* free marker */
	if (m) {
		g_hash_table_remove(mar->hash_table, name);
		g_free(m->name);
		g_free(m);
	}

	m = g_new(marker,1);
	
	m->name = g_strdup(name);
	m->pos = pos;
	
	g_hash_table_insert(mar->hash_table, m->name, m);

	return m->name;
}

void  mar_remove_marker(Markers *mar, gchar *name)
{
	marker *m;
	g_assert(mar);
	g_assert(name);

	m = (marker*)g_hash_table_lookup(mar->hash_table, name);

	if (m) {
		g_hash_table_remove(mar->hash_table, name);
		g_free(m->name);
		g_free(m);
	}
}

gboolean cut_func(gpointer key, gpointer value, gpointer range)
{
	glong begin,end;
	marker *m;
	
	begin = ((glong*)range)[0];
	end = ((glong*)range)[1];

	m = (marker*)value;

	if ( m->pos >= begin ) {
		if ( strlen(m->name) == 0 ) {
			/* do not destroy the insertion point */
			m->pos = 0; 
			return FALSE;
		}
		if (m->pos <= end )
			/* in range: eliminate */
			return TRUE;
		else {
			/* beyond range: move back */
			m->pos -= (end - begin + 1);
		}
	}
	return FALSE;
}

void mar_cut_range(Markers *mar, glong begin, glong end)
{
	glong range[2];

	g_assert(mar);

	range[0]= begin;
	range[1]= end;
	
	g_hash_table_foreach_remove(mar->hash_table, cut_func, range);
}

void insert_func(gpointer key, gpointer value, gpointer range)
{
	glong pos, size;
	marker *m;
	
	pos = ((glong*)range)[0];
	size = ((glong*)range)[1];

	m = (marker*)value;

#ifdef DEBUG
	g_print("%d %d %s %d\n", pos, size , m->name, m->pos);
#endif

	if ( m->pos >= pos ) m->pos += size;
}

void mar_insert_range(Markers *mar, glong pos, glong size)
{
	glong range[2];

	g_assert(mar);

	range[0]= pos;
	range[1]= size;
	
	g_hash_table_foreach(mar->hash_table, insert_func, range);
}

void get_func(gpointer key, gpointer value, gpointer data)
{
	glong begin, end;
	GList *marker_list;
	marker *m;
	
        begin = ((glong*)((gpointer*)data)[0])[0];
	end = ((glong*)((gpointer*)data)[0])[1];
	marker_list = (GList*)((gpointer*)data)[1];
	
	m = (marker*)value;

	if ( m->pos >= begin && m->pos <= end ) 
		marker_list = g_list_append(marker_list, m->name);

	((gpointer*)data)[1] = marker_list;
}


GList* mar_get_markers_in_range(Markers *mar, glong begin, glong end)
{
	GList *marker_list;
	gpointer data[2];
	glong range[2];

	g_assert(mar);

	marker_list = NULL;

	range[0] = begin;
	range[1] = end;
	data[0] = range;
	data[1] = marker_list;
	
	g_hash_table_foreach(mar->hash_table, get_func, data);

	return (GList*)data[1];
}

Markers *mar_new(void)
{
	Markers* new;
	
	new = g_new(Markers, 1);
	new->hash_table = g_hash_table_new(g_str_hash, g_str_equal);
	new->unique_id = 0; 
	
	return new;
}

void mar_free(Markers *mar)
{
	g_assert(mar);
	
	g_hash_table_destroy( mar->hash_table);
	g_free(mar);
}


/**************************************************************************/

static PluginIdentifier ident[] = { 
	{ 0x00000100, "markers_system" },
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{0, NULL}
};

static PluginGate gates[] = {
	{ "set_marker", mar_set_marker },
	{ "get_marker", mar_get_marker },
	{ "set_unique_marker", mar_set_unique_marker },
	{ "remove_marker", mar_remove_marker },
	{ "cut_range", mar_cut_range },
	{ "insert_range",  mar_insert_range },
	{ "get_markers_in_range", mar_get_markers_in_range },
	{ "new", mar_new },
	{ "free",  mar_free }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Markers System";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}



