/* Electric Ears Selection System
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _MARKERS_SYSTEM_H_
#define _MARKERS_SYSTEM_H_

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _Markers     	        Markers;

struct _Markers
{
	GHashTable* hash_table;

	/* this is an incrementing id for the generation of new markers */
	gint unique_id;

};


gboolean mar_set_marker                 (Markers *mar, gchar *name, glong pos);

glong mar_get_marker                    (Markers *mar, gchar *name);

const gchar* mar_set_unique_marker      (Markers *mar, glong pos);

void  mar_remove_marker                     (Markers *mar, gchar *name);

void mar_cut_range                          (Markers *mar, glong begin, glong end);
void mar_insert_range                       (Markers *mar, glong pos, glong size);

GList* mar_get_markers_in_range             (Markers *mar, glong begin, glong end);

Markers *mar_new                        (void);

void mar_free                           (Markers *);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MARKERS_SYSTEM_H_ */
