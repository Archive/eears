
#include <config.h>
#include <gnome.h>
#include "plugins.h"
#include "metadefs.h"
#include "audio_fileselection.h"

#include <sys/types.h>
#include <dirent.h>

gboolean 	init_plugin 		(PluginData *pd);
void 		file_selection		(void(*process_file)(gchar*, gint));
void		call_process_file 	(GtkWidget *file_selection, gpointer);

/* void		call_process_file 	(GtkWidget *widget, GtkWidget *file_selection); */
/* gboolean 	is_dir 			(const gchar * filename); */

/* gboolean  */
/* is_dir (const gchar * filename) { */
/* 	DIR *d; */
/* 	if ((d = opendir(filename)) == NULL) { */
/* 		return FALSE; */
/* 	} else { */
/* 		closedir(d); */
/* 		return TRUE; */
/* 	} */
/* } */

void
call_process_file (GtkWidget *file_selection, gpointer p_f)
{
/* <<<<<<< file_selection.c */
	void (*process_file)(gchar*, gint) = p_f;

	process_file(gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selection)), 
		     audio_file_selection_get_format(AUDIO_FILE_SELECTION(file_selection)));

/* ======= */

/* 	if (!(is_dir(gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selection))))) { */
/* 	void (*process_file)(gchar*, gint) = gtk_object_get_data(GTK_OBJECT(file_selection), "process_file"); */
/* 	process_file(gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selection)), audio_file_selection_get_format(AUDIO_FILE_SELECTION(file_selection))); */
/* 	} else { */
/* 		gnome_error_dialog(_("Unable to open a directory. You have to specify a valid filename.")); */
/* 	} */
/* >>>>>>> 1.3 */
	return;
}

void
file_selection	(void (*process_file)(gchar*, gint))
{
	GtkWidget *file_selection;

	file_selection = audio_file_selection_new(_("Select a file"));
	gtk_window_set_modal(file_selection, TRUE);
	gtk_widget_show(file_selection);

	
	gtk_signal_connect (GTK_OBJECT(file_selection),
			    "file_selected",
			    GTK_SIGNAL_FUNC(call_process_file),
			    (gpointer) process_file);

	return;
}

static PluginIdentifier ident[] = { 
	{ 0x00000100, "file_selection"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{0, NULL}
};

static PluginGate gates[] = {
	{ "file_selection", file_selection }
};

gboolean 
init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Default File Selection";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}



