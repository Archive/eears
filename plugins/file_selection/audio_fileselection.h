/* Electric Ears File Selection module
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __AUDIO_FILESEL_H__
#define __AUDIO_FILESEL_H__

#include <gdk/gdk.h>
#include <gtk/gtkfilesel.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_AUDIO_FILE_SELECTION              (audio_file_selection_get_type ())
#define AUDIO_FILE_SELECTION(obj)              (GTK_CHECK_CAST ((obj), TYPE_AUDIO_FILE_SELECTION, AudioFileSelection))
#define AUDIO_FILE_SELECTION_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), TYPE_AUDIO_FILE_SELECTION, AudioFileSelectionClass))
#define IS_AUDIO_FILE_SELECTION(obj)           (GTK_CHECK_TYPE ((obj), TYPE_AUDIO_FILE_SELECTION))
#define IS_AUDIO_FILE_SELECTION_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), TYPE_AUDIO_FILE_SELECTION))

typedef struct _AudioFileSelection       AudioFileSelection;
typedef struct _AudioFileSelectionClass  AudioFileSelectionClass;

struct _AudioFileSelection
{
	GtkFileSelection fileselection;
        gint file_format;
};

struct _AudioFileSelectionClass
{
	GtkFileSelectionClass parent_class;
	void (* file_selected)  (AudioFileSelection *afs);

};

GtkType    audio_file_selection_get_type            (void);
GtkWidget* audio_file_selection_new                 (const gchar      *title);
/*
void       audio_file_selection_set_filename        (AudioFileSelection *filesel,
                                                   const gchar      *filename); 
gchar*     audio_file_selection_get_filename        (AudioFileSelection *filesel);
*/

gint audio_file_selection_get_format(AudioFileSelection  *filesel);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AUDIO_FILESEL_H__ */



