/* Electric Ears File Selection module
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gnome.h>
#include <audiofile.h>
#include "audio_fileselection.h"
#include <unistd.h>
#include <sys/stat.h>

static void audio_file_selection_class_init (AudioFileSelectionClass *klass);
static void audio_file_selection_init(AudioFileSelection *fileselection);

void file_sel_play(GtkWidget *w, GtkWidget* file_sel);
void change_directory (GtkWidget *widget, GtkWidget *file_selection);
void file_set_format (GtkWidget *widget, gpointer data);

/* New signals */

enum {
  FILE_SELECTED,
  LAST_SIGNAL
};

static guint afs_signals[LAST_SIGNAL] = { 0 };




GtkType 
audio_file_selection_get_type (void)
{
	static GtkType audio_file_selection_type = 0;

	if (!audio_file_selection_type)
	{
		static const GtkTypeInfo audio_file_selection_info =
		{
			"AudioFileSelection",
			sizeof (AudioFileSelection),
			sizeof (AudioFileSelectionClass),
			(GtkClassInitFunc) audio_file_selection_class_init,
			(GtkObjectInitFunc) audio_file_selection_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		audio_file_selection_type = gtk_type_unique (gtk_file_selection_get_type (), &audio_file_selection_info);

	}
	return audio_file_selection_type;
}

static void
audio_file_selection_class_init (AudioFileSelectionClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) klass;
	widget_class = (GtkWidgetClass*) klass;

        afs_signals[FILE_SELECTED] =
        gtk_signal_new ("file_selected",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (AudioFileSelectionClass, file_selected),
			gtk_marshal_NONE__NONE,
			GTK_TYPE_NONE, 0);

        gtk_object_class_add_signals (object_class, afs_signals, LAST_SIGNAL);

	klass -> file_selected = NULL;

}

void file_sel_play(GtkWidget *w, GtkWidget* file_sel)
{
        gchar *filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_sel));
        gnome_sound_init(NULL);
        gnome_sound_play(filename);
        gnome_sound_shutdown();
}

/* void normalize_filename( gchar *fn) */
/* { */
/* 	gchar *d; */
/* 	typedef enum { dot1, dot2, ex, norm} norm_mode ;  */
/* 	norm_mode m; */

/* 	if (!fn) return; */
/* 	m = norm; */
/* 	d = fn; */
/* 	while (*fn) { */
/* 		if (*fn == '.') { */
/* 			if (m == norm) m = dot1; */
/* 		        else if ( m == dot1) m=dot2; */
/* 			else if ( m == dot2) m=ex; */
/* 		} */
/* 		if (*fn == '/') { */
/* 			if (m == dot1) d-=2; */
/* 			else if ( m== dot2) { */
/* 				if (d - 3 >= fn) { */
/* 					d -= 3; *d = '\0'; */
/* 					if (! (d = rindex(fn, '/'))) { */
/* 						*fn = '\0'; */
/* 						d = fn;  */
/* 					} */
/* 				} */
/* 			} */
/* 		} */
/* 		*d++ = * (fn ++); */
/* 	} */
/* } */

void change_directory (GtkWidget *widget, GtkWidget *file_selection)
{
        gchar *filename, *dirname, *basename, *destname;
	struct stat st;

        filename = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selection)));
        dirname = g_dirname (filename);
	basename = g_basename (filename);
	destname = filename;
/* 	g_print("bn %s-\ndn %s-\nfn %s-", basename, dirname, filename); */

	stat(filename, &st);
	if (S_ISDIR(st.st_mode)) {
		if ( filename[strlen(filename) -1 ] != '/')
			destname = g_strconcat(filename, "/", NULL);
		else 
			destname = g_strdup( filename );

/* 		normalize_filename(destname); */
		gtk_file_selection_set_filename(GTK_FILE_SELECTION(file_selection), 
						destname);
		g_free(destname);
	}
	else 
		gtk_signal_emit_by_name(GTK_OBJECT(file_selection), "file_selected", NULL);

/* 	g_print("bn %s-\ndn %s-\nfn %s-", basename, dirname, filename); */
	

/* 	g_print(" dest %s\n", destname); */
	chdir(filename); /* unistd stuff */
        g_free(filename);
}

void file_set_format (GtkWidget *widget, gpointer data)
{
        g_assert(widget);
	g_assert(data);

        AUDIO_FILE_SELECTION(data)->file_format = 
	  (gint) gtk_object_get_data(GTK_OBJECT(widget),"AF_FILE");
}

gint audio_file_selection_get_format(AudioFileSelection *filesel)
{
        gint format;
	gchar *filename;
	static gchar *ext_table[] = { "wav", "aiff", "aiffc", "au", NULL };
	static gint format_table[] = { AF_FILE_WAVE, AF_FILE_AIFF, AF_FILE_AIFFC, 
				       AF_FILE_NEXTSND, AF_FILE_UNKNOWN };

	g_assert(filesel);
	format = filesel->file_format;
	
/* 	g_print("format: %d",format); */

	/* try to identify format from the suffix */
	if (format == AF_FILE_UNKNOWN) {
	  int i;
	  filename = gtk_file_selection_get_filename(GTK_FILE_SELECTION(filesel));
	  g_assert(filename);
	  for (i=0; ext_table[i] != NULL; i++)
	    if (strlen(filename) >= strlen(ext_table[i]))
	      if (g_strcasecmp(ext_table[i], filename + strlen(filename) - strlen(ext_table[i])) == 0) {
		format = format_table[i]; break; 
	      }
	}
/* 	g_print("format: %d",format); */
  
	return format;
}

static void
audio_file_selection_init (AudioFileSelection *fileselection)
{
	GtkWidget *play_button;
	GtkWidget *opt, *menu, *item;

	play_button = gtk_button_new_with_label(_("Play"));
	gtk_box_pack_start(
		     GTK_BOX(GTK_FILE_SELECTION(fileselection)->action_area),
			     play_button, TRUE, TRUE, 5);
	gtk_widget_show(play_button);

	gtk_signal_connect(GTK_OBJECT(play_button), "clicked",
			GTK_SIGNAL_FUNC(file_sel_play), fileselection);

	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (fileselection));

	/* fileformat select */
	opt = gtk_option_menu_new();
	menu = gtk_menu_new();

	item = gtk_menu_item_new_with_label (_("Automatic from suffix"));
	gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(file_set_format), (gpointer)fileselection);
	gtk_object_set_data(GTK_OBJECT(item), "AF_FILE", (gpointer)AF_FILE_UNKNOWN);
	gtk_menu_append(GTK_MENU(menu), item);

	item = gtk_menu_item_new_with_label (_("RIFF (*.wav)"));
	gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(file_set_format), (gpointer)fileselection);
	gtk_object_set_data(GTK_OBJECT(item), "AF_FILE", (gpointer)AF_FILE_WAVE);
	gtk_menu_append(GTK_MENU(menu), item);

	item = gtk_menu_item_new_with_label (_("NeXT/Sun (*.au)"));
	gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(file_set_format), (gpointer)fileselection);
	gtk_object_set_data(GTK_OBJECT(item), "AF_FILE", (gpointer)AF_FILE_NEXTSND);
	gtk_menu_append(GTK_MENU(menu), item);

	item = gtk_menu_item_new_with_label (_("AIFF (*.aiff)"));
	gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(file_set_format), (gpointer)fileselection);
	gtk_object_set_data(GTK_OBJECT(item), "AF_FILE", (gpointer)AF_FILE_AIFF);
	gtk_menu_append(GTK_MENU(menu), item);

	item = gtk_menu_item_new_with_label (_("AIFFC (*.aiffc)"));
	gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(file_set_format), (gpointer)fileselection);
	gtk_object_set_data(GTK_OBJECT(item), "AF_FILE", (gpointer)AF_FILE_AIFFC);
	gtk_menu_append(GTK_MENU(menu), item);

	gtk_option_menu_set_menu(GTK_OPTION_MENU(opt), menu);

	gtk_box_pack_start(
		     GTK_BOX(GTK_FILE_SELECTION(fileselection)->action_area),
			     opt, TRUE, TRUE, 5);
	gtk_widget_show_all(opt);
	
	

	gtk_signal_connect_after(
		GTK_OBJECT(fileselection),
		"file_selected", GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);
	gtk_signal_connect_object_after(
		GTK_OBJECT(GTK_FILE_SELECTION(fileselection)->cancel_button),
		"clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
		GTK_OBJECT(fileselection));

        gtk_signal_connect (
                GTK_OBJECT(GTK_FILE_SELECTION(fileselection)->ok_button),
                "clicked",
                GTK_SIGNAL_FUNC(change_directory),
                fileselection);

	fileselection->file_format = AF_FILE_UNKNOWN;
}

GtkWidget*
audio_file_selection_new (const gchar *title)
{
	AudioFileSelection *audio_file_selection;

	audio_file_selection = gtk_type_new(audio_file_selection_get_type());
	return GTK_WIDGET(audio_file_selection);
}


