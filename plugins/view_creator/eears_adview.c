/* Electric Ears
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#undef REAL_USE_GTHREADS

#include <config.h>
#include <gnome.h>
#include "metadefs.h"
#include "eears.h"
#include "eears_adview.h"

#define EEADV_DEFAULT_HEIGHT 400
#define EEADV_DEFAULT_WIDTH 640
#define EEADV_READ_FILE_BUFFER_SIZE (2<<14)
#define EEADV_MAX_LARGE_CACHE_SIZE 214748

#define pixel2frame(a) ((glong)((a)*eeadv->mult + eeadv->base_frame))
#define frame2pixel(a) ((glong)(((a) - eeadv->base_frame)/eeadv->mult))
#define pixel2frame_ratio(a) ((glong)((a)*eeadv->mult))
#define frame2pixel_ratio(a) ((glong)((a)/eeadv->mult))


/* Forward declarations */

static void	eears_adview_class_init		(EEarsAdViewClass    *klass);
static void 	eears_adview_init		(EEarsAdView     *eeadv);
static void 	eears_adview_destroy		(GtkObject       *object);
static void 	eears_adview_realize		(GtkWidget       *widget);
static void	eears_adview_size_request	(GtkWidget       *widget,
					       		GtkRequisition *requisition);
static void 	eears_adview_size_allocate	(GtkWidget     *widget,
							GtkAllocation *allocation);

/*static gint 	eears_adview_update_playline 	(gpointer data);*/
static void     eeadv_bg_build_large_cache_stop (EEarsAdView *eeadv);
static void 	eeadv_bg_build_large_cache	(EEarsAdView *eeadv);
static guint    gtk_statusbar_push_perc         (GtkStatusbar *s,
						 guint ctx, gchar *str, gint perc);



static void 	eeadv_build_large_cache		(EEarsAdView *eeadv);

static void 	eeadv_build_fast_cache		(EEarsAdView *eeadv, 
							MetaAudioData mad, 
							glong base, 
							gdouble zoom);
static void 	eeadv_display_from_cache	(EEarsAdView *eeadv,
							GdkWindow *pixmap, 
							MetaAudioData mad, 
							gint w, 
							gint h);
gint 		eears_adview_motion_notify 	(GtkWidget *widget, 
							GdkEventMotion *event);
gint		eears_adview_button_press 	(GtkWidget *widget, 
							GdkEventButton *event);
gint		eears_adview_button_release 	(GtkWidget *widget, 
							GdkEventButton *event);
gint		eears_adview_key_release 	(GtkWidget *widget, 
							GdkEventKey *event);
gint		eears_adview_key_press  	(GtkWidget *widget, 
							GdkEventKey *event);
void		eeadv_reset_cache		(EEarsAdViewCache *cache);
void            eeadv_init_cache                (EEarsAdViewCache *cache, 
						 gulong size, gdouble zoom);
void            eeadv_resize_cache              (EEarsAdViewCache *cache, 
						 gulong size, gdouble zoom);
static gint     eears_adview_focus_in           (GtkWidget     *widget,
						 GdkEventFocus *event);
static gint     eears_adview_focus_out          (GtkWidget     *widget,
						 GdkEventFocus *event);
void		eears_adview_update_insertion_point  	(EEarsAdView * eeadv, 
							glong point);
void		eears_adview_set_marker  	(EEarsAdView * eeadv, 
						 glong point);
void            eears_adview_paint_markers      (EEarsAdView *eeadv);

/* Local data */

static GtkWidgetClass *parent_class = NULL;

/* New signals */

enum {
  CURSOR_FRAME_CHANGED,
  INSERTION_POINT_CHANGED,
  LAST_SIGNAL
};

static guint eeadv_signals[LAST_SIGNAL] = { 0, 0 };


/* other declarations */

extern EEars *eears;

guint
eears_adview_get_type ()
{
        static guint adview_type = 0;

	if (!adview_type) {
		GtkTypeInfo adview_info =
		{
			"EEarsAdView",
			sizeof (EEarsAdView),
			sizeof (EEarsAdViewClass),
			(GtkClassInitFunc) eears_adview_class_init,
			(GtkObjectInitFunc) eears_adview_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
	  
		adview_type = gtk_type_unique (gtk_widget_get_type (), 
					       &adview_info);
	}

	return adview_type;
}

static void
eears_adview_class_init (EEarsAdViewClass *class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) class;
	widget_class = (GtkWidgetClass*) class;

	parent_class = gtk_type_class (gtk_widget_get_type ());

	object_class->destroy = eears_adview_destroy;

	/* create the new signals to notify the movement of the cursor */
	
        eeadv_signals[CURSOR_FRAME_CHANGED] =
        gtk_signal_new ("cursor_frame_changed",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (EEarsAdViewClass, cursor_frame_changed),
			gtk_marshal_NONE__NONE,
			GTK_TYPE_NONE, 0);
        eeadv_signals[INSERTION_POINT_CHANGED] =
        gtk_signal_new ("insertion_point_changed",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (EEarsAdViewClass, insertion_point_changed),
			gtk_marshal_NONE__NONE,
			GTK_TYPE_NONE, 0);

        gtk_object_class_add_signals (object_class, eeadv_signals, LAST_SIGNAL);

	widget_class->realize = eears_adview_realize;
	widget_class->expose_event = eears_adview_expose;
	widget_class->size_request = eears_adview_size_request;
	widget_class->size_allocate = eears_adview_size_allocate;

	widget_class->button_press_event = eears_adview_button_press;
	widget_class->button_release_event = eears_adview_button_release; 
	widget_class->motion_notify_event = eears_adview_motion_notify; 
	widget_class->key_release_event = eears_adview_key_release;
	widget_class->key_press_event = eears_adview_key_press;
	widget_class->focus_in_event = eears_adview_focus_in;
	widget_class->focus_out_event = eears_adview_focus_out;
	class->cursor_frame_changed = NULL;
	class->insertion_point_changed = NULL;
}

static void
eears_adview_init (EEarsAdView *eeadv)
{
	GdkColor c;
	
	eeadv->mad = NULL;
	eeadv->view = NULL;
	eeadv->last_update = g_timer_new();

	c.red   = 0x0000;
	c.green = 0x0000;
	c.blue  = 0xffff;
	eeadv->single_channel_color = gdk_color_copy(&c);

	c.red   = 0x0000;
	c.green = 0x0000;
	c.blue  = 0xffff;
	eeadv->right_channel_color = gdk_color_copy(&c);

	c.red   = 0xffff;
	c.green = 0x0000;
	c.blue  = 0x0000;
	eeadv->left_channel_color = gdk_color_copy(&c);

	c.red   = 0x0000;
	c.green = 0x0000;
	c.blue  = 0x0000;
	eeadv->insertion_point_color = gdk_color_copy(&c);

	c.red   = 0x0000;
	c.green = 0xffff;
	c.blue  = 0x0000;
	eeadv->marker_color = gdk_color_copy(&c);

	eeadv->marker_font = gdk_font_load(DEFAULT_MARKER_FONTNAME);
	
	eeadv->markers_in_view = NULL;

	eeadv->selection_begin = -1.0;
	eeadv->selection_end = -1.0;
	eeadv->base_frame = 0; 
	eeadv->cursor_frame = 0; 
	eeadv->insertion_point = -1; 
	eeadv->zoom = 1;
	eeadv->mult = 1;
	eeadv->force_bitmap_redraw = FALSE;

	eeadv->cursor_mode = CM_NORMAL;
	eeadv->cursors[CM_NORMAL] = gdk_cursor_new(GDK_CROSSHAIR);
	eeadv->cursors[CM_SELECTING] = gdk_cursor_new(GDK_SB_H_DOUBLE_ARROW);
	eeadv->cursors[CM_OVER_MARKER] = gdk_cursor_new(GDK_HAND2);

	
	GTK_WIDGET_SET_FLAGS(eeadv, GTK_CAN_FOCUS| GTK_RECEIVES_DEFAULT);
	GTK_WIDGET_UNSET_FLAGS (eeadv, GTK_NO_WINDOW);

	//eeadv->menuPopup = create_menuPopup((gpointer)eeadv);
#ifndef REAL_USE_GTHREADS
	pthread_mutex_init(&eeadv->large_cache_mutex, NULL);
#else
#endif
}


/* the following two routines handle the focus activities of the widget */

static gint
eears_adview_focus_in (GtkWidget     *widget,
		     GdkEventFocus *event)
{
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (IS_EEARS_ADVIEW (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);
	gtk_widget_draw_focus (widget);
	
/*   g_print("focus in\n"); */
	return FALSE;
}

static gint
eears_adview_focus_out (GtkWidget     *widget,
		      GdkEventFocus *event)
{
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (IS_EEARS_ADVIEW (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);
	
	GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);
	gtk_widget_draw_focus (widget);
	
	return FALSE;
}


gint	
eears_adview_button_press (GtkWidget *widget, GdkEventButton *event)
{
	EEarsAdView *eeadv;
	glong tmp;
	GdkGC *gc;
	GdkPixmap *pixmap;

	g_assert(widget);
	g_assert(event);
	g_assert(IS_EEARS_ADVIEW(widget));
	
	eeadv = EEARS_ADVIEW(widget);
	if (event->type == GDK_BUTTON_PRESS) {
	        if (!GTK_WIDGET_HAS_FOCUS (widget)) gtk_widget_grab_focus (widget);  
	}

	if (event->state & GDK_SHIFT_MASK) {
		if (event->button == 1)
			eears_adview_update_insertion_point(eeadv, 
							    pixel2frame(((GdkEvent *)event)->motion.x ));
		if (event->button == 2)
			eears_adview_set_marker(eeadv, 
						 pixel2frame(((GdkEvent *)event)->motion.x ));
	} else /* non SHIFT */ {
		if (event->button == 1)
		{
			eeadv->cursor_mode = CM_SELECTING;
			gdk_window_set_cursor(widget->window, eeadv->cursors[eeadv->cursor_mode]);
			
			eeadv->selection_begin = pixel2frame( ((GdkEvent *)event)->motion.x );
			eeadv->selection_end = pixel2frame( ((GdkEvent *)event)->motion.x );
			/* g_print("%f, %f\n", ((GdkEvent *)event)->motion.x, ((GdkEvent *)event)->motion.y); */

			eears->ssm->set_selection( eeadv->ms, eeadv->selection_begin, eeadv->selection_end);

		} else if (event->button == 2){
			eeadv->cursor_mode = CM_SELECTING;
			gdk_window_set_cursor(widget->window, eeadv->cursors[eeadv->cursor_mode]);
			tmp = pixel2frame(((GdkEvent *)event)->motion.x);
			if (tmp < eeadv->selection_begin) {
				eeadv->selection_begin = eeadv->selection_end;
				eeadv->selection_end = tmp;
			} else if (tmp > eeadv->selection_end) {
				eeadv->selection_end = tmp;
			} else if ((tmp - eeadv->selection_begin) > (eeadv->selection_end - tmp)) {
			
				eeadv->selection_end = tmp;
			} else {
				eeadv->selection_begin = eeadv->selection_end;
				eeadv->selection_end = tmp;
			}
			eears->ssm->set_selection( eeadv->ms, eeadv->selection_begin, eeadv->selection_end);

		} else if (event->button == 3) {
			eeadv->selection_begin = -1;
			eeadv->selection_end = -1;
		}

		if ((event->button == 1) || 
		    (event->button == 2) || 
		    (event->button == 3))
		{
			pixmap = gdk_pixmap_new (widget->window, eeadv->view_width, eeadv->view_height, -1);
			gc = gdk_gc_new(pixmap);
			/*	
				gdk_draw_rectangle (pixmap, widget->style->bg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, eeadv->view_width, eeadv->view_height); 
			*/
			gdk_draw_pixmap (pixmap,
					 widget->style->fg_gc[GTK_STATE_NORMAL],
					 eeadv->view,
					 0, 0, 0, 0, eeadv->view_width, eeadv->view_height);

			/* check if some part of the selection is visible */
			if (MIN(eeadv->selection_begin, eeadv->selection_end) <= eeadv->base_frame + pixel2frame(eeadv->view_width) && 
			    MAX(eeadv->selection_begin, eeadv->selection_end) >= eeadv->base_frame) {
				glong start, size;

				start = MAX(frame2pixel(MIN(eeadv->selection_begin, eeadv->selection_end)),0);
				size = MAX(frame2pixel_ratio(MAX(eeadv->selection_end, eeadv->selection_begin) - MAX(MIN(eeadv->selection_begin, eeadv->selection_end), eeadv->base_frame)), 1);
				/*g_print("start = %ld size = %ld", start, size);*/
				gdk_draw_pixmap (pixmap,
						 widget->style->fg_gc[GTK_STATE_SELECTED],
						 eeadv->view_selected,
						 start,
						 0,
						 start,
						 0,
						 size,
						 eeadv->view_height);
			}
			gdk_draw_pixmap (widget->window,
					 widget->style->fg_gc[GTK_STATE_NORMAL],
					 pixmap,
					 0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
			gdk_gc_unref(gc);
			gdk_pixmap_unref(pixmap);

			eears_adview_paint_markers(eeadv);
		}
	}
	return FALSE;
}

gint
eears_adview_button_release (GtkWidget *widget, GdkEventButton *event)
{
	EEarsAdView *eeadv;
	glong tmp_max, tmp_min;
	GdkGC *gc;
	GdkPixmap *pixmap;

	g_assert(widget);
	g_assert(event);
	g_assert(IS_EEARS_ADVIEW(widget));

	eeadv = EEARS_ADVIEW(widget);

	if (eeadv->cursor_mode == CM_SELECTING)
	{
		eeadv->cursor_mode = CM_NORMAL;
		gdk_window_set_cursor(widget->window, eeadv->cursors[eeadv->cursor_mode]);
		eeadv->selection_end = pixel2frame(((GdkEvent *)event)->motion.x);

		if (eeadv->selection_end < 0) eeadv->selection_end = 0;
		if (eeadv->selection_begin < 0) eeadv->selection_begin = 0;
		if (eeadv->selection_end > eears->adm->get_framecount(eeadv->mad)) eeadv->selection_end = eears->adm->get_framecount(eeadv->mad);
		if (eeadv->selection_begin > eears->adm->get_framecount(eeadv->mad)) eeadv->selection_begin = eears->adm->get_framecount(eeadv->mad);

		tmp_max = MAX(eeadv->selection_end, eeadv->selection_begin);
		tmp_min = MIN(eeadv->selection_end, eeadv->selection_begin);

		eeadv->selection_end = tmp_max;
		eeadv->selection_begin = tmp_min;

		eears->ssm->set_selection( eeadv->ms, eeadv->selection_begin, eeadv->selection_end);
		
		pixmap = gdk_pixmap_new (widget->window, eeadv->view_width, eeadv->view_height, -1);
		gc = gdk_gc_new(pixmap);
	/*	
		gdk_draw_rectangle (pixmap, widget->style->bg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, eeadv->view_width, eeadv->view_height); 
	*/
		gdk_draw_pixmap (pixmap,
				widget->style->fg_gc[GTK_STATE_NORMAL],
				eeadv->view,
				0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
		if (tmp_min <= eeadv->base_frame + pixel2frame(eeadv->view_width) &&
		    tmp_max >= eeadv->base_frame) {
		        gint start, size;

			start = MAX(frame2pixel(tmp_min),0);
			size = MAX(frame2pixel_ratio(tmp_max - MAX(tmp_min, eeadv->base_frame)), 1);
			gdk_draw_pixmap (pixmap,
					 widget->style->fg_gc[GTK_STATE_SELECTED],
					 eeadv->view_selected,
					 start,
					 0,
					 start,
					 0,
					 size,
					 eeadv->view_height);
		}
		gdk_draw_pixmap (widget->window,
				widget->style->fg_gc[GTK_STATE_NORMAL],
				pixmap,
				0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
		gdk_gc_unref(gc);
		gdk_pixmap_unref(pixmap);

		eears_adview_paint_markers(eeadv);
		/* g_print("%f, %f\n", eeadv->selection_begin, eeadv->selection_end); */
/*		g_print("%d\n", (glong)((eeadv->selection_begin * (double) eears->adm->get_framecount(eeadv->mad)) / (double) eeadv->view_width)); */
	}
	return FALSE;
}

gint
eears_adview_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
	EEarsAdView *eeadv;
	glong tmp_max, tmp_min;
	GdkGC *gc;
	GdkPixmap *pixmap;

	g_assert(widget);
	g_assert(event);
	g_assert(IS_EEARS_ADVIEW(widget));

	eeadv = EEARS_ADVIEW(widget);

	/* update current cursor frame, and confine in sample bounds */
	eeadv->cursor_frame = MAX(0, MIN(pixel2frame(event->x), eears->adm->get_framecount(eeadv->mad)));
	gtk_signal_emit (GTK_OBJECT (eeadv), eeadv_signals[CURSOR_FRAME_CHANGED]);

	if (eeadv->cursor_mode == CM_SELECTING)
	{
		eeadv->selection_end = pixel2frame(event->x);
		if (eeadv->selection_end < 0) eeadv->selection_end = 0;
		if (eeadv->selection_begin < 0) eeadv->selection_begin = 0;
		if (eeadv->selection_end > eears->adm->get_framecount(eeadv->mad)) eeadv->selection_end = eears->adm->get_framecount(eeadv->mad);
		if (eeadv->selection_begin > eears->adm->get_framecount(eeadv->mad)) eeadv->selection_begin = eears->adm->get_framecount(eeadv->mad);

		tmp_max = MAX(eeadv->selection_end, eeadv->selection_begin);
		tmp_min = MIN(eeadv->selection_end, eeadv->selection_begin);

		eears->ssm->set_selection( eeadv->ms, eeadv->selection_begin, eeadv->selection_end);

		pixmap = gdk_pixmap_new (widget->window, eeadv->view_width, eeadv->view_height, -1);
		gc = gdk_gc_new(pixmap);
	/*	
		gdk_draw_rectangle (pixmap, widget->style->bg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, eeadv->view_width, eeadv->view_height); 
	*/
		gdk_draw_pixmap (pixmap,
				widget->style->fg_gc[GTK_STATE_NORMAL],
				eeadv->view,
				0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
		if (tmp_min <= eeadv->base_frame + pixel2frame(eeadv->view_width) &&
		    tmp_max >= eeadv->base_frame) {
		        gint start, size;

			start = MAX(frame2pixel(tmp_min),0);
			size = MAX(frame2pixel_ratio(tmp_max - MAX(tmp_min, eeadv->base_frame)), 1);
			gdk_draw_pixmap (pixmap,
					 widget->style->fg_gc[GTK_STATE_SELECTED],
					 eeadv->view_selected,
					 start,
					 0,
					 start,
					 0,
					 size,
					 eeadv->view_height);
		}

		gdk_draw_pixmap (widget->window,
				widget->style->fg_gc[GTK_STATE_NORMAL],
				pixmap,
				0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
		gdk_gc_unref(gc);
		gdk_pixmap_unref(pixmap);

		eears_adview_paint_markers(eeadv);
	} else { /* not in selecting mode highlight marker under mouse */
		// TODO!
		GList *l;
		gboolean cursor_set=FALSE;

		for (l = eeadv->markers_in_view	; l; l= g_list_next(l)) {
			if (((EEarsAdViewMarkerItem*)l->data)->pos == event->x) {
#ifdef DEBUG
				g_print("marker %s\n", ((EEarsAdViewMarkerItem*)l->data)->name);
#endif
				eeadv->cursor_mode = CM_OVER_MARKER; 
				gdk_window_set_cursor(widget->window, eeadv->cursors[eeadv->cursor_mode]);
				
				cursor_set = TRUE;
			}
		}
		if (!cursor_set && eeadv->cursor_mode == CM_OVER_MARKER) {
			eeadv->cursor_mode = CM_NORMAL; 
			gdk_window_set_cursor(widget->window, eeadv->cursors[eeadv->cursor_mode]);
		}
	}

	return FALSE;
}

gint 
eears_adview_key_release(GtkWidget *widget, GdkEventKey *event)
{
  
	return FALSE;
}

gint 
eears_adview_key_press(GtkWidget *widget, GdkEventKey *event)
{
	EEarsAdView *eeadv;
	gboolean update_view;
	
	g_assert(widget);
	g_assert(event);
	g_assert(IS_EEARS_ADVIEW(widget));

	eeadv = EEARS_ADVIEW(widget);
  /*  g_print("keypress: %x %s %x\n", event->keyval, event->string, event->state);*/

	update_view = FALSE;
	
	if (event->keyval == GDK_KP_Add) {
		gfloat magstep;
		
		magstep = ((float)eears -> csm -> get_int( mc, "magstep" ))/100.0;

		eears_adview_set_magnification(eeadv,
					       eears_adview_get_magnification(eeadv,ZOOM_UNIT_FPP)/(1+magstep),ZOOM_UNIT_FPP);
		eears -> vcm -> cur_view_update();
 	}
	
	if (event->keyval == GDK_KP_Subtract) {
		gfloat magstep;
		
		magstep = ((float)eears -> csm -> get_int( mc, "magstep" ))/100.0;

		eears_adview_set_magnification(eeadv,
					       eears_adview_get_magnification(eeadv,ZOOM_UNIT_FPP)*(1+magstep),ZOOM_UNIT_FPP);
		eears -> vcm -> cur_view_update();
 	}
	if (event->keyval == GDK_KP_Multiply) {
 	        eears_adview_set_window_range(eeadv,eeadv->selection_begin,
 				              eeadv->selection_end);
 	        eears -> vcm -> cur_view_update();
 	}
 
        if (event->keyval == GDK_Left) {
	        eears_adview_update_insertion_point( eeadv, 
						     MAX(0, MIN(eeadv->insertion_point - 1,
								eears->adm->get_framecount(eeadv->mad))));
		update_view = TRUE;
	}
	if (event->keyval == GDK_Right) {
	        eears_adview_update_insertion_point ( eeadv,
						      MAX(0, MIN(eeadv->insertion_point + 1,
								 eears->adm->get_framecount(eeadv->mad))));
		update_view = TRUE;
	}
	if (event->keyval == GDK_Tab) {
		// Move insertion point to the next marker, wrap.
		// Find next marker, relative to insertion_point.
		GList *l = NULL;
		glong pos;
		glong first_marker = eeadv->insertion_point;
		glong next_marker = eears->adm->get_framecount(eeadv->mad);

		l = eears -> msm -> get_markers_in_range(eeadv->mm,
							 0,
							 eears->adm->get_framecount(eeadv->mad));

		while (l) {
			pos = eears -> msm -> get_marker(eeadv->mm, (gchar*)l->data);
			if (pos > eeadv->insertion_point && pos < next_marker)
				next_marker = pos;
			if (pos < first_marker)
				first_marker = pos;
			l = g_list_next(l);
		}

		if (next_marker != eears->adm->get_framecount(eeadv->mad)) {
			eears_adview_update_insertion_point ( eeadv, next_marker );
		} else {
			eears_adview_update_insertion_point ( eeadv, first_marker );
		}
	        
		g_list_free(l);
		update_view = TRUE;
	}

	if (event->keyval == GDK_ISO_Left_Tab) {
		// Move insertion point to the previous marker, wrap.
		// Find next marker, relative to insertion_point.
		GList *l = NULL;
		glong pos;
		glong last_marker = eeadv->insertion_point;
		glong prev_marker = 0;

		l = eears -> msm -> get_markers_in_range(eeadv->mm,
							 0,
							 eears->adm->get_framecount(eeadv->mad));

		while (l) {
			pos = eears -> msm -> get_marker(eeadv->mm, (gchar*)l->data);
			if (pos < eeadv->insertion_point && pos > prev_marker)
				prev_marker = pos;
			if (pos > last_marker)
				last_marker = pos;
			l = g_list_next(l);
		}

		if (prev_marker != 0) {
			eears_adview_update_insertion_point ( eeadv, prev_marker );
		} else {
			eears_adview_update_insertion_point ( eeadv, last_marker );
		}
	        
		g_list_free(l);
		update_view = TRUE;
	}

	/* went out of view, update view */
	if (update_view && (eeadv->insertion_point < eeadv->base_frame ||
	    frame2pixel(eeadv->insertion_point) > eeadv->view_width) ) {
		gtk_adjustment_set_value(eeadv->scroll_adj,
					 eeadv->insertion_point - pixel2frame_ratio(eeadv->view_width/2));
	}

	return TRUE;
}



GtkWidget*
eears_adview_new (EEarsViewInfo *evi, GtkAdjustment *scroll_adj, 
		  GtkAdjustment *zoom_adj,GtkStatusbar *statusbar)
{
        EEarsAdView *eeadv;

	g_assert(evi);
	g_assert(scroll_adj);
	g_assert(zoom_adj);

	eeadv = gtk_type_new (eears_adview_get_type ());
	
	eeadv->mad = evi->mad;
	eeadv->ms = evi->ms;
	eeadv->mm = evi->mm;
	eeadv->scroll_adj = scroll_adj;
	eeadv->zoom_adj = zoom_adj;
	eeadv->statusbar = statusbar;

	g_assert(eeadv->mad);
	g_assert(eeadv->ms);
	
	//gnome_popup_menu_attach(eeadv->menuPopup, GTK_WIDGET(eeadv), (gpointer)eeadv);
	gtk_signal_connect_object( GTK_OBJECT(eeadv->scroll_adj), "value_changed", 
				   GTK_SIGNAL_FUNC (eears_adview_adj_changed),
				   GTK_OBJECT(eeadv) );
	gtk_signal_connect_object( GTK_OBJECT(eeadv->scroll_adj), "changed", 
				   GTK_SIGNAL_FUNC (eears_adview_adj_changed),
				   GTK_OBJECT(eeadv) );
	
	/* the large cache contains a max of 2^31/1000 "virtual" frames or
	 * the actual number of frames and is used for large zoom values 
	 */
	eeadv_init_cache(&eeadv->large_cache,
			 MAX(1,MIN(EEADV_MAX_LARGE_CACHE_SIZE, eears->adm->get_framecount(eeadv->mad))),
			 ((gfloat)eears->adm->get_framecount(eeadv->mad)) /
			 MAX(1,MIN(EEADV_MAX_LARGE_CACHE_SIZE, eears->adm->get_framecount(eeadv->mad))));
	/* the fast cache is small and contains as many
	 * elements as the width of the widget with a variable, small, zoom ratio to
	 * the sample */
	eeadv_init_cache(&eeadv->fast_cache,
			 EEADV_DEFAULT_WIDTH,
			 eeadv->mult);
	eeadv_build_large_cache(eeadv);
	
	return GTK_WIDGET (eeadv);
}

static void
eears_adview_destroy (GtkObject *object)
{
        EEarsAdView *eeadv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (object));

	eeadv = EEARS_ADVIEW (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
	  (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
eears_adview_realize (GtkWidget *widget)
{
	EEarsAdView *eeadv;
	GdkWindowAttr attributes;
	gint attributes_mask;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (widget));

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
	eeadv = EEARS_ADVIEW (widget);

	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.window_type = GDK_WINDOW_CHILD;

	attributes.event_mask = gtk_widget_get_events (widget) | 
		GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
		GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK|
			    GDK_KEY_RELEASE_MASK| GDK_KEY_PRESS_MASK;

	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
	widget->window = gdk_window_new (widget->parent->window,
			&attributes,
			attributes_mask);

	widget->style = gtk_style_attach (widget->style, widget->window);

	gdk_window_set_user_data (widget->window, widget);

	gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);

//	eeadv->mult = MAX(1,((gfloat)eears->adm->get_framecount(eeadv->mad))/EEADV_DEFAULT_WIDTH/eeadv->zoom);

}

static void 
eears_adview_size_request (GtkWidget      *widget,
		       GtkRequisition *requisition)
{
	requisition->width = EEADV_DEFAULT_WIDTH;
	requisition->height = EEADV_DEFAULT_HEIGHT;
}

static void
eears_adview_size_allocate (GtkWidget     *widget,
			GtkAllocation *allocation)
{
	EEarsAdView *eeadv;
	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (widget));
	g_return_if_fail (allocation != NULL);

	g_assert(allocation);

	eeadv = EEARS_ADVIEW(widget);

	widget->allocation = *allocation;

	if (GTK_WIDGET_REALIZED (widget))
	{
		gdk_window_move_resize (widget->window,
				allocation->x, allocation->y,
				allocation->width, allocation->height);

	}
	
	eeadv_resize_cache(&eeadv->fast_cache,
			 GTK_WIDGET(eeadv)->allocation.width,
			 eeadv->mult);

	if (eeadv->scroll_adj) {
	        AudioDataMethods *adm;
	        gint rate;
		glong frames;
		adm = eears->adm;
		rate = adm->get_rate(eeadv->mad);
		frames = adm->get_framecount(eeadv->mad);
		
		eeadv->mult = MAX(1,((gfloat)eears->adm->get_framecount(eeadv->mad))/allocation->width/eeadv->zoom);

		eeadv->scroll_adj->lower = 0;
		eeadv->scroll_adj->upper = frames;
		eeadv->scroll_adj->value = eeadv -> base_frame;
		eeadv->scroll_adj->step_increment = ((gfloat)frames) / eeadv->zoom / 10;
		eeadv->scroll_adj->page_increment = ((gfloat)frames) / eeadv->zoom;
		eeadv->scroll_adj->page_size = ((gfloat)frames) / eeadv->zoom;
		gtk_signal_emit_by_name (GTK_OBJECT (eeadv->scroll_adj), "changed");
	}

	if (eeadv->zoom_adj) {
		eeadv->zoom_adj->upper = ((gfloat)eears->adm->get_framecount(eeadv->mad))/allocation->width;
		gtk_signal_emit_by_name (GTK_OBJECT (eeadv->zoom_adj), "changed");
		eeadv->zoom_adj->value = MIN(eeadv->zoom_adj->value, ((gfloat)eears->adm->get_framecount(eeadv->mad))/allocation->width);
		gtk_signal_emit_by_name (GTK_OBJECT (eeadv->zoom_adj), "value_changed");
	}
}

void
eears_adview_adj_changed( EEarsAdView *eeadv)
{
#ifdef DEBUG
	g_print("adj_changed");
#endif
        g_assert(eeadv);
        g_assert(eeadv->scroll_adj);

	/* fit base_frame into bounds */
	eeadv->base_frame = MAX(0,MIN(eears->adm->get_framecount(eeadv->mad) * ( 1 - 1.0/eeadv->zoom), eeadv->scroll_adj->value));
#ifdef DEBUG
	g_print("%ld %f %f\n", eears->adm->get_framecount(eeadv->mad),
		eeadv->zoom, eeadv->scroll_adj->value);
#endif
//	eeadv->mult = MAX(1,((gfloat)eears->adm->get_framecount(eeadv->mad))/eeadv->view_width/eeadv->zoom);
	/* fit insertion_point into bound */
	eears_adview_update_insertion_point ( eeadv, MAX(0, MIN(eeadv->insertion_point, eears->adm->get_framecount(eeadv->mad))));
	
	eeadv->force_bitmap_redraw = TRUE;
	gtk_widget_draw(GTK_WIDGET(eeadv), NULL);
}

void
eeadv_init_cache(EEarsAdViewCache *cache, gulong size, gdouble zoom)
{
	/*g_print("init large cache\n");*/
	cache->size = size;
	cache->zoom = zoom;

	if (!(cache->items = g_malloc(sizeof(EEarsAdViewCacheItem)*cache->size)))
	{
		g_error("unable to allocate mem");
		return;
	}
	eeadv_reset_cache(cache);
}


void 
eeadv_resize_cache(EEarsAdViewCache *cache, gulong size, gdouble zoom)
{
/*         g_print("resize cache %ld %f\n", size, zoom); */
        cache->size = size;
        cache->zoom = zoom;

	if (cache->items) g_free(cache->items);

        if (!(cache->items = g_malloc(sizeof(EEarsAdViewCacheItem)*cache->size)))
	{
		g_error("unable to allocate mem");
		return;
	}
	eeadv_reset_cache(cache);
}

void
eeadv_reset_cache(EEarsAdViewCache *cache)
{
	gint i;
	for (i=0; i<cache->size; i++)
	{
		cache->items[i].left1 = 0;
		cache->items[i].left2 = 0;
		cache->items[i].right1 = 0;
		cache->items[i].right2 = 0;
	}
	cache->begin_frame = 0;
	cache->end_frame = 0;
	cache->ready = FALSE;
}

/* There are two caches, the Large cache and the Fast cache. 
 * the Large cache (max. 1MB big) contains a zoomed version of the sample
 * for navigation at high zoom values. The Large cache is rebuild only 
 * when the sample changes.
 * the Fast cache is small and dynamic. Used for low zoom values, it is 
 * computed according to the current viewport on the sample.
 * The two caches use the same strucure and are used almost in the same
 * way in display_from_cache. The Fast cache also implements a fast scrolling
 * mechanism to enhance partial scrolls.
 */

void
eeadv_build_large_cache(EEarsAdView *eeadv)
{
	AudioDataMethods *adm;
	MetaAudioData *mad;
	glong num_frame;
	glong i, j, k;
	guint8 *buffer, *buffer_baser, *buffer_basel;
	guint8 tmp;
	gint tmpl1=255, tmpl2=0;
	gint tmpr1=255, tmpr2=0;
	gdouble step, step_add;
	glong stepcfr;
	gint buffer_add, buffer_value_add;
	gint framesize;
	gint channels;
	gint framesize_div_2;
	gboolean channels2, bits16;
	gint hop;
#ifndef REAL_USE_GTHREADS
	gint status;
#endif

	if (!(buffer = g_malloc(EEADV_READ_FILE_BUFFER_SIZE)))
	{
		g_error("unable to allocate memory");
		return;
	}

	eeadv_bg_build_large_cache_stop(eeadv); 
	eeadv_reset_cache(&eeadv->large_cache);
	
	adm = eears->adm;
	mad = eeadv->mad;

	/* num_frame is the actual number of frames to read in. */
	num_frame = adm->get_framecount(mad);
	adm->seek(mad, 0);
	/* FIXME: use the return value of read, instead assuming
	 * that read will read MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame) frames
	 **/
	adm->read(mad, buffer, MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame));

	/* compute the cache to frames ratio */
	step_add = ((double)num_frame) / ((double) eeadv->large_cache.size); 
	step = step_add;
	stepcfr = (glong) step; 

	if (adm->get_bits(mad)==16)
	{
		buffer_add = 1;
		buffer_value_add = 128;
		bits16 = TRUE;
	} else {
		buffer_add = 0;
		buffer_value_add = 0;
		bits16 = FALSE;
	}

	framesize = adm->get_framesize(mad);
	channels = adm->get_channels(mad);

	framesize_div_2 = framesize / 2;
	
	if (channels == 2) channels2 = TRUE;
	else channels2 = FALSE;

	/* in the next loops i is the absolute frame pointer, 
	                     j is the pointer in the readin data from the sample,
			     k is the output pointer in the cache */

	buffer_basel = buffer + buffer_add;
	buffer_baser = buffer + framesize_div_2 + buffer_add;

	hop = MAX(1, step/4);

	if (channels2) {
		for (i=0, j=0, k=0; i<num_frame; i+=hop, j += framesize)
		{
			if ( i >= stepcfr )
			{
				tmpl1>>=1;
				tmpl2>>=1;
				tmpr1>>=1;
				tmpr2>>=1;

				eeadv->large_cache.items[k].left1 = tmpl1;
				eeadv->large_cache.items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				eeadv->large_cache.items[k].right1 = tmpr1;
				eeadv->large_cache.items[k].right2 = tmpr2;
				tmpr1 = 255;
				tmpr2 = 0;

				step += step_add;
				stepcfr = (glong) step;
				k++;	
			}

			tmp = *(buffer_basel + j) + buffer_value_add;
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			tmp = *(buffer_baser + j) + buffer_value_add;
			if (tmp < tmpr1) tmpr1 = tmp;
			if (tmp > tmpr2) tmpr2 = tmp;

			if (j >= EEADV_READ_FILE_BUFFER_SIZE - framesize)
			{
				adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j = -framesize;
				/*g_print("%ld ",i);*/
			}
		}
		/* flush the last frames */
		eeadv->large_cache.items[k].left1 = tmpl1;
		eeadv->large_cache.items[k].left2 = tmpl2;
		eeadv->large_cache.items[k].right1 = tmpr1;
		eeadv->large_cache.items[k].right2 = tmpr2;
	} else {
		for (i=0, j=0, k=0; i<num_frame; i+=hop, j+=framesize)
		{
			if (i >= ((glong) step) )
			{
				eeadv->large_cache.items[k].left1 = tmpl1;
				eeadv->large_cache.items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				step += step_add;
				k++;	
			}

			tmp = *(buffer_basel + j) + buffer_value_add;
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			if (j >= EEADV_READ_FILE_BUFFER_SIZE - framesize)
			{
			        adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j = -framesize;
			}
		}
		/* flush the last frames */
		eeadv->large_cache.items[k].left1 = tmpl1;
		eeadv->large_cache.items[k].left2 = tmpl2;
	}
	eeadv->large_cache.begin_frame = 0;
	eeadv->large_cache.end_frame = num_frame;
	eeadv->large_cache.ready = TRUE;

/*	g_print("%d, %d\n", eeadv->cache.begin_frame, eeadv->cache.end_frame); */

	g_free(buffer);

#ifdef DEBUG
	g_print("background large cache, begin\n");
#endif
	
#ifdef REAL_USE_GTHREADS
	eeadv->play_thread = g_thread_create(eeadv_bg_build_large_cache, eeadv, 0, TRUE, TRUE, G_THREAD_PRIORITY_NORMAL);
#else
	status = pthread_create (&eeadv->blc_tid,
			NULL,
			(pthread_start_routine_t*)eeadv_bg_build_large_cache,
			eeadv);
	if (status) { 
		g_warning("Couldn't spawn thread for blc!"); 
		eeadv->blc_tid = 0;
	} 
#endif
}

void eeadv_bg_build_large_cache_stop (EEarsAdView *eeadv)
{
#ifdef DEBUG
	g_print("begin blc stop\n");
#endif
	
#ifdef REAL_USE_GTHREADS
	must_stop = TRUE;
	/* TODO: check: do I need this? */
	if (eeadv->play_thread) g_thread_join(eeadv->play_thread);
	eeadv->play_thread = NULL;
	must_stop = FALSE;
#else 
	if (eeadv->blc_tid != 0)
	{
		/*
		if (pthread_join(play_tid, NULL)!=0)
		{
			g_warning("error joining threads\n");
		}
		*/
		pthread_cancel(eeadv->blc_tid);
		eeadv->blc_tid = 0;
	}
		
#endif
#ifdef DEBUG
	g_print("end blc stop\n");
#endif
}

static
guint gtk_statusbar_push_perc(GtkStatusbar *s, guint ctx, gchar *str, gint perc) 
{
	gchar buf[100];

	snprintf(buf, 100, "%s %d%%", str, perc);
	return gtk_statusbar_push(s, ctx, buf);
}

/* The "right" large cache is rebuilt in background by this secondary thread */
void
eeadv_bg_build_large_cache(EEarsAdView *eeadv)
{
	AudioDataMethods *adm;
	MetaAudioData *mad;
	glong num_frame;
	glong i, j, k;
	guint8 *buffer, *buffer_baser, *buffer_basel;
	guint8 tmp;
	gint tmpl1=255, tmpl2=0;
	gint tmpr1=255, tmpr2=0;
	gdouble step, step_add;
	glong stepcfr;
	gint buffer_add, buffer_value_add;
	gint framesize;
	gint channels;
	gint framesize_div_2;
	gboolean channels2, bits16;
	EEarsAdViewCacheItem *items, *tmpitems;
//	guint statusbar_ctx;
	
#ifndef REAL_USE_GTHREADS
	pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
#endif

	if (!(buffer = g_malloc(EEADV_READ_FILE_BUFFER_SIZE)))
	{
		g_error("unable to allocate memory");
		return;
	}

        if (!(items = g_malloc(sizeof(EEarsAdViewCacheItem)*eeadv->large_cache.size)))
	{
		g_error("unable to allocate mem");
		return;
	}
	
	adm = eears->adm;
	mad = eeadv->mad;

	/* num_frame is the actual number of frames to read in. */
	num_frame = adm->get_framecount(mad);
	adm->seek(mad, 0);
	/* FIXME: use the return value of read, instead assuming
	 * that read will read MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame) frames
	 **/
	adm->read(mad, buffer, MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame));

	/* compute the cache to frames ratio */
	step_add = ((double)num_frame) / ((double) eeadv->large_cache.size); 
	step = step_add;
	stepcfr = (glong) step; 

	if (adm->get_bits(mad)==16)
	{
		buffer_add = 1;
		buffer_value_add = 128;
		bits16 = TRUE;
	} else {
		buffer_add = 0;
		buffer_value_add = 0;
		bits16 = FALSE;
	}

	framesize = adm->get_framesize(mad);
	channels = adm->get_channels(mad);

	framesize_div_2 = framesize / 2;
	
	if (channels == 2) channels2 = TRUE;
	else channels2 = FALSE;

	/* in the next loops i is the absolute frame pointer, 
	                     j is the pointer in the readin data from the sample,
			     k is the output pointer in the cache */

	buffer_basel = buffer + buffer_add;
	buffer_baser = buffer + framesize_div_2 + buffer_add;

/* 	statusbar_ctx = gtk_statusbar_get_context_id ( eeadv->statusbar,  */
/* 						       "Background sample scan"); */
/* 	gtk_statusbar_push_perc(eeadv->statusbar, statusbar_ctx, _("Scanning"), 0); */
/* //				gtk_statusbar_pop(eeadv->statusbar, statusbar_ctx); */
/* 				gtk_statusbar_push_perc(eeadv->statusbar, statusbar_ctx, _("Scanning"), ((gfloat)i)/ num_frame*100 ); */
	if (channels2) {
		for (i=0, j=0, k=0; i<num_frame; i++, j += framesize)
		{
			if ( i >= stepcfr )
			{
				tmpl1>>=1;
				tmpl2>>=1;
				tmpr1>>=1;
				tmpr2>>=1;

				items[k].left1 = tmpl1;
				items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				items[k].right1 = tmpr1;
				items[k].right2 = tmpr2;
				tmpr1 = 255;
				tmpr2 = 0;

				step += step_add;
				stepcfr = (glong) step;
				k++;	
			}

			tmp = *(buffer_basel + j) + buffer_value_add;
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			tmp = *(buffer_baser + j) + buffer_value_add;
			if (tmp < tmpr1) tmpr1 = tmp;
			if (tmp > tmpr2) tmpr2 = tmp;

			if (j >= EEADV_READ_FILE_BUFFER_SIZE - framesize)
			{
				adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j = -framesize;
				/*g_print("%ld ",i);*/
			}
		}
		/* flush the last frames */
		items[k].left1 = tmpl1;
		items[k].left2 = tmpl2;
		items[k].right1 = tmpr1;
		items[k].right2 = tmpr2;
	} else {
		for (i=0, j=0, k=0; i<num_frame; i++, j+=framesize)
		{
			if (i >= ((glong) step) )
			{
				items[k].left1 = tmpl1;
				items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				step += step_add;
				k++;	
			}

			tmp = *(buffer_basel + j) + buffer_value_add;
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			if (j >= EEADV_READ_FILE_BUFFER_SIZE - framesize)
			{
			        adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j = -framesize;
			}
		}
		/* flush the last frames */
		items[k].left1 = tmpl1;
		items[k].left2 = tmpl2;
	}

#ifdef DEBUG
	g_print("end of bg blc %d, %d\n",
		eeadv->large_cache.begin_frame, eeadv->large_cache.end_frame); 
#endif

#ifndef REAL_USE_GTHREADS
	pthread_mutex_lock(&eeadv->large_cache_mutex);
#else
#endif
	tmpitems = eeadv->large_cache.items;
	eeadv->large_cache.items = items;
#ifndef REAL_USE_GTHREADS
	pthread_mutex_unlock(&eeadv->large_cache_mutex);
#else
#endif
	if (tmpitems) g_free(tmpitems);
	eeadv->large_cache.ready = TRUE;


	g_free(buffer);
}

/**************************************************************************/

void
eeadv_build_fast_cache(EEarsAdView *eeadv, MetaAudioData mad, glong base, gdouble zoom)
{
	AudioDataMethods *adm;
	glong num_frame;
	glong i, j, k;
	glong outbegin, inbegin, inend, outend;
	guint8* buffer;
	guint8 tmp;
	gint tmpl1=255, tmpl2=0;
	gint tmpr1=255, tmpr2=0;
	gdouble step, step_add;
	gint buffer_add, buffer_value_add;
	gint framesize;
	gint channels;
	gint framesize_div_2;
	gboolean channels2, bits16;
	glong begin,end;

	if (!(buffer = g_malloc(EEADV_READ_FILE_BUFFER_SIZE)))
	{
		g_error("unable to allocate memory");
		return;
	}

	/*eeadv_reset_cache(&eeadv->fast_cache);*/
	
	adm = eears->adm;

	inbegin = begin = base;
	inend = end = base + eeadv->fast_cache.size * zoom;
	outbegin = 0;
	outend = MIN(eeadv->fast_cache.size, adm->get_framecount(mad) / zoom);

	/* try to recycle previous cache */
/* 	if (adm->get_lastmodify(mad) >= g_timer_elapsed(eeadv->last_update, NULL)) { */
	if (zoom == eeadv->fast_cache.zoom && eeadv->fast_cache.ready
	    && base >= eeadv->fast_cache.begin_frame 
	    && base <= eeadv->fast_cache.end_frame) {
	  EEarsAdViewCacheItem *l, *m;
	  gint difference;

	  difference = (base - eeadv->fast_cache.begin_frame) / eeadv->fast_cache.zoom;
	  outbegin = eeadv->fast_cache.size -1 - difference;
	  inbegin = eeadv->fast_cache.end_frame;
	  l = eeadv->fast_cache.items + difference;
	  m = eeadv->fast_cache.items;

	  for (i = 0; i < eeadv->fast_cache.size - difference; i++)
	    m[i] = l[i]; 

	} else if (zoom == eeadv->fast_cache.zoom && eeadv->fast_cache.ready
	    && end >= eeadv->fast_cache.begin_frame 
	    && end <= eeadv->fast_cache.end_frame) {
	  EEarsAdViewCacheItem *l, *m;
	  gint difference;

	  difference = (eeadv->fast_cache.end_frame - end) / eeadv->fast_cache.zoom;
	  l = eeadv->fast_cache.items;
	  m = eeadv->fast_cache.items + difference;

	  for (i = eeadv->fast_cache.size - 1 - difference; i >= 0; i--)
	    m[i] = l[i]; 
	  inend = eeadv->fast_cache.begin_frame;
	  outend = difference;
	}
/* 	} else eeadv_reset_cache(&eeadv->fast_cache); */

	/* num_frame is the actual number of frames to read in. */
	num_frame = MIN(inend,adm->get_framecount(mad)) - inbegin;
	adm->seek(mad, MAX(inbegin,0));
	/* FIXME: use the return value of read, instead assuming
	 * that read will read MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame) frames
	 **/
	adm->read(mad, buffer, MIN((EEADV_READ_FILE_BUFFER_SIZE / adm->get_framesize(mad)), num_frame));

	/* compute the cache to frames ratio */

	step_add = zoom; 
	step = step_add;

	if (adm->get_bits(mad)==16)
	{
		buffer_add = 1;
		buffer_value_add = 128;
		bits16 = TRUE;
	} else {
		buffer_add = 0;
		buffer_value_add = 0;
		bits16 = FALSE;
	}

	framesize = adm->get_framesize(mad);
	channels = adm->get_channels(mad);

	framesize_div_2 = framesize / 2;
	
	if (channels == 2) channels2 = TRUE;
	else channels2 = FALSE;

	/* in the next loops i is the absolute frame pointer, 
	                     j is the pointer in the readin data from the sample,
			     k is the output pointer in the cache */
/* 	g_print("refresh: %d %d\n", outbegin, outend); */

	if (channels2) {
		for (i=0, j=0, k=outbegin; k<outend; i++, j+=framesize)
		{
			if (((double) i) >= step)
			{
				tmpl1>>=1;
				tmpl2>>=1;
				tmpr1>>=1;
				tmpr2>>=1;

				eeadv->fast_cache.items[k].left1 = tmpl1;
				eeadv->fast_cache.items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				eeadv->fast_cache.items[k].right1 = tmpr1;
				eeadv->fast_cache.items[k].right2 = tmpr2;
				tmpr1 = 255;
				tmpr2 = 0;

				step += step_add;
				k++;	
			}

			tmp = *(buffer + j + buffer_add) + buffer_value_add;
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			tmp = *(buffer + j + framesize_div_2 + buffer_add) + buffer_value_add;
			if (tmp < tmpr1) tmpr1 = tmp;
			if (tmp > tmpr2) tmpr2 = tmp;

			if ( ( j + framesize ) >= EEADV_READ_FILE_BUFFER_SIZE )
			{
				adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j=-framesize;
			}
		}
	} else {
		for (i=0, j=0, k=outbegin; k<outend; i++, j+=framesize)
		{
			if (((double) i) >= step)
			{
				eeadv->fast_cache.items[k].left1 = tmpl1;
				eeadv->fast_cache.items[k].left2 = tmpl2;
				tmpl1 = 255;
				tmpl2 = 0;

				step += step_add;
				k++;	
			}

			tmp = *(buffer + j + buffer_add) + buffer_value_add;
/* 			        g_print("(%d:%d-%d) ", (gint)tmp,tmpl1, tmpl2); */
			if (tmp < tmpl1) tmpl1 = tmp;
			if (tmp > tmpl2) tmpl2 = tmp;

			if ( (j + framesize ) >= EEADV_READ_FILE_BUFFER_SIZE )
			{
				adm->read(mad, buffer, EEADV_READ_FILE_BUFFER_SIZE / framesize);
				j=-framesize;
			}
		}
	}
	eeadv->fast_cache.begin_frame = begin;
	eeadv->fast_cache.end_frame = end;
	eeadv->fast_cache.zoom = zoom;
	eeadv->fast_cache.ready = TRUE;
/* 	g_print("fast cache %ld, %ld, %f\n", eeadv->fast_cache.begin_frame, eeadv->fast_cache.end_frame, eeadv->fast_cache.zoom);  */
	
	g_free(buffer);
}

void 
eeadv_display_from_cache(EEarsAdView *eeadv,
				GdkWindow *pixmap, 
				MetaAudioData mad, 
				gint w, 
				gint h)
{
	GdkGC *gc;
	GdkColor *left_color, *right_color;
	gdouble step, step_add;
	gint tmpl1=255, tmpl2=0;
	gint tmpr1=255, tmpr2=0;
	glong i, k;
	glong begin, end;
	gboolean channels2;
	EEarsAdViewCache *cache;

	g_assert(eeadv);
	g_assert(pixmap);
	g_assert(mad);
	g_assert(w>0);
	g_assert(h>0);

	gc = gdk_gc_new(pixmap);

	if (eears->adm->get_channels(mad)==2) channels2 = TRUE;
	else channels2 = FALSE;
	
	left_color = eeadv->left_channel_color;
	right_color = eeadv->right_channel_color;
	gdk_gc_set_foreground(gc, eeadv->single_channel_color);
	
	if (eeadv->large_cache.zoom >= eeadv->mult ||
	    !eeadv->large_cache.ready) {
		cache = &eeadv->fast_cache; 
		begin = (eeadv->base_frame - cache -> begin_frame)/ cache->zoom; 
		end = MIN( cache->size, 
			   (eeadv->base_frame - cache -> begin_frame + w * eeadv->mult) / cache->zoom); 
	} else {
#ifndef REAL_USE_GTHREADS
		pthread_mutex_lock(&eeadv->large_cache_mutex);
#else
#endif
		cache = &eeadv->large_cache;
		begin = eeadv->base_frame / cache->zoom; 
		end = MIN( cache->size, 
			   (eeadv->base_frame + w * eeadv->mult) / cache->zoom); 
	}

	step_add = eeadv->mult / cache->zoom;
	step = step_add;

	for (i=begin, k=0; i<end; i++)
	{
	        if (cache->items[i].left1 < tmpl1) tmpl1 = cache->items[i].left1;
		if (cache->items[i].left2 > tmpl2) tmpl2 = cache->items[i].left2;

		if (channels2)
		{
			if (cache->items[i].right1 < tmpr1) tmpr1 = cache->items[i].right1;
			if (cache->items[i].right2 > tmpr2) tmpr2 = cache->items[i].right2;
		}

		if (((double)(i-begin)) > step)
		{
			if (channels2)
			{
				tmpl1 = ((h/2)-((tmpl1 * h)/256));
				tmpl2 = ((h/2)-((tmpl2 * h)/256));
				gdk_gc_set_foreground(gc, left_color);
				gdk_draw_line(pixmap, gc, k, tmpl1, k, tmpl2);
				tmpl1 = 255;
				tmpl2 = 0;

				tmpr1 = (h/2)+((h/2)-((tmpr1 * h)/256));
				tmpr2 = (h/2)+((h/2)-((tmpr2 * h)/256));
				gdk_gc_set_foreground(gc, right_color);
				gdk_draw_line(pixmap, gc, k, tmpr1, k, tmpr2);
				tmpr1 = 255;
				tmpr2 = 0;
			} else {
				tmpl1 = (h-((tmpl1 * h)/256));
				tmpl2 = (h-((tmpl2 * h)/256));
				gdk_draw_line(pixmap, gc, k, tmpl1, k, tmpl2);
				tmpl1 = 255;
				tmpl2 = 0;
			}

			step += step_add;
			k++;
		}
			
	}

	if (cache == &eeadv->large_cache) {
#ifndef REAL_USE_GTHREADS
		pthread_mutex_unlock(&eeadv->large_cache_mutex);
#else
#endif
	}
	
	gdk_gc_unref(gc);
}

gint
eears_adview_expose (GtkWidget      *widget,
		 	GdkEventExpose *event)
{
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (IS_EEARS_ADVIEW (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

/* 	g_print("expose"); */
	if (GTK_WIDGET_DRAWABLE (widget))
	{
		EEarsAdView *eeadv;
		MetaAudioData mad;
		AudioDataMethods *adm;
		GdkGC *gc;
		GdkGC *real_gc;
		GdkPixmap *pixmap;
		GdkPixmap *real_pixmap;
		gint h, w;
		glong b;
		glong selection_begin, selection_end;

		h = widget->allocation.height;
		w = widget->allocation.width;

		eeadv = EEARS_ADVIEW (widget);
	        b = MAX(0,MIN(eears->adm->get_framecount(eeadv->mad) * ( 1 - 1.0/eeadv->zoom), eeadv->scroll_adj->value));
		mad = eeadv->mad;
		adm = eears->adm;

		eears->ssm->get_selection(eeadv->ms, &selection_begin, &selection_end);
		
		g_assert((adm->get_bits(mad)==16) || (adm->get_bits(mad)==8));
		g_assert((adm->get_channels(mad)==2) || (adm->get_channels(mad)==1));
		if ((eeadv->view) && (eeadv->view_selected)
			&& (adm->get_lastmodify(mad) > g_timer_elapsed(eeadv->last_update, NULL))
			&& (eeadv->view_height == h)
			&& (eeadv->view_width == w) 
		        && (eeadv->base_frame == b)
		    && (!eeadv->force_bitmap_redraw))
				
		{
/* 			g_print("not updated\n");  */
			real_pixmap = gdk_pixmap_new (widget->window, eeadv->view_width, eeadv->view_height, -1);
			real_gc = gdk_gc_new(real_pixmap);
		/*	
			gdk_draw_rectangle (real_pixmap, widget->style->bg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, w, h); 
		*/
			gdk_draw_pixmap (real_pixmap, 
				widget->style->fg_gc[GTK_STATE_NORMAL], 
				eeadv->view,
				0, 0, 0, 0, w, h);
			if (selection_begin <= eeadv->base_frame + pixel2frame(eeadv->view_width) && 
			    selection_end >= eeadv->base_frame) {
			        glong start, size;
				start = MAX(frame2pixel(selection_begin),0);
				size = MAX(frame2pixel_ratio(selection_end - MAX(selection_begin, eeadv->base_frame)), 1);
				gdk_draw_pixmap (real_pixmap,
						 widget->style->fg_gc[GTK_STATE_SELECTED],
						 eeadv->view_selected,
						 start,
						 0,
						 start,
						 0,
						 size,
						 eeadv->view_height);
			}
			gdk_draw_pixmap (widget->window,
					widget->style->fg_gc[GTK_STATE_NORMAL],
					real_pixmap,
					0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
			gdk_gc_unref(real_gc);
			gdk_pixmap_unref(real_pixmap);

			eears_adview_paint_markers(eeadv);

			return FALSE;
		} 
/* 		g_print("updated\n"); */

		eeadv->view_height = h;
		eeadv->view_width = w;
		eeadv->base_frame = b;
		eeadv->force_bitmap_redraw = FALSE;

/*  		g_print("%ld %ld %ld %f, %ld",eeadv->fast_cache.begin_frame, */
/* 		                          eeadv->base_frame, */
/* 					  eeadv->fast_cache.end_frame,  */
/* 			                  eeadv->mult, */
/* 			                  pixel2frame(eeadv->view_width));  */
		if ((eeadv->fast_cache.begin_frame != eeadv->base_frame) 
		    || (eeadv->fast_cache.end_frame != pixel2frame(eeadv->view_width)) 
		    || (eeadv->fast_cache.zoom != eeadv->mult)
		    || (adm->get_lastmodify(mad) < g_timer_elapsed(eeadv->last_update, NULL)))
		{
/*  			g_print("fast_cache rebuilt\n");  */
		  /* if using the fast cache, rebuild */ 
		  if (eeadv->large_cache.zoom >= eeadv->mult || 
		      !eeadv->large_cache.ready)
		    eeadv_build_fast_cache(eeadv, mad, 
					   eeadv->base_frame, 
					   eeadv->mult);
		}

		if (adm->get_lastmodify(mad) < g_timer_elapsed(eeadv->last_update, NULL)) {
		        eeadv_resize_cache(&eeadv->large_cache,
					   MIN(EEADV_MAX_LARGE_CACHE_SIZE, eears->adm->get_framecount(eeadv->mad)),
					   ((gfloat)eears->adm->get_framecount(eeadv->mad)) /
					   MIN(EEADV_MAX_LARGE_CACHE_SIZE, eears->adm->get_framecount(eeadv->mad)));
			eeadv_build_large_cache(eeadv);
		}
		g_timer_reset(eeadv->last_update);
		
		/* Double Buffer! */
		real_pixmap = gdk_pixmap_new (widget->window, eeadv->view_width, eeadv->view_height, -1);
		real_gc = gdk_gc_new(real_pixmap);


		pixmap = gdk_pixmap_new	(widget->window, w, h, -1);
		gc = gdk_gc_new(pixmap);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->single_channel_color);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->right_channel_color);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->left_channel_color);
	
		gdk_draw_rectangle (pixmap, widget->style->bg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, w, h); 

		eeadv_display_from_cache(eeadv, pixmap, mad, w, h);

		gdk_draw_pixmap (real_pixmap, 
				widget->style->fg_gc[GTK_STATE_NORMAL], 
				pixmap,
				0, 0, 0, 0, w, h);
		gdk_gc_unref(gc); 
		if (eeadv->view) gdk_pixmap_unref(eeadv->view);
		eeadv->view = pixmap;


		/* let's build the selected view 
		 * This will slow down *2 this phase, but it will
		 * speed up a lot in the "selecting" phase */
		pixmap = gdk_pixmap_new (widget->window, w, h, -1);
		gc = gdk_gc_new(pixmap);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->single_channel_color);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->right_channel_color);
		gdk_color_alloc(gtk_widget_get_colormap(widget), eeadv->left_channel_color);
	
		gdk_draw_rectangle (pixmap, widget->style->bg_gc[GTK_STATE_SELECTED], TRUE, 0, 0, w, h);
	
		eeadv_display_from_cache(eeadv, pixmap, mad, w, h);

		if (selection_begin <= eeadv->base_frame + pixel2frame(eeadv->view_width) && 
		    selection_end >= eeadv->base_frame) {
		        glong start, size;
			start = MAX(frame2pixel(selection_begin),0);
			size = MAX(frame2pixel_ratio(selection_end - MAX(selection_begin, eeadv->base_frame)), 1);
			
			gdk_draw_pixmap (real_pixmap,
					 widget->style->fg_gc[GTK_STATE_SELECTED],
					 pixmap,
					 start,
					 0,
					 start,
					 0,
					 size,
					 eeadv->view_height);
		}
		gdk_gc_unref(gc);

		if (eeadv->view_selected) gdk_pixmap_unref(eeadv->view_selected);
		eeadv->view_selected = pixmap;


		gdk_draw_pixmap (widget->window,
				widget->style->fg_gc[GTK_STATE_NORMAL],
				real_pixmap,
				0, 0, 0, 0, eeadv->view_width, eeadv->view_height);
		
		gdk_gc_unref(real_gc);
		gdk_pixmap_unref(real_pixmap);

		eears_adview_paint_markers(eeadv);
	} 
	return FALSE;
}

void 
eears_adview_update_insertion_point(EEarsAdView *eeadv, glong point)
{
	GdkGC *gc;
	gint pos;
	
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	/* check that the point is in bounds */
	point = MAX(0, MIN( point, eears->adm->get_framecount(eeadv->mad)) );

	pos = frame2pixel( eeadv-> insertion_point );

/* 	g_print("pos: %d %d\n", pos, point); */
	if (GTK_WIDGET(eeadv)->window) {
		gc = gdk_gc_new(GTK_WIDGET(eeadv)->window);

		gdk_gc_set_foreground(gc, eeadv->insertion_point_color);
		gdk_gc_set_function(gc, GDK_INVERT);
	
		/* erase previous line */
		if (pos >= 0 && pos < eeadv->view_width && eeadv->insertion_point >=0 )
			gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, pos, 0, pos, eeadv->view_height);
	}

	eeadv->insertion_point = point;
	/* the null string marker is a special marker for the insertion point. */
	eears->msm->set_marker( eeadv->mm, "", point); 
	
	if (GTK_WIDGET(eeadv)->window) {
		pos = frame2pixel( eeadv-> insertion_point );
		/* draw line */
		if (pos >= 0 && pos < eeadv->view_width)
			gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, pos, 0, pos, eeadv->view_height);

		gdk_gc_unref(gc);
	}

	gtk_signal_emit (GTK_OBJECT (eeadv), eeadv_signals[INSERTION_POINT_CHANGED]);		
}

void 
eears_adview_set_marker(EEarsAdView *eeadv, glong point)
{
	GdkGC *gc;
	gint pos;
	const gchar *marker_name;
	
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	/* check that the point is in bounds */
	g_return_if_fail (point >= 0 && point <= eears->adm->get_framecount(eeadv->mad));

/* 	g_print("marker %d\n", point); */
	
	marker_name = eears->msm->set_unique_marker(eeadv->mm, point); 

	gc = gdk_gc_new(GTK_WIDGET(eeadv)->window);

	gdk_gc_set_foreground(gc, eeadv->marker_color);
	
	pos = frame2pixel( point );
	/* draw line */
	if (pos >= 0 && pos < eeadv->view_width) {
		EEarsAdViewMarkerItem *m;
				
		gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, pos, 0, pos, eeadv->view_height);
		gdk_draw_string(GTK_WIDGET(eeadv)->window, eeadv->marker_font, gc, pos + 5, 15, marker_name);

		m = g_new(EEarsAdViewMarkerItem, 1);

		m->pos = pos;
		m->name = g_strdup(marker_name);
		eeadv->markers_in_view = g_list_prepend( eeadv->markers_in_view, m );
	}
	gdk_gc_unref(gc);
}

void eears_adview_paint_markers(EEarsAdView *eeadv)
{
	glong pos;
	GdkGC *gc;
	GList *l;

	/* paint the markers */
	l = eears -> msm -> get_markers_in_range(eeadv->mm,
						 eeadv->base_frame,
						 eeadv->base_frame + eeadv->view_width * eeadv->mult);

	gc = gdk_gc_new(GTK_WIDGET(eeadv)->window);
	gdk_color_alloc(gtk_widget_get_colormap(GTK_WIDGET(eeadv)), eeadv->marker_color);
	gdk_gc_set_foreground(gc, eeadv->marker_color);

	/* free the old visible markers list */
	while (eeadv->markers_in_view) {
		if (eeadv->markers_in_view->data) {
			g_free(((EEarsAdViewMarkerItem*)eeadv->markers_in_view->data)->name);
			g_free(eeadv->markers_in_view->data);
		}
		eeadv->markers_in_view = g_list_remove_link( eeadv->markers_in_view, 
							     eeadv->markers_in_view);
	}

	while (l) {
		gchar *name;
		name = (gchar*) l->data;
		pos = frame2pixel( eears -> msm -> get_marker(eeadv->mm, name) );
		/* draw line, but skip the cursor and out-of range
                   markers, which shouldn't be there anyway. */
		if (strlen(name) > 0 && pos >= 0 && pos < eeadv->view_width) {
			EEarsAdViewMarkerItem *m;
				
			gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, pos, 0, pos, eeadv->view_height);
			gdk_draw_string(GTK_WIDGET(eeadv)->window, eeadv->marker_font, gc, pos + 5, 15, name);

			/* add elem to visible markers list (used by motion routine) */
			m = g_new(EEarsAdViewMarkerItem, 1);

			m->pos = pos;
			m->name = g_strdup(name);

			eeadv->markers_in_view = g_list_prepend( eeadv->markers_in_view, m );
		}
		l = g_list_next(l);
	}

	gdk_gc_unref(gc);

	/* paint the insertion point marker */
	pos = frame2pixel( eears -> msm -> get_marker(eeadv->mm, "") );
	
	gc = gdk_gc_new(GTK_WIDGET(eeadv)->window);
		
	gdk_gc_set_foreground(gc, eeadv->insertion_point_color);
	gdk_gc_set_function(gc, GDK_INVERT);
	
	if (pos >= 0 && pos < eeadv->view_width && eeadv->insertion_point >=0 )
		gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, pos, 0, pos, eeadv->view_height);
	
	gdk_gc_unref(gc);

}

void
eears_adview_set_single_channel_color(EEarsAdView *eeadv, GdkColor *color)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_color_free(eeadv->single_channel_color);
	eeadv->single_channel_color = gdk_color_copy(color);
}

void
eears_adview_set_right_channel_color(EEarsAdView *eeadv, GdkColor *color)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_color_free(eeadv->right_channel_color);
	eeadv->right_channel_color = gdk_color_copy(color);
}

void
eears_adview_set_left_channel_color(EEarsAdView *eeadv, GdkColor *color)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_color_free(eeadv->left_channel_color);
	eeadv->left_channel_color = gdk_color_copy(color);
}

glong eears_adview_get_cursor_frame (EEarsAdView *eeadv)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	return eeadv->cursor_frame;
}

void eears_adview_set_marker_color	        (EEarsAdView *eeadv, 
						 GdkColor *color)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_color_free(eeadv->marker_color);
	eeadv->marker_color = gdk_color_copy(color);
}

void eears_adview_set_marker_font 	        (EEarsAdView *eeadv, 
						 gchar* font)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_font_unref(eeadv->marker_font);
	eeadv->marker_font = gdk_font_load(font);
}

void eears_adview_set_insertion_point_color	(EEarsAdView *eeadv, 
						 GdkColor *color)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	gdk_color_free(eeadv->insertion_point_color);
	eeadv->insertion_point_color = gdk_color_copy(color);
}

glong eears_adview_get_insertion_point  (EEarsAdView *eeadv)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	return eeadv->insertion_point;
}

void eears_adview_set_zoom(EEarsAdView *eeadv, gfloat zoom)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	eeadv->zoom = zoom;
	
	if (eeadv->mad)
		eeadv->mult = MAX(1,((gfloat)eears->adm->get_framecount(eeadv->mad))/GTK_WIDGET(eeadv)->allocation.width/eeadv->zoom);

}

gfloat  eears_adview_get_zoom(EEarsAdView *eeadv)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	return eeadv->zoom;
}

void eears_adview_set_base_frame(EEarsAdView *eeadv, glong base)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	eeadv->base_frame = base;
}

glong  eears_adview_get_base_frame(EEarsAdView *eeadv)
{
	g_assert(eeadv);
	g_assert(IS_EEARS_ADVIEW(eeadv));

	return eeadv->base_frame;
}


#if 0
void
eears_adview_start_play(EEarsAdView *eeadv)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));

	if (eeadv->timer)
	{
		g_timer_destroy(eeadv->timer);
	}
	eeadv->timer = g_timer_new();
	g_timer_start(eeadv->timer);
	eeadv->playline_tag = gtk_idle_add(eears_adview_update_playline, eeadv);
}


void
eears_adview_stop_play(EEarsAdView *eeadv)
{
	g_return_if_fail (eeadv != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW (eeadv));
	/*g_print("stop\n");*/

	gtk_idle_remove(eeadv->playline_tag);
	if (eeadv->timer) g_timer_destroy(eeadv->timer);
	eeadv->timer = NULL;
}

static gint 
eears_adview_update_playline (gpointer data)
{
	EEarsAdView *eeadv;
	AudioData *ad;
	gdouble tmpbegin, tmpend, tmp;
	gint h, w;
	GdkGC *gc;


	/*g_print("update\n");*/
	g_return_val_if_fail(data!=NULL, 0);
	g_return_val_if_fail (IS_EEARS_ADVIEW (data), 0);

	eeadv = EEARS_ADVIEW(data);
	ad = eeadv->ad;

	w = GTK_WIDGET(eeadv)->allocation.width;
	h = GTK_WIDGET(eeadv)->allocation.height;

	/* g_print("x: %d, y: %d\n", w, h); */
	tmpbegin = 0.0;
	tmpend = (double) w;

	if (tmpend < 0) return 1;

	tmp  = ((double)(w) * g_timer_elapsed(eeadv->timer, NULL)) /
		((double) ad->size / ((double)(ad->rate * (ad->bits/8) * ad->channels)));


	/*g_print("tmp: %f, tmpbegin: %f, tmpend: %f\n", tmp, tmpbegin, tmpend);*/
	if ((tmp+tmpbegin)>tmpend) 
	{
		/*       g_print("stop da update\n");*/
		eears_adview_stop_play(eeadv);
		return 0;
	}
	if (eeadv->x_playline_cur == (tmpbegin+tmp))
	{
		return 1;
	}

	gdk_draw_pixmap (GTK_WIDGET(eeadv)->window, 
			GTK_WIDGET(eeadv)->style->fg_gc[GTK_STATE_NORMAL], 
			eeadv->view,
			0, 0, 0, 0, w, h);

	gc = gdk_gc_new(GTK_WIDGET(eeadv)->window);
	gdk_color_alloc(gtk_widget_get_colormap(GTK_WIDGET(eeadv)), eeadv->single_channel_color);
	gdk_gc_set_foreground(gc, eeadv->single_channel_color);

	gdk_draw_line(GTK_WIDGET(eeadv)->window, gc, tmpbegin+tmp, 0, tmpbegin+tmp+1, h);
	eeadv->x_playline_cur = tmpbegin+tmp;

	return 1;
}
#endif

gfloat eears_adview_get_magnification(EEarsAdView *eeadv, MagnifyUnits unit)
{
	MetaAudioData mad= eeadv->mad;
	AudioDataMethods *adm= eears->adm;
	
	switch(unit)
	{
	case ZOOM_UNIT_FPP:  
		return(eeadv->mult);
		break;
	case ZOOM_UNIT_WPS:
		return((adm->get_framecount(mad))/
		       (eeadv->mult)/(GTK_WIDGET(eeadv)->allocation.width));
		break;
	case ZOOM_UNIT_SPW:
		return((eeadv->mult)*(GTK_WIDGET(eeadv)->allocation.width)/
		       (adm->get_rate(mad)));
		break;
	}
	return(0.0);
}


void eears_adview_set_magnification(EEarsAdView *eeadv, gdouble value,
                                    MagnifyUnits unit)
{
	gdouble width,framecount,
		mult=1.0,zoom=1.0;
	MetaAudioData mad= eeadv->mad;
	AudioDataMethods *adm= eears->adm;
	
	// set hi-use values
	width=(GTK_WIDGET(eeadv)->allocation.width);
	// width= EEARS_ADVIEW(eeadvs->eeadv)->view_width;
	framecount=adm->get_framecount(mad);
	
	switch(unit)
	{
	case ZOOM_UNIT_FPP:  
		mult=value;
		break;
		
	case ZOOM_UNIT_WPS:
		mult=framecount/width/value;
		break;
	case ZOOM_UNIT_SPW:
		mult=(adm->get_rate(mad)) * value /width;
		break;
	default:
		mult=1.0;
		break;
	}
	
	mult=MAX(1.0,mult);
	if(width>0.0)  // only test if displayed
	{
		mult=MIN(mult,framecount/width);
		zoom=framecount/width/mult;
		// fix_scrole_stuff();
	}
	eeadv->mult=mult;
	eeadv->zoom=zoom;
	// EEARS_ADVIEW_SCROLLED(obj) 
	return ;
}


void eears_adview_set_window_range(EEarsAdView *eeadv, glong first_frame,
                                   glong last_frame)
{
	if(first_frame >= 0 && last_frame >=0)
	{
		glong frames;
		frames = eears->adm->get_framecount(eeadv->mad);

		g_assert(first_frame <= last_frame);
		// fix last_frame <= end of sample
		// width= EEARS_ADVIEW(eeadvs->eeadv)->view_width;
		eears_adview_set_base_frame(eeadv,first_frame);
		eears_adview_set_magnification(eeadv,
					       ((glong)(last_frame-first_frame))/
					       (GTK_WIDGET(eeadv)->allocation.width),
					       ZOOM_UNIT_FPP);
		eeadv->scroll_adj->value = eeadv -> base_frame;
		eeadv->scroll_adj->step_increment = ((gfloat)frames) / eeadv->zoom / 10;
		eeadv->scroll_adj->page_increment = ((gfloat)frames) / eeadv->zoom;
		eeadv->scroll_adj->page_size = ((gfloat)frames) / eeadv->zoom;
		gtk_signal_emit_by_name (GTK_OBJECT (eeadv->scroll_adj), "changed");
		eeadv->zoom_adj->value =  eeadv->zoom;
		gtk_signal_emit_by_name (GTK_OBJECT (eeadv->zoom_adj), "value_changed");
		return ;
	}
}
