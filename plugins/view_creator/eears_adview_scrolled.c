/* Electric Ears
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <math.h>
#include "metadefs.h"
#include "eears.h"
#include "eears_adview.h"
#include "eears_adview_scrolled.h"

#define EVENT_METHOD(i, x) GTK_WIDGET_CLASS(GTK_OBJECT(i)->klass)->x

extern EEars *eears;

/* This widget is actually a vbox which contains a hruler, a eeadv, a
 * hscrollbar, and some other convenience. The widget also links the
 * various childs together to react with each-other. It is possible to
 * set the zoom value in the zoom combo, and the scrollbar influences
 * the eeadv. The eeadv also notifies the Position entry about the
 * position of the mouse pointer.
 */

/* Forward declarations */

static void	eears_adview_scrolled_class_init	(EEarsAdViewScrolledClass    *klass);
static void 	eears_adview_scrolled_init		(EEarsAdViewScrolled     *eeadv);
static void 	eears_adview_scrolled_destroy		(GtkObject       *object);

static void     eears_adview_scrolled_read_configuration_values(EEarsAdViewScrolled *eeadvs);

static void     eears_adview_scrolled_set_hruler_range  (EEarsAdViewScrolled *eeadvs);
static void     eears_adview_scrolled_on_spinbuttonZoom_changed
                                                        (GtkEditable     *editable,
							 EEarsAdViewScrolled *eeadvs);
/* static void     eears_adview_scrolled_on_buttonPosition_clicked */
/*                                                         (GtkButton       *button,  */
/* 							 gpointer         user_data); */
/* static void     eears_adview_scrolled_on_entryPosition_changed  */
/*                                                         (GtkEditable     *editable,  */
/* 							 gpointer         user_data); */
static void     eears_adview_scrolled_on_position_changed
                                                        (EEarsAdViewScrolled *eeadvs);
static void     eears_adview_scrolled_on_insertion_point_changed
                                                        (EEarsAdViewScrolled *eeadvs);
static gint     eears_adview_scrolled_print_frame       (EEarsAdViewScrolled *eeadvs, 
							 gchar *buf, glong pos);

static void     eears_hruler_custom_draw_ticks          (GtkRuler *ruler);

/* Local data */

static GtkVBoxClass *parent_class = NULL;

guint
eears_adview_scrolled_get_type ()
{
  static guint adview_scrolled_type = 0;

  if (!adview_scrolled_type)
    {
      GtkTypeInfo adview_scrolled_info =
      {
	"EEarsAdViewScrolled",
	sizeof (EEarsAdViewScrolled),
	sizeof (EEarsAdViewScrolledClass),
	(GtkClassInitFunc) eears_adview_scrolled_class_init,
	(GtkObjectInitFunc) eears_adview_scrolled_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };

      adview_scrolled_type = gtk_type_unique (gtk_vbox_get_type (), &adview_scrolled_info);
    }

  return adview_scrolled_type;
}

static void
eears_adview_scrolled_class_init (EEarsAdViewScrolledClass *class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) class;
	widget_class = (GtkWidgetClass*) class;

	parent_class = gtk_type_class (gtk_vbox_get_type ());

	object_class->destroy = eears_adview_scrolled_destroy;

	//widget_class->realize = eears_adview_realize;
}


static void
eears_adview_scrolled_init (EEarsAdViewScrolled *eeadvs)
{
}

GtkWidget*
eears_adview_scrolled_new (EEarsViewInfo *evi)
{
        gchar buf[32];
        GtkWidget *vseparator1;
        EEarsAdViewScrolled *eeadvs;
        g_assert(evi);

        eeadvs = gtk_type_new (eears_adview_scrolled_get_type ());

        eeadvs->evi = evi;

	eeadvs->hruler = gtk_hruler_new ();

	eeadvs->adjustment = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 0, 0, 0, 0));
	eeadvs->spinbuttonZoom_adj = GTK_ADJUSTMENT (gtk_adjustment_new (1, 1, 200000, 1, 10, 10));
	eeadvs->statusbarView = gtk_statusbar_new ();
	gtk_widget_ref (eeadvs->statusbarView);

	eeadvs->eeadv = EEARS_ADVIEW(eears_adview_new( eeadvs->evi,
						       eeadvs->adjustment,
						       eeadvs->spinbuttonZoom_adj,
						       GTK_STATUSBAR(eeadvs->statusbarView)));

	eeadvs->hscrollbar = gtk_hscrollbar_new (eeadvs->adjustment);
	/*gtk_range_set_update_policy(GTK_RANGE(eeadvs->hscrollbar), GTK_UPDATE_DELAYED);
	 */
	gtk_signal_connect_object( GTK_OBJECT(eeadvs->eeadv), "cursor_frame_changed",
				   (GtkSignalFunc)EVENT_METHOD(eeadvs->hruler, motion_notify_event),
				   GTK_OBJECT(eeadvs->hruler) );
	GTK_RULER_CLASS(GTK_OBJECT(eeadvs->hruler)->klass)->draw_ticks = eears_hruler_custom_draw_ticks;


	gtk_signal_connect_object_after( GTK_OBJECT(eeadvs->eeadv), "expose_event",
				   GTK_SIGNAL_FUNC (eears_adview_scrolled_set_hruler_range),
				   GTK_OBJECT(eeadvs) );

	
	gtk_box_pack_start (GTK_BOX (eeadvs), eeadvs->hruler, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (eeadvs), GTK_WIDGET(eeadvs->eeadv), TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (eeadvs), eeadvs->hscrollbar, FALSE, TRUE, 0);

	gtk_widget_show (eeadvs->hscrollbar);
	gtk_widget_show (eeadvs->hruler);
	gtk_widget_show (GTK_WIDGET(eeadvs->eeadv));

	eeadvs->hboxStatus = gtk_hbox_new (FALSE, 4);
	gtk_widget_ref (eeadvs->hboxStatus);
	gtk_widget_show (eeadvs->hboxStatus);
	gtk_box_pack_start (GTK_BOX (eeadvs), eeadvs->hboxStatus, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (eeadvs->hboxStatus), 2);

	eeadvs->labelFramerate = gtk_label_new ("");
	gtk_widget_ref (eeadvs->labelFramerate);
	gtk_widget_show (eeadvs->labelFramerate);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->labelFramerate, FALSE, FALSE, 0);
  
	sprintf(buf,"%dHz", eears->adm->get_rate(eeadvs->evi->mad));
	/*gtk_label_set_text(GTK_LABEL(GTK_BIN (eeadvs->labelFramerate)->child), buf);*/
	gtk_label_set_text(GTK_LABEL(eeadvs->labelFramerate), buf);

	eeadvs->labelVoices = gtk_label_new ("");
	gtk_widget_ref (eeadvs->labelVoices);
	gtk_widget_show (eeadvs->labelVoices);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->labelVoices, FALSE, FALSE, 0);
	gtk_misc_set_padding (GTK_MISC (eeadvs->labelVoices), 4, 0);
  
	if (eears->adm->get_channels(eeadvs->evi->mad)==1)
		gtk_label_set_text(GTK_LABEL(eeadvs->labelVoices), _("mono"));
	else 
		gtk_label_set_text(GTK_LABEL(eeadvs->labelVoices), _("stereo"));

	eeadvs->labelBits = gtk_label_new ("");
	gtk_widget_ref (eeadvs->labelBits);
	gtk_widget_show (eeadvs->labelBits);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->labelBits, FALSE, FALSE, 0);
	if (eears->adm->get_bits(eeadvs->evi->mad)==8)
		gtk_label_set_text(GTK_LABEL(eeadvs->labelBits), _("8 bits"));
	else 
		gtk_label_set_text(GTK_LABEL(eeadvs->labelBits), _("16 bits"));

	vseparator1 = gtk_vseparator_new ();
	gtk_widget_ref (vseparator1);
	gtk_widget_show (vseparator1);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), vseparator1, FALSE, TRUE, 0);

	/*****************************************************************************/

	eeadvs->labelPosition = gtk_label_new ("");
	gtk_widget_ref (eeadvs->labelPosition);
	gtk_widget_show (eeadvs->labelPosition);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus),
			    eeadvs->labelPosition, FALSE, FALSE, 0);

	eeadvs->entryPosition = gtk_entry_new ();
	gtk_widget_ref (eeadvs->entryPosition);
	gtk_widget_show (eeadvs->entryPosition);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus),
			    eeadvs->entryPosition, FALSE, TRUE, 0);
	gtk_entry_set_editable (GTK_ENTRY (eeadvs->entryPosition), FALSE);

	eears_adview_scrolled_print_frame(eeadvs, buf, 0);
	gtk_entry_set_text (GTK_ENTRY (eeadvs->entryPosition), buf);
	gtk_widget_set_usize (eeadvs->entryPosition, 100, -2);
	gtk_signal_connect_object( GTK_OBJECT(eeadvs->eeadv), "cursor_frame_changed",
				   GTK_SIGNAL_FUNC (eears_adview_scrolled_on_position_changed),
				   GTK_OBJECT(eeadvs) );

	eeadvs->labelInsPoint = gtk_label_new (_("Position:"));
	gtk_widget_ref (eeadvs->labelInsPoint);
	gtk_widget_show (eeadvs->labelInsPoint);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), 
			    eeadvs->labelInsPoint, FALSE, FALSE, 0);

	eeadvs->entryInsPoint = gtk_entry_new ();
	gtk_widget_ref (eeadvs->entryInsPoint);
	gtk_widget_show (eeadvs->entryInsPoint);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), 
			    eeadvs->entryInsPoint, FALSE, TRUE, 0);
	gtk_entry_set_editable (GTK_ENTRY (eeadvs->entryInsPoint), FALSE);

	eears_adview_scrolled_print_frame(eeadvs, buf, 0);
	gtk_entry_set_text (GTK_ENTRY (eeadvs->entryInsPoint), buf);
	gtk_widget_set_usize (eeadvs->entryInsPoint, 100, -2);
	gtk_signal_connect_object( GTK_OBJECT(eeadvs->eeadv), "insertion_point_changed",
				   GTK_SIGNAL_FUNC (eears_adview_scrolled_on_insertion_point_changed),
				   GTK_OBJECT(eeadvs) );

	/*****************************************************************************/

	eeadvs->labelZoom = gtk_label_new (_("Zoom x:"));
	gtk_widget_ref (eeadvs->labelZoom);
	gtk_widget_show (eeadvs->labelZoom);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->labelZoom, FALSE, FALSE, 0);
	gtk_misc_set_padding (GTK_MISC (eeadvs->labelZoom), 4, 0);

	eeadvs->spinbuttonZoom = gtk_spin_button_new (GTK_ADJUSTMENT (eeadvs->spinbuttonZoom_adj), 1, 0);
	gtk_widget_ref (eeadvs->spinbuttonZoom);
	gtk_widget_show (eeadvs->spinbuttonZoom);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->spinbuttonZoom, FALSE, TRUE, 0);
	gtk_widget_set_usize (eeadvs->spinbuttonZoom, 74, -2);
	gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eeadvs->spinbuttonZoom), TRUE);

	gtk_widget_show (eeadvs->statusbarView);
	gtk_box_pack_start (GTK_BOX (eeadvs->hboxStatus), eeadvs->statusbarView, TRUE, TRUE, 0);

	/*
   gtk_signal_connect (GTK_OBJECT (eeadvs->buttonFramerate), "clicked",
                      GTK_SIGNAL_FUNC (on_buttonFramerate_clicked),
                      NULL);
  
	gtk_signal_connect (GTK_OBJECT (eeadvs->buttonPosition), "clicked",
			    GTK_SIGNAL_FUNC (eears_adview_scrolled_on_buttonPosition_clicked),
			    eeadvs);*/
/* 	gtk_signal_connect (GTK_OBJECT (eeadvs->entryPosition), "changed", */
/* 			    GTK_SIGNAL_FUNC (eears_adview_scrolled_on_entryPosition_changed), */
/* 			    eeadvs); */
	gtk_signal_connect (GTK_OBJECT (eeadvs->spinbuttonZoom), "changed",
			    GTK_SIGNAL_FUNC (eears_adview_scrolled_on_spinbuttonZoom_changed),
			    eeadvs);

	/******************************************************************/

	/* Read in default configuration values 
	 */	
	eears_adview_scrolled_read_configuration_values(eeadvs);

	/* recalculate the upper adjustment value */
	gtk_signal_emit_by_name (GTK_OBJECT (eeadvs->spinbuttonZoom_adj), "changed");

        return GTK_WIDGET (eeadvs);
}


static void eears_adview_scrolled_read_configuration_values(EEarsAdViewScrolled *eeadvs)
{
	GdkColor temp;
	gdouble r,g,b;

	gtk_adjustment_set_value( GTK_ADJUSTMENT(eeadvs->spinbuttonZoom_adj),
				  eears -> csm -> get_float( mc, "zoom" ));
	gtk_signal_emit_by_name (GTK_OBJECT (eeadvs->spinbuttonZoom_adj), "changed");

	sscanf( eears -> csm -> get_color( mc, "left_chan_color" ),
		"%lf %lf %lf", &r, &g, &b);
	temp.red = r*(0xffff); temp.green = g*(0xffff); temp.blue = b*(0xffff);
	eears_adview_set_left_channel_color(eeadvs -> eeadv, &temp);

	sscanf( eears -> csm -> get_color( mc, "right_chan_color" ),
		"%lf %lf %lf", &r, &g, &b);
	temp.red = r*(0xffff); temp.green = g*(0xffff); temp.blue = b*(0xffff);
	eears_adview_set_right_channel_color(eeadvs -> eeadv, &temp);

	sscanf( eears -> csm -> get_color( mc, "single_chan_color" ),
		"%lf %lf %lf", &r, &g, &b);
	temp.red = r*(0xffff); temp.green = g*(0xffff); temp.blue = b*(0xffff);
	eears_adview_set_single_channel_color(eeadvs -> eeadv, &temp);

	sscanf( eears -> csm -> get_color( mc, "insertion_point_color" ),
		"%lf %lf %lf", &r, &g, &b);
	temp.red = r*(0xffff); temp.green = g*(0xffff); temp.blue = b*(0xffff);
	eears_adview_set_insertion_point_color(eeadvs -> eeadv, &temp);

	sscanf( eears -> csm -> get_color( mc, "marker_color" ),
		"%lf %lf %lf", &r, &g, &b);
	temp.red = r*(0xffff); temp.green = g*(0xffff); temp.blue = b*(0xffff);
	eears_adview_set_marker_color(eeadvs -> eeadv, &temp);

	eears_adview_set_marker_font(eeadvs -> eeadv, 
				     eears -> csm -> get_string( mc, "marker_font" ));
}

void eears_adview_scrolled_update(EEarsAdViewScrolled *eeadvs)
{
	AudioDataMethods *adm;
	glong frames;
	adm = eears->adm;
	frames = adm->get_framecount(eeadvs->evi->mad);

// 	g_print("base frame %d", eeadvs->eeadv->base_frame); 
	eeadvs->eeadv->scroll_adj->value = MAX(0, MIN( frames, eeadvs->eeadv->scroll_adj->value));
	eeadvs->eeadv->scroll_adj->upper = frames;
	eears_adview_scrolled_set_hruler_range( eeadvs ); 
	gtk_signal_emit_by_name (GTK_OBJECT (eeadvs->eeadv->scroll_adj), "changed");

}

static gint eears_adview_scrolled_print_frame(EEarsAdViewScrolled *eeadvs, gchar *buf, glong pos)
{
	AudioDataMethods *adm;
        glong rate;

	adm = eears->adm;
        rate = adm->get_rate(eeadvs->evi->mad);

        return sprintf(buf,"%02ld:%02ld:%02ld.%05ld", 
		       pos / rate / 3600, 
		      (pos / rate / 60) % 60,
		      (pos / rate ) % 60,
		      (pos % rate));
		      
}


/* static void */
/* eears_adview_scrolled_on_buttonPosition_clicked(GtkButton       *button, */
/*                                         gpointer         user_data) */
/* { */

/* } */


/* static void */
/* eears_adview_scrolled_on_entryPosition_changed(GtkEditable     *editable, */
/*                                         gpointer         user_data) */
/* { */

/* } */

static void
eears_adview_scrolled_on_spinbuttonZoom_changed(GtkEditable     *editable,
                                        EEarsAdViewScrolled *eeadvs)
{
        gfloat z;
//	GtkAllocation *allocation;
	AudioDataMethods *adm;
	GtkAdjustment *sb_adj;
	
	adm = eears->adm;

	g_assert(eeadvs);
	g_assert(editable);

	z = gtk_spin_button_get_value_as_float( GTK_SPIN_BUTTON( editable ) );
//	allocation = &(GTK_WIDGET(eeadvs->eeadv)->allocation);

	eears_adview_set_zoom( eeadvs->eeadv, z);
//	eeadvs->eeadv->zoom = ((gfloat)adm->get_framecount(eeadvs->eeadv->mad))/allocation->width/z;
//	if (eeadvs->eeadv->zoom < 1) eeadvs->eeadv->zoom = 1;

//	g_print("z: %f %f", z, eeadvs->eeadv->zoom);

	eeadvs->adjustment->step_increment = ((gfloat)adm->get_framecount(eeadvs->evi->mad))/z/10;
	eeadvs->adjustment->page_increment = ((gfloat)adm->get_framecount(eeadvs->evi->mad))/z;
	eeadvs->adjustment->page_size = ((gfloat)adm->get_framecount(eeadvs->evi->mad))/z;
	eears_adview_scrolled_set_hruler_range( eeadvs );
	gtk_signal_emit_by_name (GTK_OBJECT (eeadvs->adjustment), "changed");

	/* update spinbutton adjustment */

	sb_adj = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( editable ) );
	sb_adj->page_increment = pow(10, floor(log10(z-1)));
	sb_adj->upper = ((gfloat)eears->adm->get_framecount(eeadvs->evi->mad))/EEARS_ADVIEW(eeadvs->eeadv)->view_width;

}

static void
eears_adview_scrolled_on_position_changed( EEarsAdViewScrolled *eeadvs)
{
        gchar buf[32];

	g_assert(eeadvs);
	g_assert(IS_EEARS_ADVIEW_SCROLLED(eeadvs));

	eears_adview_scrolled_print_frame(eeadvs, buf,
					  eears_adview_get_cursor_frame( eeadvs->eeadv));
	gtk_entry_set_text (GTK_ENTRY (eeadvs->entryPosition), buf);
}

static void
eears_adview_scrolled_on_insertion_point_changed( EEarsAdViewScrolled *eeadvs)
{
        gchar buf[32];

	g_assert(eeadvs);
	g_assert(IS_EEARS_ADVIEW_SCROLLED(eeadvs));

	eears_adview_scrolled_print_frame(eeadvs, buf,
					  eears_adview_get_insertion_point( eeadvs->eeadv));
	gtk_entry_set_text (GTK_ENTRY (eeadvs->entryInsPoint), buf);
}


static void
eears_adview_scrolled_set_hruler_range( EEarsAdViewScrolled *eeadvs)
{
	AudioDataMethods *adm;
        gint rate, framecount;
        gfloat lower,upper;

	adm = eears->adm;
        rate = adm->get_rate(eeadvs->evi->mad);
	framecount = adm->get_framecount(eeadvs->evi->mad);
  
        /* unit is seconds */
        lower = (gfloat)eears_adview_get_base_frame(eeadvs -> eeadv)/(gfloat)rate;
        upper = ((gfloat)eears_adview_get_base_frame(eeadvs -> eeadv) + ((gfloat)framecount) / eears_adview_get_zoom(eeadvs->eeadv))/rate;

/* 	g_print("%d %f %f %f\n",framecount,lower,upper, eears_adview_get_zoom(eeadvs->eeadv)); */

        gtk_ruler_set_range (GTK_RULER (eeadvs->hruler), lower, upper, GTK_RULER (eeadvs->hruler)->position, upper);
}


static void
eears_adview_scrolled_destroy (GtkObject *object)
{
        EEarsAdViewScrolled *eeadvs;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_EEARS_ADVIEW_SCROLLED (object));

	eeadvs = EEARS_ADVIEW_SCROLLED (object);
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
	  (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/*******************************************************************
 * the following function is taken from gtk+-1.2.6/gtk/gtkhruler.c
 * long live to free software!
 * It implements an hruler with awkward labels (hrs:min:sec) instead 
 * of integers. TODO : generalize and contribute back.
 */


#define RULER_HEIGHT          14
#define MINIMUM_INCR          5
#define MAXIMUM_SUBDIVIDE     5
#define MAXIMUM_SCALES        10

#define ROUND(x) ((int) ((x) + 0.5))
#define hms_sprintf(buf, i) sprintf((buf),"%02d:%02d:%02d",(i)/3600, ((i)/60) %60, (i)%60)


static void eears_hruler_custom_draw_ticks(GtkRuler *ruler)
{
  GtkWidget *widget;
  GdkGC *gc, *bg_gc;
  GdkFont *font;
  gint i;
  gint width, height;
  gint xthickness;
  gint ythickness;
  gint length, ideal_length;
  gfloat lower, upper;		/* Upper and lower limits, in ruler units */
  gfloat increment;		/* Number of pixels per unit */
  gint scale;			/* Number of units per major unit */
  gfloat subd_incr;
  gfloat start, end, cur;
  gchar unit_str[32];
  gint digit_height;
  gint text_width;
  gint pos;

  g_return_if_fail (ruler != NULL);
  g_return_if_fail (GTK_IS_HRULER (ruler));

  if (!GTK_WIDGET_DRAWABLE (ruler)) 
    return;

  widget = GTK_WIDGET (ruler);

  gc = widget->style->fg_gc[GTK_STATE_NORMAL];
  bg_gc = widget->style->bg_gc[GTK_STATE_NORMAL];
  font = widget->style->font;

  xthickness = widget->style->klass->xthickness;
  ythickness = widget->style->klass->ythickness;
  digit_height = font->ascent; /* assume descent == 0 ? */

  width = widget->allocation.width;
  height = widget->allocation.height - ythickness * 2;

   
   gtk_paint_box (widget->style, ruler->backing_store,
		  GTK_STATE_NORMAL, GTK_SHADOW_OUT, 
		  NULL, widget, "hruler",
		  0, 0, 
		  widget->allocation.width, widget->allocation.height);


   gdk_draw_line (ruler->backing_store, gc,
		 xthickness,
		 height + ythickness,
		 widget->allocation.width - xthickness,
		 height + ythickness);

  upper = ruler->upper / ruler->metric->pixels_per_unit;
  lower = ruler->lower / ruler->metric->pixels_per_unit;

  if ((upper - lower) == 0) 
    return;
  increment = (gfloat) width / (upper - lower);

  /* determine the scale
   *  We calculate the text size as for the vruler instead of using
   *  text_width = gdk_string_width(font, unit_str), so that the result
   *  for the scale looks consistent with an accompanying vruler
   */
  scale = ceil (ruler->max_size / ruler->metric->pixels_per_unit);
  hms_sprintf (unit_str, scale);
  text_width = strlen (unit_str) * digit_height + 1;

  for (scale = 0; scale < MAXIMUM_SCALES; scale++)
    if (ruler->metric->ruler_scale[scale] * fabs(increment) > 2 * text_width)
      break;

  if (scale == MAXIMUM_SCALES)
    scale = MAXIMUM_SCALES - 1;

  /* drawing starts here */
  length = 0;
  for (i = MAXIMUM_SUBDIVIDE - 1; i >= 0; i--)
    {
      subd_incr = (gfloat) ruler->metric->ruler_scale[scale] / 
	          (gfloat) ruler->metric->subdivide[i];
      if (subd_incr * fabs(increment) <= MINIMUM_INCR) 
	continue;

      /* Calculate the length of the tickmarks. Make sure that
       * this length increases for each set of ticks
       */
      ideal_length = height / (i + 1) - 1;
      if (ideal_length > ++length)
	length = ideal_length;

      if (lower < upper)
	{
	  start = floor (lower / subd_incr) * subd_incr;
	  end   = ceil  (upper / subd_incr) * subd_incr;
	}
      else
	{
	  start = floor (upper / subd_incr) * subd_incr;
	  end   = ceil  (lower / subd_incr) * subd_incr;
	}

  
      for (cur = start; cur <= end; cur += subd_incr)
	{
	  pos = ROUND ((cur - lower) * increment);

	  gdk_draw_line (ruler->backing_store, gc,
			 pos, height + ythickness, 
			 pos, height - length + ythickness);

	  /* draw label */
	  if (i == 0)
	    {
	      hms_sprintf (unit_str, (int) cur);
	      gdk_draw_string (ruler->backing_store, font, gc,
			       pos + 2, ythickness + font->ascent - 1,
			       unit_str);
	    }
	}
    }
}

