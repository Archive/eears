/*
 * Electric Ears
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include "plugins.h"
#include "metadefs.h"
#include "eears_adview.h"
#include "eears_adview_scrolled.h"
#include "eears.h"

extern EEars *eears;
MetaConfiguration mc;

/**************************************************************************/

gboolean init_plugin (PluginData *pd);
static gboolean configure_plugin (PluginData *pd);
GtkWidget* view_creator (GnomeMDIChild *child, gpointer data);
void cur_view_update(void);


GtkWidget* view_creator (GnomeMDIChild *child, gpointer data)
{
	return eears_adview_scrolled_new((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child)));
}

void cur_view_update(void)
{
	EEarsAdViewScrolled *eeadvs;
	eeadvs = EEARS_ADVIEW_SCROLLED(gnome_mdi_get_active_view (eears->mdi));

	eears_adview_scrolled_update(eeadvs);
}


/**************************************************************************/

static PluginIdentifier ident[] = { 
	{ 0x00000100, "view_creator"},
	{ 0, NULL}
};

static PluginIdentifier deps[] = { 
	{ 0x00000100, "audiodata_system"},
	{ 0x00000100, "configuration_system"},
	{ 0, NULL}
};

static PluginGate gates[] = {
	{ "view_creator", view_creator },
	{ "cur_view_update", cur_view_update },
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = configure_plugin;
	pd->title = "Default View Creator";
	pd->author = "A. Bosio and G. Iachello";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}

static gboolean configure_plugin (PluginData *pd)
{
/*   MetaConfiguration mc2; */
	/* Register the configurable parameters 
	 */
	mc = eears-> csm -> create( "View Creator", _("Sample View Options"), _("You may change here the default startup options for the sample view. Note that changes will only affect samples opened from now on, and not already open samples."));
	eears -> csm -> register_item( mc, "float", "zoom=1", "1 200000 1", 
				       _("Default Zoom Value"), 
				       _("Here you can customize the default zoom value"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "int", "magstep=50", "1 1 100", 
				       _("Default Magnification Step"), 
				       _("Here you can customize the default magnification step value (percentage)"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "color", "single_chan_color=0.0 0.0 1.0", NULL, 
					_("Single Channel Color"), 
					_("Here you can customize the default color for single-channel samples"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "color", "left_chan_color=0.0 0.0 1.0", NULL,
					_("Left Channel Color"), 
					_("Here you can customize the default color for the left channel of stereo samples"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "color", "right_chan_color=1.0 0.0 0.0", NULL,
					_("Right Channel Color"), 
					_("Here you can customize the default color for the right channel of stereo samples"),
				       NULL, NULL );

	eears -> csm -> register_item( mc, "color", "insertion_point_color=0.0 0.0 0.0", NULL,
					_("Insertion Point Color"), 
					_("Here you can customize the default color for the insertion point bar"),
				       NULL, NULL );

	eears -> csm -> register_item( mc, "color", "marker_color=0.0 1.0 0.0", NULL,
					_("Marker Color"), 
					_("Here you can customize the default color for the marker bars"),
				       NULL, NULL );

	eears -> csm -> register_item( mc, "font", "marker_font=" DEFAULT_MARKER_FONTNAME, NULL,
					_("Marker Font"), 
					_("Here you can customize the default font for the marker labels"),
				       NULL, NULL );

/* 	mc2 = eears-> csm -> new( "Test config 2", "Another config panel", NULL); */

/* 	eears -> csm -> register_item( mc2, "bool", "test_bool=true", NULL, */
/* 					_("Test bool"),  */
/* 					NULL, */
/* 				       NULL, NULL ); */
/* 	eears -> csm -> register_item( mc2, "string", "test_string=ciao!", "9", */
/* 					_("Test string"),  */
/* 					NULL, */
/* 				       NULL, NULL ); */
/* 	eears -> csm -> register_item( mc2, "int", "test_int=13", "1 1 10", */
/* 					_("Test string"),  */
/* 					NULL, */
/* 				       NULL, NULL ); */
/* 	eears-> csm -> new( "Test config 3", "Still another", NULL); */
/* 	eears-> csm -> new( "Test config 4", NULL, NULL); */

	return TRUE;
}



