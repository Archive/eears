/* Electric Ears
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __EEARS_ADVIEW_H__
#define __EEARS_ADVIEW_H__

#undef REAL_USE_GTHREADS

#include <gnome.h>
#include "eears.h"

#ifndef REAL_USE_GTHREADS
/* 
 * The pthread code is stolen from gears code, written by Rusty Chris Holleman.
 */
#include <pthread.h>

typedef void* pthread_start_routine_t(void*); 

#endif


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_EEARS_ADVIEW	   (eears_adview_get_type())
#define EEARS_ADVIEW(obj)          (GTK_CHECK_CAST (obj, TYPE_EEARS_ADVIEW, EEarsAdView))
#define EEARS_ADVIEW_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), TYPE_EEARS_ADVIEW, EEarsAdViewClass))
#define IS_EEARS_ADVIEW(obj)       (GTK_CHECK_TYPE ((obj), TYPE_EEARS_ADVIEW))
#define IS_EEARS_ADVIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), TYPE_EEARS_ADVIEW))


typedef struct _EEarsAdView    		EEarsAdView;
typedef struct _EEarsAdViewClass   	EEarsAdViewClass;
typedef struct _EEarsAdViewCache	EEarsAdViewCache;
typedef struct _EEarsAdViewCacheItem	EEarsAdViewCacheItem;
typedef struct _EEarsAdViewMarkerItem   EEarsAdViewMarkerItem;
typedef enum   _EEarsAdViewCursorMode   EEarsAdViewCursorMode;
	
struct _EEarsAdViewCacheItem
{
	guint8 left1;
	guint8 left2;
	guint8 right1;
	guint8 right2;
};


struct _EEarsAdViewCache
{
	glong begin_frame;
	glong end_frame;
        gdouble zoom;
        
        glong size;

        /* the big cache is built by a separate thread, which signals 
	 * the readiness of the cache with this flag 
	 */
        gboolean ready;
	EEarsAdViewCacheItem	*items;
};

struct _EEarsAdViewMarkerItem
{
	glong pos;
	gchar *name;
};


enum _EEarsAdViewCursorMode
{
	CM_NORMAL = 0,
	CM_SELECTING = 1,
	CM_OVER_MARKER = 2,
	CM_MODES = 3
};

struct _EEarsAdView
{
	GtkWidget widget;

	MetaAudioData 	mad;
	MetaSelection ms;
	MetaMarkers mm;

	GdkPixmap*		view;
	GdkPixmap*		view_selected;
	gint			view_height;
	gint			view_width;
	GTimer* 		last_update;

#ifdef REAL_USE_GTHREADS
	GThread *blc_thread;
#else
	pthread_t blc_tid;
	pthread_mutex_t large_cache_mutex;
#endif
	
	EEarsAdViewCache large_cache;
	EEarsAdViewCache fast_cache;

	GdkColor *insertion_point_color;
	GdkColor *marker_color;
	GdkColor *single_channel_color;
	GdkColor *right_channel_color;
	GdkColor *left_channel_color;

	GdkFont  *marker_font;
	
	GList *markers_in_view;

        /* this value is the value of the frame under the mouse cursor */
        glong cursor_frame;
        /* this value is the value of the fixed frame */
        glong insertion_point;

        GtkAdjustment *scroll_adj;
        GtkAdjustment *zoom_adj;

	GtkStatusbar *statusbar;

        /* the zoom of the sample, in 1/zoom sample per view */
        gdouble zoom;
	/* the inverse zoom of the sample in frames per pixel */
	gdouble mult;

	/* internal selection */
	glong selection_end, selection_begin;
	
	GdkCursor *cursors[CM_MODES];
	EEarsAdViewCursorMode cursor_mode;
	
        /* the first (leftmost) displayed frame */
        glong base_frame;  
        /* TRUE if we want to force a backing bitmap redraw */ 
        gboolean force_bitmap_redraw;
 
        GtkWidget *marker_popup_menu; 
        GtkWidget *normal_popup_menu; 

	GTimer *timer;
	gint playline_tag;
	gint x_playline_cur;
};

struct _EEarsAdViewClass
{
	GtkWidgetClass parent_class;
        void (* cursor_frame_changed)  (EEarsAdView *eeadv);
        void (* insertion_point_changed)  (EEarsAdView *eeadv);

};




GtkWidget*     eears_adview_new                    (EEarsViewInfo *evi, 
						    GtkAdjustment *scroll_adj, 
						    GtkAdjustment *zoom_adj,
						    GtkStatusbar *statusbar);
GtkType        eears_adview_get_type               (void);

void eears_adview_set_left_channel_color	(EEarsAdView *eeadv, 
						 GdkColor *color);
void eears_adview_set_right_channel_color	(EEarsAdView *eeadv, 
						 GdkColor *color);
void eears_adview_set_single_channel_color	(EEarsAdView *eeadv, 
						 GdkColor *color);
void eears_adview_set_marker_color	        (EEarsAdView *eeadv, 
						 GdkColor *color);
void eears_adview_set_insertion_point_color	(EEarsAdView *eeadv, 
						 GdkColor *color);
void eears_adview_set_marker_font 	        (EEarsAdView *eeadv, 
						 gchar* font);

glong eears_adview_get_cursor_frame     (EEarsAdView *eeadv);
glong eears_adview_get_insertion_point  (EEarsAdView *eeadv);
void eears_adview_set_zoom              (EEarsAdView *eeadv, gfloat zoom);
void eears_adview_set_base_frame        (EEarsAdView *eeadv, glong base);
glong  eears_adview_get_base_frame      (EEarsAdView *eeadv);
gfloat eears_adview_get_zoom            (EEarsAdView *eeadv);
	
gint eears_adview_expose (GtkWidget      *widget,
		 	GdkEventExpose *event);

void eears_adview_start_play(EEarsAdView *eeadv);
void eears_adview_stop_play(EEarsAdView *eeadv);

void eears_adview_adj_changed( EEarsAdView *eeadv);

//////
enum _MagnifyUnits
{
	ZOOM_UNIT_FPP, // frames per pixel
	ZOOM_UNIT_WPS, // windows per sample
	ZOOM_UNIT_SPW // seconds per window

};

typedef enum  _MagnifyUnits MagnifyUnits;

extern MetaConfiguration mc;

// void eears_adview_zoom_selected(EEarsAdView *eeadv);
// void eears_adview_select_visible(EEarsAdView *eeadv);
gfloat eears_adview_get_magnification(EEarsAdView *eeadv, 
                                      MagnifyUnits unit);
void eears_adview_set_magnification  (EEarsAdView *eeadv, gdouble value,
				      MagnifyUnits unit);
void eears_adview_set_window_range(EEarsAdView *eeadv, glong first_frame,
                                   glong last_frame);

///////

#ifdef __cplusplus
}
#endif /* __cplusplus */

#define DEFAULT_MARKER_FONTNAME "-*-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*"

#endif /* __EEARS_ADVIEW_H__  */
