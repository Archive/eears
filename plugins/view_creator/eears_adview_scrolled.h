 /* -*- Mode: C; tab-width: 8; indent-tabs-mode: t;  -*- */

/* Electric Ears
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __EEARS_ADVIEW_SCROLLED_H__
#define __EEARS_ADVIEW_SCROLLED_H__

#include <gnome.h>
#include "eears.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_EEARS_ADVIEW_SCROLLED	   (eears_adview_scrolled_get_type())
#define EEARS_ADVIEW_SCROLLED(obj)          (GTK_CHECK_CAST (obj, TYPE_EEARS_ADVIEW_SCROLLED, EEarsAdViewScrolled))
#define EEARS_ADVIEW_SCROLLED_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), TYPE_EEARS_ADVIEW_SCROLLED, EEarsAdViewScrolledClass))
#define IS_EEARS_ADVIEW_SCROLLED(obj)       (GTK_CHECK_TYPE ((obj), TYPE_EEARS_ADVIEW_SCROLLED))
#define IS_EEARS_ADVIEW_SCROLLED_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), TYPE_EEARS_ADVIEW_SCROLLED))


typedef struct _EEarsAdViewScrolled    		EEarsAdViewScrolled;
typedef struct _EEarsAdViewScrolledClass   	EEarsAdViewScrolledClass;


struct _EEarsAdViewScrolled
{
	GtkVBox vbox;

	EEarsViewInfo 	*evi;
	GtkWidget *hruler;
	GtkWidget *hscrollbar;
        EEarsAdView *eeadv;

        GtkWidget *hboxStatus;
        GtkWidget *labelFramerate;
        GtkWidget *labelVoices;
        GtkWidget *labelBits;
        GtkWidget *labelPosition;
        GtkWidget *entryPosition;
        GtkWidget *labelInsPoint;
        GtkWidget *entryInsPoint;
        GtkWidget *labelZoom;
        GtkAdjustment *spinbuttonZoom_adj;
        GtkWidget *spinbuttonZoom;
        GtkWidget *statusbarView;
       
        GtkAdjustment *adjustment;
};

struct _EEarsAdViewScrolledClass
{
	GtkVBoxClass parent_class;
};

extern MetaConfiguration mc;



GtkWidget*     eears_adview_scrolled_new        (EEarsViewInfo *evi);
GtkType        eears_adview_scrolled_get_type   (void);

void           eears_adview_scrolled_update     (EEarsAdViewScrolled *eeadvs);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EEARS_ADVIEW_SCROLLED_H__  */
