/* Electric Ears Sample Record subsystem
 * Copyright (C) 1999 G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Parts of this code are ripped from sound-monitor 0.7.1 by John
 * Ellis <johne@bellatlantic.net> 
 *
 */
#ifndef _DIALOG_INTERFACE_H_
#define _DIALOG_INTERFACE_H_

#include "mad_recorder.h"

GtkWidget* create_dialogRecord (RecordInfo *ri);

#endif
