#include <gnome.h>


void
on_buttonRecord_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonPause_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_button3_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_recParms_changed                     (GtkWidget      *widget,
					 gpointer         user_data);
