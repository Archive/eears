/* Electric Ears Sample Record subsystem
 * Copyright (C) 1999 G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Parts of this code are ripped from sound-monitor 0.7.1 by John
 * Ellis <johne@bellatlantic.net> 
 *
 */

#include <config.h>
#include <gnome.h>
#include <esd.h>
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"
#include "mad_recorder.h"
#include "dialog_interface.h"
#include "support.h"

extern EEars *eears;
static MetaConfiguration mc;

gboolean init_plugin (PluginData *pd);
static gboolean configure_plugin (PluginData *pd);
static MetaAudioData *produce(void);

GnomeDialog *dialogRecord;


RecordInfo ri;

/****************************************************************************/


void update_meters(gpointer data) // , gint source, GdkInputCondition condition)
{
        RecordInfo *ri;
	unsigned char buffer[ESD_BUF_SIZE];
	register gint i;
	register short val_l, val_r;
	static unsigned short bigl, bigr;
	gint framesize;
	gfloat l,r;
	int read_bytes;

	ri = (RecordInfo *)data;

	g_assert(ri);

	framesize = 1;
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS16) framesize <<= 1;
	if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) framesize <<= 1;

	if (!(read_bytes=read (ri->stream, buffer, ESD_BUF_SIZE)))
		{
		g_print("...nothing");
		return;
		}

	val_l = val_r = 0;
	bigl = 0;
	bigr = 0;
	//g_print("buf %d %d\n",read_bytes,framesize);
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS8) {
	        for (i = 0; i < read_bytes; i+=framesize) {
		        val_l = abs( ((signed short)(buffer[i])) - 128 );
			bigl = ( val_l > bigl ? val_l : bigl);
			if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) {
			  val_r = abs( ((signed short)(buffer[i+1])) - 128 );
			  bigr = ( val_r > bigr ? val_r : bigr);
			}
		}
	} else {
	        for (i = 0; i < read_bytes; i+=framesize) {
		        val_l = abs (*((signed short*) (buffer+i)));
			bigl = ( val_l > bigl ? val_l : bigl);
			if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) {
			  val_r = abs (*((signed short*) (buffer+i+2)));
			  bigr = ( val_r > bigr ? val_r : bigr);
			}
		}
	}
	/* ok, these values are stored, and will be updated in the
	   next screen redraw update, this will be improved, but for now...
	   the idea is to allow a screen refresh rate slower than the input rate */
	//	g_print("pau %d %d\n",bigl,bigr);
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS8) {
	  l = ((float)bigl)/128;
	  r = ((float)bigr)/128;
	} else {
	  l = ((float)bigl)/128/256;
	  r = ((float)bigr)/128/256;
	}
	gtk_progress_bar_update(ri->lBar, l);

	gtk_progress_bar_update(ri->rBar, r);	
}

void record_data(gpointer data, gint source, GdkInputCondition condition)
{
        RecordInfo *ri;
	unsigned char buffer[ESD_BUF_SIZE];
	gchar buf[32];
	register gint i;
	register short val_l, val_r;
	static unsigned short bigl, bigr;
	int read_bytes;
	gfloat l,r;
	gint framesize;
	gint write_ret;
	
	ri = (RecordInfo *)data;

	g_assert(ri);

	framesize = 1;
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS16) framesize <<= 1;
	if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) framesize <<= 1;

	if (!(read_bytes=read (ri->stream, buffer, ESD_BUF_SIZE)))
		{
		printf("...nothing");
		return;
		}

	/* dump the bytes */
	write_ret =  eears -> adm -> write(ri->mad, buffer, read_bytes / framesize);
	if (write_ret < 0) {
		g_warning("write error while recording.");
	} else ri->framecount += write_ret;

	sprintf(buf,"%02ld:%02ld:%02ld.%05ld", 
		ri->framecount / ri->rate / 3600, 
		(ri->framecount / ri->rate / 60) % 60,
		(ri->framecount / ri->rate ) % 60,
		(ri->framecount % ri->rate));
	gtk_entry_set_text(ri->timeEntry, buf);
	
	/******************************************************************/
	/* display the bars */

	bigl = 0; 
	bigr = 0;
	val_l = val_r = 0;
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS8) {
	        for (i = 0; i < read_bytes; i+=framesize) {
		        val_l = abs( ((signed short)(buffer[i])) - 128 );
			bigl = ( val_l > bigl ? val_l : bigl);
			if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) {
			  val_r = abs( ((signed short)(buffer[i+1])) - 128 );
			  bigr = ( val_r > bigr ? val_r : bigr);
			}
		}
	} else {
	        for (i = 0; i < read_bytes; i+=framesize) {
		        val_l = abs (*((signed short*) (buffer+i)));
			bigl = ( val_l > bigl ? val_l : bigl);
			if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) {
			  val_r = abs (*((signed short*) (buffer+i+2)));
			  bigr = ( val_r > bigr ? val_r : bigr);
			}
		}
	}
	/* ok, these values are stored, and will be updated in the
	   next screen redraw update, this will be improved, but for now...
	   the idea is to allow a screen refresh rate slower than the input rate */
	if ((ri->format & ESD_MASK_BITS) == ESD_BITS8) {
	  l = ((float)bigl)/128;
	  r = ((float)bigr)/128;
	} else {
	  l = ((float)bigl)/128/256;
	  r = ((float)bigr)/128/256;
	}
	gtk_progress_bar_update(ri->lBar, l);

	gtk_progress_bar_update(ri->rBar, r);	
}

static MetaAudioData *produce(void)
{
	ri.format = ESD_STREAM|ESD_RECORD;

	/* get configuration settings */
	ri.rate = atoi( eears-> csm -> get_string(mc, "rate"));
	if (atoi(eears-> csm -> get_string(mc, "channels"))==1)
	  ri.format |= ESD_MONO;
	else 
	  ri.format |= ESD_STEREO;
	if (atoi(eears-> csm -> get_string(mc, "bits"))==8)
	  ri.format |= ESD_BITS8;
	else 
	  ri.format |= ESD_BITS16;

	ri.stream = esd_record_stream_fallback(ri.format, ri.rate, NULL, NULL);

	
        dialogRecord = GNOME_DIALOG(create_dialogRecord(&ri));

	ri.rBar = GTK_PROGRESS_BAR(lookup_widget(GTK_WIDGET(dialogRecord), "progressbarVolumeRight"));
	gtk_progress_bar_set_bar_style(ri.rBar,GTK_PROGRESS_CONTINUOUS);
	ri.lBar = GTK_PROGRESS_BAR(lookup_widget(GTK_WIDGET(dialogRecord), "progressbarVolumeLeft"));
	gtk_progress_bar_set_bar_style(ri.lBar,GTK_PROGRESS_CONTINUOUS);
	ri.timeEntry = GTK_ENTRY(lookup_widget(GTK_WIDGET(dialogRecord), "entrySampleSize"));

	if (ri.stream)
          ri.input_cb_id = gtk_idle_add(update_meters, (gpointer) &ri);
	
	
	ri.mad = NULL;
	ri.framecount = 0;
	
	gnome_dialog_run_and_close(dialogRecord);
	
	if (ri.input_cb_id > -1)
          gtk_idle_remove(ri.input_cb_id);
	esd_close (ri.stream);
	
	return ri.mad;
}

static gboolean configure_plugin (PluginData *pd)
{
	mc = eears-> csm -> create( "Recorder", _("Sample Record Options"), _("You may change here the default startup options for the sample record dialog. Note that changes will affect recordings from now on."));
	eears -> csm -> register_item( mc, "selection", "bits=8", "8\n16\n", 
				       _("Default bits"), 
				       _("Here you can customize the default bits number for the recorded samples"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "selection", "channels=8", "1\n2\n", 
				       _("Default channels number"), 
				       _("Here you can customize the default channel number for the recorded samples"),
				       NULL, NULL );
	eears -> csm -> register_item( mc, "selection", "rate=44100", "4000\n8000\n11025\n16000\n22050\n44100\n48000\n", 
				       _("Default frequency"), 
				       _("Here you can customize the default sample frequency for the recorded samples"),
				       NULL, NULL );
        return TRUE;
}

static PluginIdentifier ident[] = { 
	{ 0x00000100, "audiodata_producer"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{ 0x00000100, "audiodata_system"},
	{0, NULL}
};

static PluginGate gates[] = {
	{ "produce", produce },
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = configure_plugin;
	pd->title = "Esound Recorder";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}



