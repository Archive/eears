/* Electric Ears Sample Record subsystem
 * Copyright (C) 1999 G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _MAD_RECORDER_H_
#define _MAD_RECORDER_H_

#include <esd.h>
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"

typedef struct _RecordInfo RecordInfo;

struct _RecordInfo {
  GtkProgressBar *rBar, *lBar;
  GtkEntry* timeEntry;
  int stream;
  gint rate;
  esd_format_t format;
  gint input_cb_id;
  MetaAudioData *mad;
  glong framecount;
};

extern  RecordInfo ri;

void update_meters(gpointer data); //, gint source, GdkInputCondition condition);

void record_data(gpointer data, gint source, GdkInputCondition condition);


#endif
