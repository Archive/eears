#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <esd.h>

#include "dialog_callbacks.h"
#include "dialog_interface.h"
#include "support.h"
#include "mad_recorder.h"

extern GnomeDialog *dialogRecord;
extern EEars *eears;

void
on_buttonRecord_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *widget;

  widget = lookup_widget(GTK_WIDGET(dialogRecord), "radiobuttonStereo");
  gtk_widget_set_sensitive(widget, FALSE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "radiobuttonMono");
  gtk_widget_set_sensitive(widget, FALSE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "radiobutton8bits");
  gtk_widget_set_sensitive(widget, FALSE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "radiobutton16bits");
  gtk_widget_set_sensitive(widget, FALSE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "comboRate");
  gtk_widget_set_sensitive(widget, FALSE);

  widget = lookup_widget(GTK_WIDGET(dialogRecord), "buttonRecord");
  gtk_widget_set_sensitive(widget, FALSE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "buttonPause");
  gtk_widget_set_sensitive(widget, TRUE);

  if (ri.input_cb_id > -1) 
    gtk_idle_remove(ri.input_cb_id);

/*   g_print("record %d %x\n", ri.rate, ri.format); */
  if (!ri.mad)
    ri.mad = eears -> adm -> create( ri.rate, 
				  ((ri.format&ESD_MASK_BITS) == ESD_BITS8 ? 8 : 16),
				  ((ri.format&ESD_MASK_CHAN) == ESD_MONO ? 1 : 2));

  if (ri.stream)
    ri.input_cb_id = 
      gdk_input_add( ri.stream, GDK_INPUT_READ, 
		     (GdkInputFunction)record_data, (gpointer)&ri);
}


void
on_buttonPause_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *widget;

  widget = lookup_widget(GTK_WIDGET(dialogRecord), "buttonRecord");
  gtk_widget_set_sensitive(widget, TRUE);
  widget = lookup_widget(GTK_WIDGET(dialogRecord), "buttonPause");
  gtk_widget_set_sensitive(widget, FALSE);

  if (ri.input_cb_id > -1)
    gdk_input_remove(ri.input_cb_id);

/*   g_print("pause\n"); */

  if (ri.stream)
    ri.input_cb_id = gtk_idle_add(update_meters, (gpointer) &ri);

}


void
on_button1_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_button3_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
/*   g_print("cancel record!\n"); */
  if (ri.mad)
    eears->adm->close( ri.mad );

  ri.mad = NULL;
}

void
on_recParms_changed                     (GtkWidget      *widget,
                                        gpointer         user_data)
{
  esd_format_t format = ESD_STREAM|ESD_RECORD;
  gint rate;

  GtkWidget *w, *combo, *button16bits, *buttonStereo;

  buttonStereo = lookup_widget(GTK_WIDGET(dialogRecord), "radiobuttonStereo");
  button16bits = lookup_widget(GTK_WIDGET(dialogRecord), "radiobutton16bits");
  combo = lookup_widget(GTK_WIDGET(dialogRecord), "comboRate");

  esd_close (ri.stream);

  rate = atoi(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(combo)->entry)));
  if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(button16bits)))
    format |= ESD_BITS16;
  else 
    format |= ESD_BITS8;

  if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(buttonStereo))) {
    format |= ESD_STEREO;
    w = lookup_widget(GTK_WIDGET(dialogRecord), "progressbarVolumeRight");
    gtk_widget_show(w);
    w = lookup_widget(GTK_WIDGET(dialogRecord), "labelVolumeRight");
    gtk_widget_show(w);
    w = lookup_widget(GTK_WIDGET(dialogRecord), "labelVolumeLeft");
    gtk_label_set_text(GTK_LABEL(w),_("Left"));
  } else { 
    format |= ESD_MONO;
    w = lookup_widget(GTK_WIDGET(dialogRecord), "progressbarVolumeRight");
    gtk_widget_hide(w);
    w = lookup_widget(GTK_WIDGET(dialogRecord), "labelVolumeRight");
    gtk_widget_hide(w);
    w = lookup_widget(GTK_WIDGET(dialogRecord), "labelVolumeLeft");
    gtk_label_set_text(GTK_LABEL(w),_("Master"));
  }

/*   g_print("ch parm %d %x\n", rate, format); */

  ri.stream = esd_record_stream_fallback(format,rate, NULL, NULL);
  ri.format = format;
  ri.rate = rate;
}

