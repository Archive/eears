/* Electric Ears Sample Record subsystem
 * Copyright (C) 1999 G. Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Parts of this code are ripped from sound-monitor 0.7.1 by John
 * Ellis <johne@bellatlantic.net> 
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "dialog_callbacks.h"
#include "dialog_interface.h"
#include "support.h"

GtkWidget*
create_dialogRecord (RecordInfo *ri)
{
  GtkWidget *dialogRecord;
  GtkWidget *dialog_vbox1;
  GtkWidget *vbox1;
  GtkWidget *frame1;
  GtkWidget *table1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
  GSList *channels_group = NULL;
  GtkWidget *radiobuttonStereo;
  GtkWidget *radiobuttonMono;
  GSList *bits_group = NULL;
  GtkWidget *radiobutton8bits;
  GtkWidget *radiobutton16bits;
  GtkWidget *comboRate;
  GList *comboRate_items = NULL;
  GtkWidget *combo_entryRate;
  GtkWidget *frame3;
  GtkWidget *table2;
  GtkWidget *progressbarVolumeLeft;
  GtkWidget *progressbarVolumeRight;
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *frame4;
  GtkWidget *table3;
  GtkWidget *label8;
  GtkWidget *entrySampleSize;
  GtkWidget *hbox1;
  GtkWidget *buttonRecord;
  GtkWidget *buttonPause;
  GtkWidget *hseparator1;
  GtkWidget *dialog_action_area1;
  GtkWidget *button1;
  GtkWidget *button3;
  GtkTooltips *tooltips;

  gchar buf[32];
  
  tooltips = gtk_tooltips_new ();

  dialogRecord = gnome_dialog_new (_("Electric Ears Record"), NULL);
  gtk_object_set_data (GTK_OBJECT (dialogRecord), "dialogRecord", dialogRecord);
  gtk_window_set_policy (GTK_WINDOW (dialogRecord), FALSE, FALSE, FALSE);

  dialog_vbox1 = GNOME_DIALOG (dialogRecord)->vbox;
  gtk_object_set_data (GTK_OBJECT (dialogRecord), "dialog_vbox1", dialog_vbox1);
  gtk_widget_show (dialog_vbox1);

  vbox1 = gtk_vbox_new (FALSE, 4);
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_box_pack_start (GTK_BOX (dialog_vbox1), vbox1, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 2);

  frame1 = gtk_frame_new (_("Sample generation"));
  gtk_widget_ref (frame1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "frame1", frame1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame1);
  gtk_box_pack_start (GTK_BOX (vbox1), frame1, TRUE, TRUE, 0);

  table1 = gtk_table_new (3, 3, FALSE);
  gtk_widget_ref (table1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "table1", table1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (frame1), table1);
  gtk_container_set_border_width (GTK_CONTAINER (table1), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 4);

  label2 = gtk_label_new (_("Rate"));
  gtk_widget_ref (label2);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "label2", label2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label2);
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  label3 = gtk_label_new (_("Bits"));
  gtk_widget_ref (label3);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "label3", label3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  label4 = gtk_label_new (_("Channels"));
  gtk_widget_ref (label4);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "label4", label4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  radiobuttonStereo = gtk_radio_button_new_with_label (channels_group, _("Stereo"));
  channels_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobuttonStereo));
  gtk_widget_ref (radiobuttonStereo);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "radiobuttonStereo", radiobuttonStereo,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (radiobuttonStereo);
  gtk_table_attach (GTK_TABLE (table1), radiobuttonStereo, 1, 2, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  radiobuttonMono = gtk_radio_button_new_with_label (channels_group, _("Mono"));
  channels_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobuttonMono));
  gtk_widget_ref (radiobuttonMono);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "radiobuttonMono", radiobuttonMono,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (radiobuttonMono);
  gtk_table_attach (GTK_TABLE (table1), radiobuttonMono, 2, 3, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  if ( (ri->format & ESD_MASK_CHAN) == ESD_STEREO)
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(radiobuttonStereo), TRUE );
  else 
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(radiobuttonMono), TRUE );

  radiobutton8bits = gtk_radio_button_new_with_label (bits_group, _("8"));
  bits_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton8bits));
  gtk_widget_ref (radiobutton8bits);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "radiobutton8bits", radiobutton8bits,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (radiobutton8bits);
  gtk_table_attach (GTK_TABLE (table1), radiobutton8bits, 1, 2, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  radiobutton16bits = gtk_radio_button_new_with_label (bits_group, _("16"));
  bits_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton16bits));
  gtk_widget_ref (radiobutton16bits);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "radiobutton16bits", radiobutton16bits,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (radiobutton16bits);
  gtk_table_attach (GTK_TABLE (table1), radiobutton16bits, 2, 3, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  if ( (ri->format & ESD_MASK_BITS) == ESD_BITS8)
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(radiobutton8bits), TRUE );
  else 
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(radiobutton16bits), TRUE );

  comboRate = gtk_combo_new ();
  gtk_widget_ref (comboRate);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "comboRate", comboRate,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (comboRate);
  gtk_table_attach (GTK_TABLE (table1), comboRate, 1, 3, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  comboRate_items = g_list_append (comboRate_items, _("4000"));
  comboRate_items = g_list_append (comboRate_items, _("8000"));
  comboRate_items = g_list_append (comboRate_items, _("11025"));
  comboRate_items = g_list_append (comboRate_items, _("16000"));
  comboRate_items = g_list_append (comboRate_items, _("22050"));
  comboRate_items = g_list_append (comboRate_items, _("44100"));
  comboRate_items = g_list_append (comboRate_items, _("48000"));
  gtk_combo_set_popdown_strings (GTK_COMBO (comboRate), comboRate_items);
  g_list_free (comboRate_items);

  combo_entryRate = GTK_COMBO (comboRate)->entry;
  gtk_widget_ref (combo_entryRate);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "combo_entryRate", combo_entryRate,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (combo_entryRate);
  gtk_tooltips_set_tip (tooltips, combo_entryRate, _("Select the sample rate of the sample"), NULL);

  snprintf(buf,32, "%d", ri->rate);
  gtk_entry_set_text (GTK_ENTRY (combo_entryRate), buf);

  frame3 = gtk_frame_new (_("Volume Meter"));
  gtk_widget_ref (frame3);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "frame3", frame3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame3);
  gtk_box_pack_start (GTK_BOX (vbox1), frame3, TRUE, TRUE, 0);

  table2 = gtk_table_new (2, 2, FALSE);
  gtk_widget_ref (table2);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "table2", table2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table2);
  gtk_container_add (GTK_CONTAINER (frame3), table2);
  gtk_container_set_border_width (GTK_CONTAINER (table2), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table2), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table2), 4);

  progressbarVolumeLeft = gtk_progress_bar_new ();
  gtk_widget_ref (progressbarVolumeLeft);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "progressbarVolumeLeft", progressbarVolumeLeft,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (progressbarVolumeLeft);
  gtk_table_attach (GTK_TABLE (table2), progressbarVolumeLeft, 1, 2, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, progressbarVolumeLeft, _("Volume meter"), NULL);

  progressbarVolumeRight = gtk_progress_bar_new ();
  gtk_widget_ref (progressbarVolumeRight);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "progressbarVolumeRight", progressbarVolumeRight,
                            (GtkDestroyNotify) gtk_widget_unref);
  
  if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) 
    gtk_widget_show (progressbarVolumeRight);
  gtk_table_attach (GTK_TABLE (table2), progressbarVolumeRight, 1, 2, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, progressbarVolumeRight, _("Volume meter"), NULL);
  //  gtk_progress_configure (GTK_PROGRESS (progressbarVolumeRight), 100, 100, 100);

  label5 = gtk_label_new ((ri->format & ESD_MASK_CHAN) == ESD_STEREO ? _("Left") : _("Master"));
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "labelVolumeLeft", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table2), label5, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  label6 = gtk_label_new (_("Right"));
  gtk_widget_ref (label6);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "labelVolumeRight", label6,
                            (GtkDestroyNotify) gtk_widget_unref);
  if ((ri->format & ESD_MASK_CHAN) == ESD_STEREO) 
    gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table2), label6, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  frame4 = gtk_frame_new (_("Recording"));
  gtk_widget_ref (frame4);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "frame4", frame4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame4);
  gtk_box_pack_start (GTK_BOX (vbox1), frame4, TRUE, TRUE, 0);

  table3 = gtk_table_new (3, 2, FALSE);
  gtk_widget_ref (table3);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "table3", table3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table3);
  gtk_container_add (GTK_CONTAINER (frame4), table3);
  gtk_container_set_border_width (GTK_CONTAINER (table3), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table3), 8);
  gtk_table_set_col_spacings (GTK_TABLE (table3), 4);

  label8 = gtk_label_new (_("Sample size: "));
  gtk_widget_ref (label8);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "label8", label8,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label8);
  gtk_table_attach (GTK_TABLE (table3), label8, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  entrySampleSize = gtk_entry_new ();
  gtk_widget_ref (entrySampleSize);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "entrySampleSize", entrySampleSize,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entrySampleSize);
  gtk_table_attach (GTK_TABLE (table3), entrySampleSize, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, entrySampleSize, _("Sample size"), NULL);
  gtk_entry_set_editable (GTK_ENTRY (entrySampleSize), FALSE);

  hbox1 = gtk_hbox_new (TRUE, 4);
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_table_attach (GTK_TABLE (table3), hbox1, 0, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 2, 0);

  buttonRecord = gtk_button_new_with_label (_("Record"));
  gtk_widget_ref (buttonRecord);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "buttonRecord", buttonRecord,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (buttonRecord);
  gtk_box_pack_start (GTK_BOX (hbox1), buttonRecord, FALSE, TRUE, 0);
  gtk_tooltips_set_tip (tooltips, buttonRecord, _("Start recording sample"), NULL);

  buttonPause = gtk_button_new_with_label (_("Pause"));
  gtk_widget_ref (buttonPause);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "buttonPause", buttonPause,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (buttonPause);
  gtk_box_pack_start (GTK_BOX (hbox1), buttonPause, FALSE, TRUE, 0);
  gtk_tooltips_set_tip (tooltips, buttonPause, _("Pause or stop recording"), NULL);
  gtk_widget_set_sensitive(buttonPause, FALSE);

  hseparator1 = gtk_hseparator_new ();
  gtk_widget_ref (hseparator1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "hseparator1", hseparator1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hseparator1);
  gtk_table_attach (GTK_TABLE (table3), hseparator1, 0, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

  dialog_action_area1 = GNOME_DIALOG (dialogRecord)->action_area;
  gtk_object_set_data (GTK_OBJECT (dialogRecord), "dialog_action_area1", dialog_action_area1);
  gtk_widget_show (dialog_action_area1);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area1), 8);

  gnome_dialog_append_button (GNOME_DIALOG (dialogRecord), GNOME_STOCK_BUTTON_OK);
  button1 = g_list_last (GNOME_DIALOG (dialogRecord)->buttons)->data;
  gtk_widget_ref (button1);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "button1", button1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button1);
  GTK_WIDGET_SET_FLAGS (button1, GTK_CAN_DEFAULT);
  gtk_tooltips_set_tip (tooltips, button1, _("Accept sample for edit"), NULL);

  gnome_dialog_append_button (GNOME_DIALOG (dialogRecord), GNOME_STOCK_BUTTON_CANCEL);
  button3 = g_list_last (GNOME_DIALOG (dialogRecord)->buttons)->data;
  gtk_widget_ref (button3);
  gtk_object_set_data_full (GTK_OBJECT (dialogRecord), "button3", button3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button3);
  GTK_WIDGET_SET_FLAGS (button3, GTK_CAN_DEFAULT);
  gtk_tooltips_set_tip (tooltips, button3, _("Forget the sample"), NULL);

  gtk_signal_connect (GTK_OBJECT (buttonRecord), "clicked",
                      GTK_SIGNAL_FUNC (on_buttonRecord_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (buttonPause), "clicked",
                      GTK_SIGNAL_FUNC (on_buttonPause_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (button1), "clicked",
                      GTK_SIGNAL_FUNC (on_button1_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (button3), "clicked",
                      GTK_SIGNAL_FUNC (on_button3_clicked),
                      NULL);

/*   gtk_signal_connect (GTK_OBJECT (radiobutton8bits), "toggled", */
/*                       GTK_SIGNAL_FUNC (on_recParms_changed), */
/*                       NULL); */
  gtk_signal_connect (GTK_OBJECT (radiobutton16bits), "toggled",
                      GTK_SIGNAL_FUNC (on_recParms_changed),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (radiobuttonStereo), "toggled",
                      GTK_SIGNAL_FUNC (on_recParms_changed),
                      NULL);
/*   gtk_signal_connect (GTK_OBJECT (radiobuttonMono), "toggled", */
/*                       GTK_SIGNAL_FUNC (on_recParms_changed), */
/*                       NULL); */
  gtk_signal_connect (GTK_OBJECT (combo_entryRate), "changed",
                      GTK_SIGNAL_FUNC (on_recParms_changed),
                      NULL);

  gtk_object_set_data (GTK_OBJECT (dialogRecord), "tooltips", tooltips);

  return dialogRecord;
}

