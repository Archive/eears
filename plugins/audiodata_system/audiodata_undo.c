/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "metadefs.h"
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"
#include "audiodata_system.h"
#include "audiodata_undo.h"

#include <pthread.h>
extern pthread_mutex_t lock;


void new_undo_level(MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	ADChunkList *old;
	pthread_mutex_lock(&lock);

	g_assert(ad);
	old = adchunklist_clone(ad->cur_chunklist);
	ad->undo_chunklists = g_list_prepend(ad->undo_chunklists, old);

	pthread_mutex_unlock(&lock);
}

void undo(MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	GList *tmp;
	pthread_mutex_lock(&lock);
	g_assert(ad);
	tmp = g_list_first(ad->undo_chunklists);
	if (tmp)
	{
		ad->redo_chunklists = g_list_prepend(ad->redo_chunklists, ad->cur_chunklist);
		ad->cur_chunklist = tmp->data;
		ad->undo_chunklists = g_list_remove_link(ad->undo_chunklists, tmp);
	}
	pthread_mutex_unlock(&lock);
}

void redo(MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	GList *tmp;
	pthread_mutex_lock(&lock);
	g_assert(ad);
	tmp = g_list_first(ad->redo_chunklists);
	if (tmp)
	{
		ad->undo_chunklists = g_list_prepend(ad->undo_chunklists, ad->cur_chunklist);
		ad->cur_chunklist = tmp->data;
		ad->redo_chunklists = g_list_remove_link(ad->redo_chunklists, tmp);
	}
	pthread_mutex_unlock(&lock);
}

