/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gnome.h>
#include <audiofile.h>
#include "audiodata_utils.h"
#include "audiodata_filehandle.h"
#include "audiodata_defaults.h"


/*
 * This is the list containing all the filehandles.
 * The performances of this module could be improved using
 * a better structure, but it will be a bit more complicated
 * because you'll need to search by filename and by ADFileHandle.
 */
GList *adfh_list;

gint 		find_by_name 		(gconstpointer a, gconstpointer b);
gint 		find_by_fh 		(gconstpointer a, gconstpointer b);
ADFileHandle* 	adfilehandle_new 	(void);
void 		adfilehandle_free 	(ADFileHandle *adfh);

gint 
find_by_name (gconstpointer a, gconstpointer b)
{
	ADFileHandle *first, *second;
	g_assert(a);
	g_assert(b);
	first = (ADFileHandle *) a;
	second = (ADFileHandle *) b;

	return strcmp(first->filename, second->filename);
}
/*
gint 
find_by_fh (gconstpointer a, gconstpointer b)
{
	ADFileHandle *first, *second;
	g_assert(a);
	g_assert(b);
	first = (ADFileHandle *) a;
	second = (ADFileHandle *) b;

#ifdef DEBUG
	g_print("find_by_fh\n");
#endif
	
	return (first->fh != second->fh);
}
*/
ADFileHandle*
adfilehandle_new (void)
{
	ADFileHandle* tmp;
	tmp = g_new(ADFileHandle, 1);
	tmp->ref_count = 0;
	tmp->filename = NULL;
	tmp->to_be_deleted = FALSE;
	tmp->delayed_flush = FALSE;
	tmp->fh = NULL;
	return tmp;
}

void
adfilehandle_free (ADFileHandle *adfh)
{
	g_assert(adfh);
	if (adfh->fh!=NULL)
	{
#ifdef DEBUG
		g_print("filehandle_free: close file\n");
#endif
		if (adfh->delayed_flush) {
#ifdef DEBUG
		g_print("delayed flush in effect\n");
#endif			
		}
		afCloseFile(adfh->fh);
	}
	if (adfh->to_be_deleted) {
#ifdef DEBUG
		g_print("filehandle_free: unlink file\n");
#endif
	        unlink(adfh->filename);
	}

	g_free(adfh->filename);
	g_free(adfh);
}

/**
 * adfilehandle_ref_by_adfh:
 * @filename: a valid filehandle to reference
 * 
 * Description: this function increase the reference count for this filehandle.
 *
 * Returns: On success the function returns a valid ADFileHandle,
 * on error NULL value is returned.
 **/
ADFileHandle*
adfilehandle_ref_by_adfh (ADFileHandle *adfh)
{
/*
	ADFileHandle *fhtmp;
	GList *listtmp; */
	g_assert(adfh);

	if (adfh -> delayed_flush)
		adfilehandle_flush(adfh);

	adfh->ref_count++;
	return adfh;
/*	
	fhtmp = adfilehandle_new();
	fhtmp->fh = fh;

	if ((listtmp = g_list_find_custom(adfh_list, fhtmp, find_by_fh))!=NULL)
	{
		fhtmp->fh = NULL;
		adfilehandle_free(fhtmp);
		((ADFileHandle *)(listtmp->data))->ref_count++;
		return ((ADFileHandle *)(listtmp->data))->fh;
	} else {
		fhtmp->fh = NULL;
		adfilehandle_free(fhtmp);
		g_warning("unable to find the filehandle in filehandle list!");
		return NULL;
	}
*/
}

/**
 * adfilehandle_ref:
 * @filename: a valid filename to open (for reading or writing or both)
 * @setup:
 * 
 * Description: this function opens the file "filename".
 * If the file is not writeable, it will be opened read only.
 * If the file doesn't exist, it will be created.
 *
 * Returns: On success the function returns a valid AFfilehandle for
 * file "filename", on error NULL value is returned.
 **/
ADFileHandle *
adfilehandle_ref (const gchar *filename, AFfilesetup setup)
{
	ADFileHandle *fhtmp;
	GList *listtmp;
	g_assert(filename);

#ifdef DEBUG
	g_print("entering adfilehandle_ref\n");
#endif

	fhtmp = adfilehandle_new();
	fhtmp->filename = g_strdup(filename);

	/* if the file has a ref_count > 0, ie if the file is just registered */
	if ((listtmp = g_list_find_custom(adfh_list, fhtmp, find_by_name))!=NULL) 
	{
#ifdef DEBUG
		g_print("\tref_count > 0\n");
#endif
		adfilehandle_free(fhtmp);

		if ( ((ADFileHandle *)(listtmp->data))-> delayed_flush)
			adfilehandle_flush((ADFileHandle *)(listtmp->data));

		((ADFileHandle *)(listtmp->data))->ref_count++;
		return (ADFileHandle *)(listtmp->data);
	} else {
	/* the file is not registered yet */
#ifdef DEBUG
		g_print("\tref_count == 0\n");
#endif
		if (file_exists(filename)) {
#ifdef DEBUG
			g_print("\t\tfile exist\n");
#endif
/*
			if (file_is_writeable(filename) && file_is_readable(filename))
			{
#ifdef DEBUG
				g_print("\t\t\tfile writeable\n");
#endif
				if ((fhtmp->fh = afOpenFile(filename, "r+", NULL))!=AF_NULL_FILEHANDLE)
				{
					fhtmp->ref_count++;
					adfh_list = g_list_prepend(adfh_list, fhtmp);
					return fhtmp;
				} else {
					g_warning("unable to open writeable and readable file: %s", filename);
					fhtmp->fh = NULL;
					adfilehandle_free(fhtmp);
					return NULL;
				}
			} else */ if (file_is_readable(filename)) {
#ifdef DEBUG
				g_print("\t\t\tfile readable\n");
#endif
				if ((fhtmp->fh = afOpenFile(filename, "r", NULL))!=AF_NULL_FILEHANDLE)
				{
					fhtmp->ref_count++;
					adfh_list = g_list_prepend(adfh_list, fhtmp);
					return fhtmp;
				} else {
					g_warning("unable to open readable file: %s", filename);
					fhtmp->fh = NULL;
					adfilehandle_free(fhtmp);
					return NULL;
				}
			} else {
				g_warning("file is not readable: %s", filename);
				adfilehandle_free(fhtmp);
				return NULL;
			}
		} else {
			/* the file doesn't exist: let's create it */
#ifdef DEBUG
			g_print("\t\tfile not exist\n");
#endif

			if (setup==NULL) {
				g_warning("if the file doesn't exist, you must pass a valid setup");
				adfilehandle_free(fhtmp);
				return NULL;
			}

			if ((fhtmp->fh = afOpenFile(filename, "w", setup))!=AF_NULL_FILEHANDLE)
			{
/*				if (afCloseFile(fhtmp->fh)==-1)
				{
					g_warning("error closing file");
				}
				if ((fhtmp->fh = afOpenFile(filename, "r+", NULL))!=AF_NULL_FILEHANDLE)
				{ */
					fhtmp->ref_count++;
					adfh_list = g_list_prepend(adfh_list, fhtmp);
					return fhtmp;
/*				} else {
					g_warning("(1) unable to open nonexistant file: %s", filename);
					adfilehandle_free(fhtmp);
					return NULL;
				} */
			} else {
				g_warning("unable to open nonexistant file: %s", filename);
				adfilehandle_free(fhtmp);
				return NULL;
			}
		}
	}
}

/**
 * adfilehandle_unref:
 * @fh: a valid file handle to be unreferenced
 *
 * Description: This functions provide safe unreference to filehandles.
 *
 * Returns: TRUE on success, FALSE otherwise. 
 **/
gboolean
adfilehandle_unref (ADFileHandle *adfh)
{
	g_assert(adfh);
	g_assert(adfh->ref_count > 0);
#ifdef DEBUG
	g_print("unref!");
	g_print("adfh->ref_count: %d\n", adfh->ref_count);
#endif
	adfh->ref_count--;
	if (adfh->ref_count == 0)
	{
		adfh_list = g_list_remove(adfh_list, adfh);
		adfilehandle_free(adfh);
	}
	return TRUE;
}

/*
gboolean
adfilehandle_unref (AFfilehandle fh)
{
	ADFileHandle *fhtmp;
	GList *listtmp;
	g_assert(fh);

#ifdef DEBUG
	g_print("entering adfilehandle_unref\n");
#endif
	
	fhtmp = adfilehandle_new();
	fhtmp->fh = fh;

	if ((listtmp = g_list_find_custom(adfh_list, fhtmp, find_by_fh))!=NULL) 
	{
#ifdef DEBUG
		g_print("\tfilehandle found\n");
#endif
		fhtmp->fh = NULL;
		adfilehandle_free(fhtmp);
		if (((ADFileHandle *)(listtmp->data))->ref_count > 0) 
		{
#ifdef DEBUG
			g_print("\t\tref count > 0\n");
#endif
			((ADFileHandle *)(listtmp->data))->ref_count--;
			if (((ADFileHandle *)(listtmp->data))->ref_count==0)
			{
#ifdef DEBUG
				g_print("\t\t\tref count == 0\n");
#endif
				adfilehandle_free(listtmp->data);
				adfh_list = g_list_remove(adfh_list, listtmp->data);
			}
			return TRUE;
		} else {
			g_error("ref count should be always > 0");
			g_assert_not_reached();
			return FALSE;
		}
	} else {
#ifdef DEBUG
		g_print("\tfilehandle not found\n");
#endif
		fhtmp->fh = NULL;
		adfilehandle_free(fhtmp);
		return FALSE;
	}
}
*/

AFfilehandle    
adfilehandle_get_fh     (ADFileHandle *adfh)
{
	g_assert(adfh);
	return adfh->fh;
}

const gchar *
adfilehandle_get_filename (ADFileHandle *adfh)
{
	g_assert(adfh);
	return adfh->filename;
}

gint 
adfilehandle_get_ref_count(ADFileHandle *adfh)
{
	g_assert(adfh);
	return adfh->ref_count;
}


void
adfilehandle_set_to_be_deleted (ADFileHandle *adfh, gboolean to_be_deleted)
{
	g_assert(adfh);
	adfh->to_be_deleted = to_be_deleted;
}

gboolean
adfilehandle_get_to_be_deleted (ADFileHandle *adfh)
{
	g_assert(adfh);
	return adfh->to_be_deleted;
}


gboolean
adfilehandle_get_delayed_flush (ADFileHandle *adfh)
{
	g_assert(adfh);
	return adfh->delayed_flush;
}

/**
 * adfilehandle_flush:
 * @adfh: the filehandle to flush.
 *
 * Description: this function flushes the file. In other words, it closes
 * the writable file and reopens it readonly. 
 *
 * Returns: TRUE in case of success. FALSE otherwise.
 *
 */
gboolean
adfilehandle_flush(ADFileHandle *adfh)
{
	g_assert(adfh);
	g_assert(adfh->ref_count > 0);

	afCloseFile(adfh->fh);
	if ((adfh->fh = afOpenFile(adfh->filename, "r", NULL))==AF_NULL_FILEHANDLE) {
                g_warning("Unable to flush file"); 
		return FALSE;
	}
	adfh -> delayed_flush = FALSE;
	
	return TRUE;
}

/**
 * adfilehandle_delayed_flush:
 * @adfh: the filehandle to delayflush.
 *
 * Description: this function sets the delay flush flag. As long as
 * there is only one reference to this file, the file is kept
 * writable, to allow consecutive writes to the file. The file is at
 * this state assumed not to be readable. When someone else references
 * the file, it is automatically flushed and made readonly. If the
 * refcount is not exactly 1, the file is flushed.
 *
 * Returns: TRUE in case of success. FALSE otherwise.
 * */
gboolean
adfilehandle_delayed_flush(ADFileHandle *adfh)
{
	g_assert(adfh);
	g_assert(adfh->ref_count > 0);

	if (adfh->ref_count == 1) {
		adfh -> delayed_flush = TRUE;
	} else {
                g_warning("Unable to delay-flush file");
		adfilehandle_flush(adfh);
	}
	
	return TRUE;
}

