/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"
#include "audiodata_system.h"
#include "audiodata_utils.h"
#include "audiodata_defaults.h"
#include "eears.h"


#include <pthread.h>
extern pthread_mutex_t lock;

extern gchar *tmpfilepath;

extern EEars *eears;

static MetaAudioData   ad_init (void);



/**
 * ad_init:
 * 
 * Description: allocate a new MetaAudioData, and clear all the fields.
 * This function is not to be used from the outside, but only as helper
 * to real creation functions.
 * 
 * Returns: a well formed MetaAudioData. 
 **/
MetaAudioData   
ad_init (void)
{
	AudioData *ad;

	ad = g_new(AudioData, 1);

	ad->redo_chunklists = NULL;
	ad->cur_chunklist = NULL;
	ad->undo_chunklists = NULL;

	ad->filename = NULL;

	ad->lastmodify = g_timer_new();

	return (MetaAudioData) ad;
}

/**
 * ad_new:
 * 
 * Description: allocate a new MetaAudioData, set to it a valid
 * temp file, set to it some defaults values (bits/rate/channels),
 * and it set it to a "empty" MetaAudioData, ie framecount = 0
 * 
 * Returns: a well formed MetaAudioData. 
 **/
MetaAudioData   
ad_new (guint rate, guint bits, guint channels)
{
	AudioData *ad;
	pthread_mutex_lock(&lock);
	
	//g_error("You shouldn't get here");
	ad = ad_init();
	ad->filename = g_strdup("");
	ad->cur_chunklist = adchunklist_new_with_settings(rate, bits, channels);
	pthread_mutex_unlock(&lock);

	return (MetaAudioData) ad;
}

/**
 * ad_open:
 * @filename: a valid filename to open
 *
 * Description: it opens the file and create a proper MetaAudioData
 *
 * Returns: a well formed MetaAudioData. If any error occours, the function
 * returns NULL.
 **/
MetaAudioData   
ad_open (const gchar *filename)
{
	AudioData *ad;
	pthread_mutex_lock(&lock);
	g_assert(filename);

	if (!file_exists(filename))
	{
		g_warning("file doesn't exist: %s", filename);
		pthread_mutex_unlock(&lock);
		return NULL;
	}
	
	ad = ad_init();
	ad->cur_chunklist = adchunklist_new_from_file(filename);

	if (ad->cur_chunklist == NULL) {
/*		ad_free(ad); */
		pthread_mutex_unlock(&lock);
		return NULL;
	}
	ad->filename = g_strdup(filename);

	pthread_mutex_unlock(&lock);
	
	return ad;
}

/**
 * ad_close:
 * @mad: a valid MetaAudioData to be closed
 *
 * Description: it closes all the files related to MetaAudioData and
 * frees allocated regions.
 *
 * Returns: The function will return FALSE value on error.
 **/
gboolean
ad_close (MetaAudioData mad)
{
	AudioData *ad = (MetaAudioData) mad;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	adchunklist_free(ad->cur_chunklist);
	
	pthread_mutex_unlock(&lock);
	return TRUE;
}

/**
 * ad_free:
 * @mad: a valid MetaAudioData to be freed
 *
 * Description: it frees all allocated regions of a valid MetaAudioData.
 *
 * Returns: The function will return FALSE value on error.
 **/
gboolean
ad_free (MetaAudioData mad)
{
	AudioData *ad = (MetaAudioData) mad;
	g_assert(ad);
	pthread_mutex_lock(&lock);
	pthread_mutex_unlock(&lock);
	return TRUE;
}

/**
 * ad_clone:
 * 
 * Description: allocate a new MetaAudioData, cloned from the given
 * MetaAudioData. The two will share the same base file but not the 
 * pointers and the chunklists.
 * 
 * Returns: the cloned MetaAudioData. 
 **/
MetaAudioData   
ad_clone (MetaAudioData mad)
{
        AudioData *ad, *orig; 
	pthread_mutex_lock(&lock);

	orig = (MetaAudioData) mad;
	g_assert(orig);
	
	ad = ad_init(); 

	ad->lastmodify = g_timer_new();
	ad->filename = g_strdup( (const gchar *) orig->filename );
	ad->cur_chunklist = adchunklist_clone( orig->cur_chunklist );

	pthread_mutex_unlock(&lock);
	return (MetaAudioData) ad;
}

/**
 * ad_save:
 * @mad: a valid MetaAudioData to be saved
 *
 * Description: it saves the MetaAudioData to its associated file
 *
 * Returns: The function will return FALSE value on error.
 **/
gboolean
ad_save (MetaAudioData mad)
{
	AudioData *ad = (MetaAudioData) mad;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	adchunklist_save(ad->cur_chunklist, (const gchar *) ad->filename);

	pthread_mutex_unlock(&lock);
	return TRUE;
}

/**
 * ad_save_as:
 * @mad: a valid MetaAudioData to be saved
 * @filename: the new filename for the MetaAudioData
 *
 * Description: it saves the MetaAudioData to the new file
 *
 * Returns: The function will return FALSE value on error.
 **/
gboolean
ad_save_as  (MetaAudioData mad, const gchar *filename, const gint format)
{
	AudioData *ad = (MetaAudioData) mad;
	pthread_mutex_lock(&lock);
	g_assert(ad);

	ad->filename = strdup( filename );
	ad->cur_chunklist = adchunklist_save_as(ad->cur_chunklist, (const gchar *)ad->filename, format);
	ad->redo_chunklists = NULL;
	ad->undo_chunklists = NULL;
	ad->lastmodify = g_timer_new();
	
	pthread_mutex_unlock(&lock);
	return TRUE;
}

/**
 * ad_read:
 * @mad: a valid MetaAudioData
 * @buffer: the buffer into wich store read frames
 * @framecount: the number of frame to read
 *
 * Description: it attempts to read up to "framecount" frames from 
 * MetaAudiodata "mad" into the buffer starting at "buffer"
 *
 * Returns: On succes, the number of frames read is returned, and
 * the MetaAudioData position is advanced by this number. On error, -1
 * is returned. In  this case  it is left unspecified whether the file 
 * position (if any) changes.
 **/
glong           
ad_read (MetaAudioData mad, gpointer buffer, glong framecount)
{
	AudioData *ad = (MetaAudioData) mad;
	glong ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value =  adchunklist_read(ad->cur_chunklist, buffer, framecount);

	pthread_mutex_unlock(&lock);
	return ret_value;
}


/**
 * ad_write:
 * @mad: a valid MetaAudioData
 * @buffer: the buffer into which are stored the frames to write 
 * @framecount: the number of frames
 *
 * Description: it writes up to "framecount" frames to MetaAudioData "mad"
 * from the buffer starting at "buffer". It doesn't really write to a file
 * or something like this: for "fix" this change you should call ad_save.
 * It also reset the last_modify timer and marks the MetaAudioData dirty.
 *
 * Returns: On success, the number of frames written are returned, and
 * the MetaAudioData position is advanced by this number. On error,
 * -1 is returned.
 **/
glong
ad_write (MetaAudioData mad, gpointer buffer, glong framecount)
{
        gint written;
	AudioData *ad = (MetaAudioData) mad;
	pthread_mutex_lock(&lock);

	g_assert(ad);
	g_assert(buffer);

	if (framecount < 0) {
		pthread_mutex_unlock(&lock);
		return -1;
	}
	if (framecount == 0) {
		pthread_mutex_unlock(&lock);
		return 0;
	}

	written = adchunklist_write(ad->cur_chunklist, buffer, framecount);

	pthread_mutex_unlock(&lock);
	return written;
}

/**
 * ad_seek:
 * @mad: a valid MetaAudioData
 * @offset: the absolute offset frame to be set
 *
 * Description: it repositions the offset of the MetaAudioData to the
 * "offset" frame.
 * 
 * Returns: On succes, it returns the resulting offset location measured
 * in frames. Otherwise -1 is returned.
 **/
glong
ad_seek (MetaAudioData mad, glong offset)
{
	AudioData *ad = (MetaAudioData) mad;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	adchunklist_seek(ad->cur_chunklist, offset);

	pthread_mutex_unlock(&lock);
	return 0;
}

/**
 * ad_get_filename:
 * @mad: a valid MetaAudioData
 *
 * Description: Returns the file name associated with the audiodata.
 * 
 * Returns: the file name.
 **/
const gchar *
ad_get_filename (MetaAudioData mad)
{
	AudioData *ad = (MetaAudioData) mad;
	g_assert(ad);

	/* This function is thread safe only if a modify on the
	 * filename occurs atomically. If I'm not wrong at all
	 * it could be enough to set ad->filename as a "volatile"
	 * variable... uhm... well I'm not sure this works everywhere.
	 * GCC docs say that "volatile" is not allowed with -traditional
	 * options... maybe it would be necessary to return a strdup'ed
	 * filename (BLEAH).
	 **/
	
	return (const gchar *) ad->filename;
}

void 
ad_reset_lastmodify (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	g_timer_reset(ad->lastmodify);

	pthread_mutex_unlock(&lock);
	return;
}

gdouble 
ad_get_lastmodify (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	g_assert(ad);

	/* Read the quote in ad_get_filename */
	
	return g_timer_elapsed(ad->lastmodify, NULL);
}

glong
ad_get_framecount (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	glong ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value = adchunklist_get_framecount(ad->cur_chunklist);

	pthread_mutex_unlock(&lock);
	return ret_value;
}

gint    
ad_get_channels (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	gint ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value = adchunklist_get_channels(ad->cur_chunklist);

	pthread_mutex_unlock(&lock);
	return ret_value;
}

gint    
ad_get_bits (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	gint ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value = adchunklist_get_bits(ad->cur_chunklist);

	pthread_mutex_unlock(&lock);
	return ret_value;
}

gint    
ad_get_rate (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	gint ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value = adchunklist_get_rate(ad->cur_chunklist);

	pthread_mutex_unlock(&lock);
	return ret_value;
}

gint
ad_get_framesize (MetaAudioData mad)
{
	AudioData *ad = (AudioData *) mad;
	gint ret_value;
	g_assert(ad);
	pthread_mutex_lock(&lock);

	ret_value = adchunklist_get_channels(ad->cur_chunklist) *(adchunklist_get_bits(ad->cur_chunklist) / 8) ;

	pthread_mutex_unlock(&lock);
	return ret_value;
}

void 
ad_set_tmp_dir( const gchar *tmpdir)
{
	if (tmpfilepath)
		g_free(tmpfilepath);

	tmpfilepath = g_strdup(tmpdir);
}
