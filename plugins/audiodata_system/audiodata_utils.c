/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "audiodata_utils.h"
#include "audiodata_defaults.h"

gchar *tmpfilepath = NULL;

/**
 * create_tmp_filename:
 * @format: the format of the audio file.
 *
 * Description: it allocate a valid temporary filename.
 *
 * Returns: a valid filename.
 * 
 **/
gchar *
create_tmp_filename(gint format)
{
	static gint i = 0;
	gchar * tmpfilename;
	
	if (!tmpfilepath)
		tmpfilepath = g_strdup(g_get_tmp_dir());

	/* remove trailing slash */
	if (tmpfilepath[strlen(tmpfilepath)-1]=='/')
		tmpfilepath[strlen(tmpfilepath)-1]='\0';

	tmpfilename = g_strdup_printf("%s/%s%d.%s", tmpfilepath, 
				      AD_DEFAULT_TMPFILE_PREFIX, i, 
				      get_suffix_from_fmt(format));
	while (file_exists(tmpfilename))
	{
		i++;
		g_free(tmpfilename);
		tmpfilename = g_strdup_printf("%s/%s%d.%s", tmpfilepath, 
					      AD_DEFAULT_TMPFILE_PREFIX, i, 
					      get_suffix_from_fmt(format));
	}
	i++;

	return tmpfilename;
}

/**
 * file_exists:
 * @filename: a filename
 *
 * Description: the function checks if the file "filename" exists.
 * 
 * Returns: if the file exist, the function returns a TRUE value, FALSE
 * otherwise.
 *
 **/
gboolean
file_exists (const gchar *filename)
{
	gint fd;
	g_assert(filename);
	if ((fd = open (filename, O_RDONLY)) == -1)
	{
		if (errno == ENOENT) return FALSE;
	} else {
		close(fd);	
	}
	return TRUE;
}

/**
 * file_is_readable:
 * @filename: a valid filename
 *
 * Description: the function checks if the file "filename" is readable
 *
 * Returns: if the file is readable it returns TRUE, FALSE otherwise
 **/
gboolean
file_is_readable (const gchar *filename)
{
	gint fd;
	g_assert(filename);

	if ((fd = open(filename, O_RDONLY)) == -1) {
		if (errno == EACCES) return FALSE;
	} else {
		close(fd);	
	}
	
	return TRUE;
}

/**
 * file_is_writeable:
 * @filename: a valid filename
 *
 * Description: the function checks if the file "filename" is writeable
 *
 * Returns: if the file is writeable it returns TRUE, FALSE otherwise
 **/
gboolean
file_is_writeable (const gchar *filename)
{
	gint fd;
	g_assert(filename);

	if ((fd=open(filename, O_WRONLY)) == -1) {
		if (errno == EACCES) return FALSE;
	} else {
		close(fd);	
	}
	
	return TRUE;
}

/**
 * get_suffix_from_fmt:
 * @fmt:
 *
 * Description:
 * 
 * Returns:
 **/
const gchar*
get_suffix_from_fmt(gint fmt)
{
	switch (fmt)
	{
		case AF_FILE_WAVE:
			return "wav";
		case AF_FILE_AIFF:
			return "aiff";
		case AF_FILE_AIFFC:
			return "aiffc";
		case AF_FILE_NEXTSND:
			return "au";
		default:
			return "";
	}
}

