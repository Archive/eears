/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __AUDIODATA_LIST_H_
#define __AUDIODATA_LIST_H_

typedef struct _ADChunkList ADChunkList;

struct _ADChunkList {

	GList*		list;
	ADChunk*	cur_chunk;
	gulong 		cur_frame;

	gint format;	

	gint    bits;
	gint    rate;
	gint    channels;

	gulong  framecount;

};

void			adchunklist_print	(ADChunkList *chunklist);
	

ADChunkList*		adchunklist_new		(void);
ADChunkList*            adchunklist_new_with_settings (guint rate, 
						       guint bits, 
						       guint channels);
ADChunkList*		adchunklist_clone	(ADChunkList *list);
gboolean 		adchunklist_free	(ADChunkList *list);
glong			adchunklist_read	(ADChunkList *chunklist, 
						 gpointer buffer, 
						 glong framecount);
glong			adchunklist_write	(ADChunkList *chunklist, 
						 gpointer buffer, 
						 glong framecount);
glong			adchunklist_seek	(ADChunkList *chunklist, 
						 glong offset);

ADChunkList*		adchunklist_save	(ADChunkList *chunklist,
							const gchar* filename);
ADChunkList*		adchunklist_save_as     (ADChunkList *chunklist, 
						 const gchar *filename, 
						 const gint format);
	
ADChunkList*		adchunklist_new_from_file	(const gchar *filename);

glong			adchunklist_cut		(ADChunkList *chunklist, 
						 glong begin_frame, 
						 glong framecount);
gboolean	adchunklist_insert_chunklist_at(ADChunkList *chunklist, 
						ADChunkList *list_to_insert, 
						glong position);

	
glong	adchunklist_get_framecount	(ADChunkList *chunklist);
gint	adchunklist_get_channels	(ADChunkList *chunklist);
gint	adchunklist_get_bits		(ADChunkList *chunklist);
gint	adchunklist_get_rate		(ADChunkList *chunklist);

#endif /* __AUDIODATA_LIST_H_ */
