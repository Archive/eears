/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"
#include "audiodata_undo.h"
#include "audiodata_clipboard.h"
#include "audiodata_system.h"
#include "plugins.h"

#include <pthread.h>
pthread_mutex_t lock;

extern ADChunkList *clipboard;


gboolean init_plugin (PluginData *pd);
static void cleanup_plugin (PluginData *pd);
static gboolean configure_plugin (PluginData *pd);

static void cleanup_plugin (PluginData *pd)
{
	if (clipboard)
	{
		adchunklist_free(clipboard);
	}
}

static gboolean configure_plugin (PluginData *pd)
{
	return TRUE;
}

static PluginIdentifier ident[] = { 
	{0x00000100, "audiodata_system"},
	{0x00000100, "undo_system"},
	{0x00000100, "clipboard_system"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{0x00000100, "selection_system"},
	{0, NULL}
};

static PluginGate gates[] = {
	{"new", ad_new},			
 	{"open", ad_open},
	{"close", ad_close},
	{"free", ad_free},
	{"clone", ad_clone},
	{"save", ad_save},
	{"save_as", ad_save_as},
	{"read", ad_read},
	{"write", ad_write},
	{"seek", ad_seek},
	{"set_tmp_dir", ad_set_tmp_dir },
	{"get_lastmodify", ad_get_lastmodify },
	{"reset_lastmodify", ad_reset_lastmodify },
	{"get_filename", ad_get_filename },	
	{"get_framecount", ad_get_framecount},     	
	{"get_channels", ad_get_channels  },	
	{"get_bits", ad_get_bits },
	{"get_rate", ad_get_rate },	
	{"get_framesize", ad_get_framesize},

	{"undo", undo },
	{"redo", redo },
	{"new_undo_level", new_undo_level },

	{"cb_cut", cb_cut },
	{"cb_copy", cb_copy },
	{"cb_paste", cb_paste },
	{"cb_open", cb_open }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = cleanup_plugin;
	pd->configure = configure_plugin;
	pd->title = "Default SuperAudioData System";
	pd->author = "A. Bosio and G. Iachello";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;


	pthread_mutex_init(&lock, NULL);
	return TRUE;
}


