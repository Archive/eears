/* Electric Ears Audiodata module test suite
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "audiodata_filehandle.h"
#include "audiodata_utils.h"

int main()
{
	gchar *readable, *writeable;
	ADFileHandle *r, *w;
	AFfilesetup setup;

	readable = create_tmp_filename(AF_FILE_AIFF);
	writeable = create_tmp_filename(AF_FILE_AIFF);

	g_print("Readable: %s\n", readable);
	g_print("Writeable: %s\n", writeable);

	chmod(readable, S_IRUSR);
	chmod(writeable, S_IWUSR);

	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
	}
	afInitFileFormat(setup, AF_FILE_AIFF);
	afInitChannels(setup, AF_DEFAULT_TRACK, 2);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, 16);
	afInitRate(setup, AF_DEFAULT_TRACK, 16000);

	/* this part will call the "nonexistant file" part of ref code */
	
	if ((r = adfilehandle_ref(readable, setup))==NULL)
	{
		g_error("error referencing the readable");
		return 1;
	}

	adfilehandle_set_to_be_deleted(r, TRUE);

	if ((w = adfilehandle_ref(writeable, setup))==NULL)
	{
		g_error("error referencing the writable");
		return 2;
	}

	g_print("readable ref: %d  writable ref: %d\n", r->ref_count, w->ref_count);
	adfilehandle_set_to_be_deleted(w, TRUE);

	if ((r = adfilehandle_ref(readable, NULL))==NULL)
	{
		g_error("error referencing the readable");
		return 1;
	}

	g_print("readable ref: %d  writable ref: %d\n", r->ref_count, w->ref_count);

	if ((r = adfilehandle_ref_by_adfh(r))==NULL)
	{
		g_error("error referencing the readable");
		return 1;
	}

	g_print("readable ref: %d  writable ref: %d\n", r->ref_count, w->ref_count);
	if (!adfilehandle_unref(r))
	{
		g_error("error unreferencing the readable");
		return 3;
	}

	g_print("readable ref: %d  writable ref: %d\n", r->ref_count, w->ref_count);
	if (!adfilehandle_unref(r))
	{
		g_error("error unreferencing the readable");
		return 3;
	}


	g_print("readable ref: %d  writable ref: %d\n", r->ref_count, w->ref_count);
	if (!adfilehandle_unref(w))
	{
		g_error("error unreferencing the writeable");
		return 4;
	}

	g_print("readable ref: %d  \n", r->ref_count);
	if (!adfilehandle_unref(r))
	{
		g_error("error unreferencing the readable");
		return 3;
	}

	g_print("Ok\n");

	/* TODO: fix audiofile bug and check the rest */
	
	return 0;
}
