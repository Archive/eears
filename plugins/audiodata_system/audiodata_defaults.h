#ifndef __AUDIODATA_DEFAULTS_H_
#define __AUDIODATA_DEFAULTS_H_

#define AD_DEFAULT_BITS                 16
#define AD_DEFAULT_RATE                 44100
#define AD_DEFAULT_CHANNELS             2
#define AD_DEFAULT_TMPFILE_PREFIX       "tmpeears"
#define AD_DEFAULT_FORMAT               AF_FILE_WAVE
#define AD_DEFAULT_BUFFER_SIZE		32768

#endif /* __AUDIODATA_DEFAULTS_H_ */
