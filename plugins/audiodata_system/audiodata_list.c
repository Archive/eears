/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"
#include "audiodata_defaults.h"

void	adchunklist_rebuild(ADChunkList *chunklist);
// ADChunkList *adchunklist_clone(ADChunkList *chunklist);

void
adchunklist_print(ADChunkList *chunklist)
{
        GList *tmp;
	g_assert(chunklist);
	g_print("chunklist:\n");
	g_print("\tcur_frame: %ld\n", chunklist->cur_frame);
	g_print("\tformat: %d\n", chunklist->format);
	g_print("\tbits: %d\n", chunklist->bits);
	g_print("\trate: %d\n", chunklist->rate);
	g_print("\tchannels: %d\n", chunklist->channels);
	g_print("\tframecount: %ld\n", chunklist->framecount);

	tmp = g_list_first(chunklist->list);
	while (tmp)
	{
		adchunk_print(tmp->data);
		tmp = g_list_next(tmp);
	}
}

ADChunkList *
adchunklist_clone(ADChunkList *chunklist)
{
	ADChunkList *newchunklist;
	GList *templist;

	if (chunklist == NULL) return NULL;

	newchunklist = adchunklist_new();

	templist = chunklist->list;

	while (templist)
	{
		g_assert(templist->data);
		newchunklist->list = g_list_append(newchunklist->list, adchunk_clone(templist->data));	
		templist = templist->next;
	}
	newchunklist->cur_chunk = chunklist->cur_chunk;
	newchunklist->cur_frame = chunklist->cur_frame;
	newchunklist->format = chunklist->format;
	newchunklist->bits = chunklist->bits;
	newchunklist->channels = chunklist->channels;
	newchunklist->rate = chunklist->rate;
	newchunklist->framecount = chunklist->framecount;

	return newchunklist;
}

void
adchunklist_rebuild(ADChunkList *chunklist)
{
	GList *tmp;
	ADChunk *chunk;
	glong framecount = 0;
	g_assert(chunklist);
	tmp = chunklist->list;
	tmp = g_list_first(tmp);
		
	while (tmp)
	{
		g_assert(tmp->data); 
		chunk = tmp->data;
		chunk->virtual_offset = framecount;
		framecount += chunk->framecount;

		tmp = g_list_next(tmp);
	}

	chunklist->framecount = framecount;
}

glong
adchunklist_cut(ADChunkList *chunklist, glong begin_frame, glong framecount)
{
	ADChunk *first, *second, *tmp_chunk1, *tmp_chunk2=NULL;
	GList *listtmp;
	gint chunk_position;
	glong tmp_framecount = framecount;

	
	g_assert(chunklist);

	adchunklist_seek(chunklist, begin_frame);
	
	g_assert(chunklist->cur_chunk);
	g_assert(begin_frame < (chunklist->cur_chunk->virtual_offset + chunklist->cur_chunk->framecount));
	g_assert(begin_frame >= (chunklist->cur_chunk->virtual_offset));

	if (chunklist->cur_chunk->virtual_offset!=begin_frame)
	{
		if (!(adchunk_split_at(chunklist->cur_chunk, begin_frame, &first, &second)))
		{
			g_error("unable to split a chunk");
		}
		g_assert(first);
		g_assert(second);

		chunk_position = g_list_index(chunklist->list, chunklist->cur_chunk);
		chunklist->list = g_list_insert(chunklist->list, first, chunk_position+1);
		chunklist->list = g_list_insert(chunklist->list, second, chunk_position+2);
		chunklist->list = g_list_remove(chunklist->list, chunklist->cur_chunk);
		adchunk_free(chunklist->cur_chunk);
		chunklist->cur_chunk = second;
	}

	tmp_chunk1 = chunklist->cur_chunk;
	
	while ((tmp_chunk1) && (tmp_framecount > 0))
	{
		if (tmp_framecount >= tmp_chunk1->framecount)
		{
			tmp_framecount -= tmp_chunk1->framecount;
			listtmp = g_list_next(g_list_find(chunklist->list, tmp_chunk1));	
			if (listtmp) tmp_chunk2 = listtmp->data;
			else tmp_chunk2 = NULL;
			chunklist->list = g_list_remove(chunklist->list, tmp_chunk1);
			adchunk_free(tmp_chunk1);
			tmp_chunk1 = tmp_chunk2;
		} else {
			tmp_chunk1->offset += tmp_framecount;	
			tmp_chunk1->framecount -= tmp_framecount;
			tmp_framecount = 0;
		}
	}
	adchunklist_rebuild(chunklist);

	return framecount - tmp_framecount;
}

/**
 * adchunklist_insert_chunklist_at
 * @chunklist target chunklist
 * @list_to_insert list to insert
 * @position where to insert; if > framecount will simply concatenate
 *
 * Description:
 *
 * Returns: TRUE in case of success, FALSE if error
 * 
 **/
gboolean
adchunklist_insert_chunklist_at(ADChunkList *chunklist, ADChunkList *list_to_insert, glong position)
{
	ADChunk *first = NULL;
	ADChunk *second = NULL;
	GList *tmp;
	gint list_pos;
	gint chunk_position;
	g_assert(chunklist);
	g_assert(list_to_insert);

	if (position == 0)
	{
		tmp = g_list_first(list_to_insert->list);
		g_assert(tmp);
		g_assert(tmp->data);
		chunklist->list = g_list_prepend(chunklist->list, adchunk_clone(tmp->data));		
		list_pos = g_list_index(chunklist->list, tmp->data);
		tmp = tmp->next;
		while (tmp)
		{
			chunklist->list = g_list_insert(chunklist->list, adchunk_clone(tmp->data), list_pos);		
			list_pos = list_pos + 1;
			tmp = tmp->next;
		}
		adchunklist_rebuild(chunklist);

		return TRUE;
	}

	adchunklist_seek(chunklist, position);

	if (chunklist->cur_frame == chunklist->framecount) {
        	/* append */
		tmp = g_list_first(list_to_insert->list);
	  
		while (tmp)
		{
		        g_assert(tmp);
			g_assert(tmp->data);

			chunklist->list = g_list_append(chunklist->list, adchunk_clone(tmp->data));
			tmp = tmp->next;
		}
		adchunklist_rebuild(chunklist);

		return TRUE;

	}

	if (!(adchunk_split_at(chunklist->cur_chunk, position, &first, &second)))
	{
		g_error("unable to split a chunk");
	}
	g_assert(first);
	g_assert(second);

	chunk_position = g_list_index(chunklist->list, chunklist->cur_chunk);
	chunklist->list = g_list_insert(chunklist->list, first, chunk_position+1);
	chunklist->list = g_list_insert(chunklist->list, second, chunk_position+2);
	chunklist->list = g_list_remove(chunklist->list, chunklist->cur_chunk);
	adchunk_free(chunklist->cur_chunk);
	chunklist->cur_chunk = second;

	list_pos = g_list_index(chunklist->list, chunklist->cur_chunk);
	tmp = g_list_first(list_to_insert->list);
	while (tmp)
	{
		chunklist->list = g_list_insert(chunklist->list, adchunk_clone(tmp->data), list_pos);		
		list_pos = list_pos + 1;
		tmp = tmp->next;
	}
	adchunklist_rebuild(chunklist);
	
	return TRUE;
}


/**
 * adchunklist_write:
 * @chunklist: a valid chunklist
 * @buffer: the buffer into wich are stored the frames to write
 * @framecount: the number of frames
 *
 * Description: it writes up to "framecount" frames to chunklist from
 * the buffer starting at "buffer". It writes the samples into a wave
 * file which is then concatenated to the chunklist at the right
 * point.
 *
 * Returns: On succes, the number of frames written are returned, and
 * the MetaAudioData position is advanced by this number. On error,
 * -1 is returned.
 *
 **/
glong
adchunklist_write(ADChunkList *chunklist, gpointer buffer, glong framecount)
{
	ADChunk *new, *tmpch;
	gint seek_to, seek_res;
	g_assert(chunklist);
	g_assert(buffer);
	
	if (framecount == 0)
		return 0;
	if (framecount < 0)
	        return -1;

	/* seek to the insertion point */
	seek_to = chunklist->cur_frame;
	seek_res = adchunklist_seek(chunklist, seek_to);

	tmpch = (ADChunk*) chunklist->cur_chunk;
	
	/* if there are no chunks, or if the seek position is the beginning,
	   just create a new and prepend. */
	if (chunklist->cur_chunk == NULL || seek_res == 0 ) {
		new = adchunk_new_from_buffer( buffer, framecount, 
					       chunklist->rate, 
					       chunklist->bits, 
					       chunklist->channels,
					       AF_FILE_WAVE);

		if (!new) {
			g_warning("creating chunk from frames.");
			return -1;
		}

		chunklist->list = g_list_prepend(chunklist->list, new);
		adchunklist_rebuild( chunklist );
		chunklist->cur_frame = new->framecount;
	} else 

	/* if it is at the end, append to the
         * last chunk (if possible) */
	if (seek_res == chunklist->framecount ) {
		if (adchunk_append_to_chunk( chunklist->cur_chunk, 
					     buffer, framecount)) {
			/* update write pointer */
			/*g_print("appended");*/
			adchunklist_rebuild( chunklist );
			chunklist->cur_frame += framecount;
			return framecount;
		} else {
			/* if not possible, append a new chunk at the end */
			new = adchunk_new_from_buffer( buffer, framecount, 
						       chunklist->rate, 
						       chunklist->bits, 
						       chunklist->channels,
						       AF_FILE_WAVE);
			if (!new) {
				g_warning("creating chunk from frames.");
				return -1;
			}
			chunklist->list = g_list_append(chunklist->list, new);
			/* update write pointer */
			adchunklist_rebuild( chunklist );
			chunklist->cur_frame += new -> framecount;
		}
	} else

        /* if it is at the beginning of a chunk, append to the
         * previous chunk (if possible) */
	if (seek_res == tmpch->virtual_offset ) {
		if (adchunk_append_to_chunk( (ADChunk*)g_list_previous( g_list_find( chunklist->list, 
									   chunklist->cur_chunk)), 
					     buffer, framecount)) {
			/* update write pointer */
			g_print("appended");
			adchunklist_rebuild( chunklist );
			chunklist->cur_frame += framecount;
			return framecount;
		} else {
			/* if not possible, insert a new chunk before
                           the current one */
			new = adchunk_new_from_buffer( buffer, framecount, 
						       chunklist->rate, 
						       chunklist->bits, 
						       chunklist->channels,
						       AF_FILE_WAVE);
			if (!new) {
				g_warning("creating chunk from frames.");
				return -1;
			}
			chunklist->list = g_list_insert(chunklist->list, new, 
				      g_list_index(chunklist->list, tmpch));
			/* update write pointer */
			adchunklist_rebuild( chunklist );
			chunklist->cur_frame += new -> framecount;
		}
	} else

	/* if it is inside a chunk, split it and make 2, 
	   then create a new chunk and insert it */
	if (seek_res < tmpch->virtual_offset + tmpch->framecount) {
		ADChunk *part1,*part2;
		gint pos;
		/* create the new chunk */
		new = adchunk_new_from_buffer( buffer, framecount, 
					       chunklist->rate, 
					       chunklist->bits, 
					       chunklist->channels,
					       AF_FILE_WAVE);
		if (!new) {
			g_warning("creating chunk from frames.");
			return -1;
		}
		
		/* split the old one */
		if (!adchunk_split_at(tmpch, seek_res, &part1, &part2)) {
			g_warning("splitting chunk.");
			return -1;
		}
		/* store link position */
		pos = g_list_index(chunklist->list, tmpch);
		/* remove the link */
		chunklist->list = g_list_remove( chunklist->list, tmpch);
		/* free the link */
		adchunk_free(tmpch);
		/* insert the new chunks in reverse order */
		chunklist->list = g_list_insert(chunklist->list, part2, pos);
		chunklist->list = g_list_insert(chunklist->list, new, pos);
		chunklist->list = g_list_insert(chunklist->list, part1, pos);
		adchunklist_rebuild( chunklist );
		chunklist->cur_frame += new -> framecount;
	} else return -1;
	return new->framecount;
}

glong
adchunklist_read(ADChunkList *chunklist, gpointer buffer, glong framecount)
{
	glong tmpcount = framecount;
	glong frames_read=0;
	glong seek_res=0;
	glong seek_to;
	guint8 *tmpbuf = buffer;
	g_assert(chunklist);
	g_assert(buffer);


	if (framecount == 0)
		return 0;

	seek_to = chunklist->cur_frame;
	seek_res = adchunklist_seek(chunklist, seek_to);
	
	while ((tmpcount > 0) && (seek_to==seek_res))
	{

		frames_read = adchunk_read(chunklist->cur_chunk, tmpbuf, chunklist->cur_frame, tmpcount);
		if (frames_read == 0) break;
		seek_to = (chunklist->cur_frame + frames_read);
		seek_res = adchunklist_seek(chunklist, seek_to);
		tmpcount -= frames_read;
		tmpbuf += frames_read * (chunklist->bits / 8) * chunklist->channels;
	}
#ifdef DEBUG
	g_print("chunklist_read: framecount - tmpcount = %d, framecount = %d, tmpcount = %d\n", framecount - tmpcount, framecount, tmpcount);	
#endif
	return framecount - tmpcount;
}


/**
 * adchunklist_seek
 * @chunklist: a valid chunklist
 * @offset: where to move the read/write pointer
 *
 * Description: This function moves the read/write pointer for the
 * chunklist to the specified offset, and updates the internal data
 * structure for cur_chunk and cur_frame.
 *
 * Returns: The actual position to where the pointer was moved. Can be
 * different from offset if an out-of bounds request is made.
 *
 */
glong
adchunklist_seek(ADChunkList *chunklist, glong offset)
{
	GList *tmp;
	ADChunk *tmpch=NULL;
	g_assert(chunklist);
	g_assert(offset>=0);
	
#ifdef DEBUG
		g_print("adchunklist_seek\n");
#endif
	
	if (offset <= 0)
	{
#ifdef DEBUG
		g_print("\toffset <= 0\n");
#endif
		tmp = g_list_first(chunklist->list);
		if (tmp) chunklist->cur_chunk = tmp->data;
		else chunklist->cur_chunk = NULL;
		chunklist->cur_frame = 0;
		return 0;
	}

	if (offset >= chunklist->framecount)
	{
#ifdef DEBUG
		g_print("\toffset > chunklist->framecount\n");
#endif
		tmp = g_list_last(chunklist->list);
		if (tmp) chunklist->cur_chunk = tmp->data;
		else chunklist->cur_chunk = NULL;
		chunklist->cur_frame = chunklist->framecount;
		return chunklist->cur_frame;
	}

	tmp = g_list_first(chunklist->list);

	while (tmp)
	{
		g_assert(tmp->data);
		tmpch = tmp->data;
		g_assert(offset >= tmpch->virtual_offset);

		if (offset < (tmpch->virtual_offset + tmpch->framecount))
			break;
		
		tmp = g_list_next(tmp);
	}

	if (tmpch)
	{
		if (offset < (tmpch->virtual_offset + tmpch->framecount))
		{
			chunklist->cur_chunk = tmp->data;
			chunklist->cur_frame = offset;
			return offset;
		}
	}

	g_assert_not_reached();	
	return -1; 
}

/** adchunklist_save: 
 * @chunklist: a valid chunklist 
 * @filename: the filename for the saved file. May be NULL, in this case 
 * the file of the first chunk is used.
 *
 * Description: saves the chunklist in a file indicated by the filename
 *              
 * Returns: On succes, the new chunklist is returned, otherwise NULL.
 *
 **/
ADChunkList*
adchunklist_save(ADChunkList *chunklist, const gchar *filename)
{
	GList *firstl;
	ADChunk *first, *second;
	ADChunk *new, *temp;
	gchar *old_filename;

	g_assert(chunklist);
#ifdef DEBUG
	g_print("adchunklist_save\n");
#endif

	/* merge the first chunk with a dummy chunk to unlink all
           possible references to the first chunk. */
	second = adchunk_new();	
	second->framecount = 0;
	g_assert(chunklist->list);
	g_assert(chunklist->list->data);
	first = chunklist->list->data;
	g_assert(first);
	g_assert(second);
	if ((new = adchunk_merge(first, second))==NULL)
	{
		g_error("error merging two chunks");
		return NULL;
	}
	chunklist->list = g_list_remove(chunklist->list, first);
	//chunklist->list = g_list_prepend(chunklist->list, new);
	adchunk_free(first);
	adchunk_free(second);
	adchunklist_rebuild(chunklist);

	/* now merge the new first chunk with all the rest */
	if (g_list_length(chunklist->list)>0)
	{
		ADChunk *new2;
		/*		firstl = g_list_first(chunklist->list);
		secondl = g_list_next(firstl);
		g_assert(firstl);
		g_assert(secondl);
		first = firstl->data;
		second = secondl->data;
		g_assert(first);
		g_assert(second);
		if ((new = adchunk_merge(first, second))==NULL)
		{
			g_error("error merging two chunks");
			return NULL;
		}
		chunklist->list = g_list_remove(chunklist->list, first);
		chunklist->list = g_list_remove(chunklist->list, second);
		chunklist->list = g_list_prepend(chunklist->list, new);
		adchunk_free(first);
		adchunk_free(second);*/

		if ((new2 = adchunk_merge_list(new, chunklist->list))==NULL)
		{
			g_error("error merging two chunks");
			return NULL;
		}
		
		adchunk_free(new);
		new = new2;

		/* remove and free the chunks belonging to the list. */
		while (chunklist->list) {
			temp = chunklist->list->data;
			chunklist->list = g_list_remove(chunklist->list, temp);
			adchunk_free(temp);
		}
	}

	/* add the new chunk to the list */
	chunklist->list = g_list_prepend(chunklist->list, new);
	adchunklist_rebuild(chunklist);

	firstl = g_list_first(chunklist->list);
	g_assert(firstl);

	/* FIXME: this sucks, get it better */
	/* rename the file */
	old_filename = g_strdup(((ADChunk *)firstl->data)->adfh->filename);
	g_assert(old_filename);

	adfilehandle_set_to_be_deleted(((ADChunk *)firstl->data)->adfh, FALSE);

	/* close the file */
	adchunklist_free(chunklist);

	if (filename) {
		if ( rename(old_filename, filename) < 0 ) {
			g_warning("Error renaming file: %s %s", old_filename, filename);
			g_free(old_filename);
			return NULL;
		}
	}

	/* reopen the file */
	chunklist = adchunklist_new_from_file(filename);
	
/*	adchunklist_seek(chunklist, chunklist->cur_frame); */
	g_free(old_filename);
	return chunklist;
}

ADChunkList*
adchunklist_save_as(ADChunkList *chunklist, const gchar *filename, const gint format)
{
	ADChunk *new;
	ADChunkList *new_chunklist;

	g_assert(chunklist);
#ifdef DEBUG
	g_print("adchunklist_save_as\n");
#endif
	new = adchunk_new_from_chunklist( chunklist->list, filename, format );

	new_chunklist = adchunklist_new_with_settings ( chunklist->rate, chunklist->bits, chunklist->channels );
	adchunklist_free(chunklist);

	new_chunklist->list = g_list_prepend(new_chunklist->list, new);
	adchunklist_rebuild(new_chunklist);

	return new_chunklist;
}

ADChunkList*
adchunklist_new_from_file(const gchar *filename)
{
	ADChunk *chunk;
	ADChunkList *chunklist;
	gint version; /* not used */
	gint sampleFormat, sampleWidth;

	g_assert(filename);

	if ((chunk = adchunk_new_from_file(filename))==NULL)
	{
		/* g_warning("unable to create a chunk for file %s", filename); */
		return NULL;
	}
	chunklist = adchunklist_new();
	chunklist->list = g_list_prepend(chunklist->list, chunk);

#ifdef DEBUG
	g_print("chunk pos: %d\n", g_list_position(chunklist->list, g_list_find(chunklist->list, chunk)));
#endif

	chunklist->cur_chunk = chunk;	
	chunklist->cur_frame = 0;

	
	chunklist->format = afGetFileFormat(adfilehandle_get_fh(chunk->adfh), &version);

	afGetSampleFormat(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, &sampleFormat, &sampleWidth);

	chunklist->bits = sampleWidth;
	chunklist->rate = afGetRate (adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK);
	chunklist->channels = afGetChannels(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK);

	chunklist->framecount = afGetFrameCount (adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK);

	return chunklist;
}

ADChunkList*
adchunklist_new (void)
{
	ADChunkList *tmp;

	tmp = g_new(ADChunkList, 1);

	g_assert(tmp);

	tmp->list = NULL;

	tmp->cur_chunk = NULL;
	tmp->cur_frame = 0;

	tmp->format = AD_DEFAULT_FORMAT;

	tmp->bits = AD_DEFAULT_BITS;
	tmp->channels = AD_DEFAULT_CHANNELS;
	tmp->rate = AD_DEFAULT_RATE;

	tmp->framecount = 0;

	return tmp;
}

ADChunkList*
adchunklist_new_with_settings (guint rate, guint bits, guint channels)
{
	ADChunkList *tmp;
	
	tmp = adchunklist_new();
	tmp->bits = bits;
	tmp->channels = channels;
	tmp->rate = rate;

	return tmp;
}

gboolean 
adchunklist_free (ADChunkList *chunklist)
{
	GList *tmp;
	ADChunk *chunk;
	g_assert(chunklist);
	
	tmp = g_list_first(chunklist->list);
	while(tmp)
	{
		chunk = tmp->data;
		tmp = g_list_remove(tmp, tmp->data);
		if (!adchunk_free(chunk))
		{
			g_warning("unable to free a chunk");
			return FALSE;
		}
	}

	return TRUE;
}

gint
adchunklist_get_rate(ADChunkList *chunklist)
{
	g_assert(chunklist);
	return chunklist->rate;
}

gint
adchunklist_get_bits(ADChunkList *chunklist)
{
	g_assert(chunklist);
	return chunklist->bits;
}

gint
adchunklist_get_channels(ADChunkList *chunklist)
{
	g_assert(chunklist);
	return chunklist->channels;
}

glong
adchunklist_get_framecount(ADChunkList *chunklist)
{
	g_assert(chunklist);
	return chunklist->framecount;
}
