/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
  Various comments (by g.iachello@iol.it)

 All unref / ref pairs (which close the underlying read-only audio
 file and reopen it in write only mode) are now implemented with the
 adfilehandle_flush function, because unreffing a filehandle of a temp
 file has, as side effect, that the file is deleted in case the
 to_be_deleted flag is set.

 TODO: Also note that there are some dangling refs, which are never
 unreffed. For example those related to clipboard operations. The
 causes temp files to be left over; this should be looked into. 

 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <errno.h>
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_utils.h"
#include "audiodata_defaults.h"

/* this is used in "merge" */
GList *globalchunklist = NULL;

void
adchunk_globalchunklist_print(void)
{
	GList *list;

	list = globalchunklist;
	
	g_print("global chunk list %d elements\n", g_list_length(list));

	while (list) {
		adchunk_print((ADChunk *)list->data);
		list = g_list_next(list);
	}
}


void
adchunk_print(ADChunk *chunk)
{
	g_assert(chunk);
	g_print("file: %s offset: %ld, framecount: %ld, virtual_offset: %ld\n", 
		(chunk->adfh ? adfilehandle_get_filename(chunk->adfh) : "<null>") , 
		chunk->offset, chunk->framecount, chunk->virtual_offset);
}


glong
adchunk_read (ADChunk *chunk, gpointer buffer, glong begin_frame, glong framecount)
{
	g_assert(chunk);
	g_assert(buffer);

	if ((begin_frame > (chunk->virtual_offset + chunk->framecount))
		|| (begin_frame < chunk->virtual_offset))
	{
		g_warning("Call seek function before this one.");
		return 0;
	}

	/* flush the readin file (be sure it's readable) */
	if (chunk->adfh)
		adfilehandle_flush(chunk->adfh);

	/* TODO: check errors: problem, I have no docs on this funcs,
	 * check audiofile code */
	afSeekFrame(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, chunk->offset + (begin_frame - chunk->virtual_offset));

	if (framecount <= (chunk->framecount - (begin_frame - chunk->virtual_offset)))
	{
		return afReadFrames(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, buffer, framecount);
	} else {
		return afReadFrames(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, buffer, (chunk->framecount - (begin_frame - chunk->virtual_offset)));
	}
}


ADChunk *
adchunk_clone(ADChunk *original)
{
	ADChunk *tmp;
	g_assert(original);

	tmp = adchunk_new();

	tmp->adfh = original->adfh;
	if (adfilehandle_ref_by_adfh(tmp->adfh)==NULL)
	{
		g_warning("unable to reference a filehandle");
		adchunk_free(tmp);
		return NULL;
	}

	tmp->offset = original->offset;
	tmp->virtual_offset = original->virtual_offset;
	tmp->framecount = original->framecount;
	return tmp;
}

/**
 * adchunk_merge:
 * @first:   first chunk to merge (the second will be appended to the first)
 * @second:  second chunk to merge
 *
 * Description: This function merges the second chunk into the first
 * one If other chunks refer to the first chunk base file, the base
 * file is copied and the references moved to the copy.  
 *
 * Returns: the merged chunk
 * 
 */
ADChunk *
adchunk_merge(ADChunk *first, ADChunk *second)
{
	GList *list;
	ADChunk *new;
	
	list = NULL;

	list = g_list_append(list, second);

	new = adchunk_merge_list(first, list);

	g_list_free(list);

	return new;
}

/**
 * adchunk_merge_list:
 * @first:   first chunk to merge (the second will be appended to the first)
 * @to_merge_list:  list of chunks to merge
 *
 * Description: This function merges the second chunk into the first
 * one If other chunks refer to the first chunk base file, the base
 * file is copied and the references moved to the copy.  
 * What a complex function!
 * TODO: split it 
 *
 * Returns: the merged chunk
 * 
 */
ADChunk *
adchunk_merge_list(ADChunk *first, GList *to_merge_list)
{
	GList *list = NULL;
	GList *locallist = NULL;
	ADChunk *tmpchunk;
	ADChunk *new_chunk;
	ADFileHandle *original_adfh;
	gboolean original_to_be_deleted = FALSE;
	AFfilesetup setup;
	glong tmpbegin=G_MAXLONG, tmpend=0, frames_to_copy;
	glong read_frames;
	glong written_frames;
	gint new_rate, new_bits, new_channels, new_format, version, sampleFormat;
	gchar *new_filename;
	gchar *old_filename;
	gpointer buffer;
	gint number_of_references;

	g_assert(first);
	g_assert(to_merge_list);

	/* flush the readin file (be sure it's readable) */
	if (first->adfh)
		adfilehandle_flush(first->adfh);

	/* Initialize the file setup for the copy of the first chunk */
	old_filename = g_strdup(adfilehandle_get_filename(first->adfh));
	afGetSampleFormat(adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK, 
			  &sampleFormat, &new_bits);
	new_rate = afGetRate (adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK);
	new_channels = afGetChannels(adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK);
	new_format = afGetFileFormat(adfilehandle_get_fh(first->adfh), &version);

	buffer = g_malloc(AD_DEFAULT_BUFFER_SIZE *
			  afGetFrameSize(adfilehandle_get_fh(first->adfh), 
					 AF_DEFAULT_TRACK, FALSE));
	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
		return NULL;
	}
	afInitFileFormat(setup, new_format);
	afInitChannels(setup, AF_DEFAULT_TRACK, new_channels);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, new_bits);
	afInitRate(setup, AF_DEFAULT_TRACK, new_rate);

	/* we need to search if some other chunks are using the same
	 * filehandle as the "first" chunk. If so, we need to create
	 * a new file (with a new filehandle) to replace the modified
	 * file (filehandle)... we need to modify the "first" chunk!
	 *
	 **/
	list = g_list_first(globalchunklist);
	number_of_references = adfilehandle_get_ref_count( first->adfh );
	while (list && number_of_references > 0)
	{
		tmpchunk = list->data;
		g_assert(tmpchunk);
		if (tmpchunk->adfh==first->adfh)
		{
#ifdef DEBUG
			g_print("I've found a chunk!\n");
#endif
			tmpbegin = MIN(tmpbegin, tmpchunk->offset);
			tmpend = MAX(tmpend, (tmpchunk->offset + tmpchunk->framecount));
			locallist = g_list_prepend(locallist, tmpchunk);
			number_of_references--;
		}
		list = g_list_next(list);
	}

	/* let's create the new file and redirect all the chunks that
           referenced the file to the new one */
	if (locallist)
	{
#ifdef DEBUG
		g_print("locallist!=NULL: %d\n", g_list_length(locallist));
#endif
		g_assert(tmpbegin >= 0);
		g_assert(tmpend <= afGetFrameCount (adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK));

		new_filename = create_tmp_filename(new_format);			
		g_assert(locallist->data);
		tmpchunk = locallist->data;

		/* save the original file handle and the to_be_deleted flag */

		original_adfh = tmpchunk->adfh;	
		original_to_be_deleted = adfilehandle_get_to_be_deleted(original_adfh);
#ifdef DEBUG
		g_print("assignin proper adfh\n");
		g_print("referenciate the new file\n");
#endif
		/* create the new file (with a temp filename) */
		
		if ((tmpchunk->adfh = adfilehandle_ref(new_filename, setup))==NULL)
		{
			g_error("unable to referenciate file: %s", new_filename);
		}

		/* delete the temp file upon unref of last chunk */
		adfilehandle_set_to_be_deleted(tmpchunk->adfh, TRUE);

		frames_to_copy = tmpend - tmpbegin; 
		afSeekFrame(adfilehandle_get_fh(tmpchunk->adfh), AF_DEFAULT_TRACK, 0);
		afSeekFrame(adfilehandle_get_fh(original_adfh), AF_DEFAULT_TRACK, tmpbegin);
		while (frames_to_copy > 0)
		{	
			read_frames = afReadFrames(adfilehandle_get_fh(original_adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
			written_frames = afWriteFrames(adfilehandle_get_fh(tmpchunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
			if (written_frames!=read_frames)
			{
				g_error("3error copying a chunk: %ld %ld", read_frames, written_frames);
				return NULL;
			}
			frames_to_copy-=read_frames;
		}

		/* flush the new file and reopen it readonly */
		adfilehandle_flush(tmpchunk->adfh);
		/* close the original file. Note that it could be that
                   the original first chunk file is automatically
                   deleted at this point, or at the last refcount in
                   the while loop below. */
		adfilehandle_unref(original_adfh);
		tmpchunk->offset = tmpchunk->offset - tmpbegin;
		locallist = g_list_remove_link(locallist, locallist);
		/* let's update the chunks we have found that need to
		 * update the filehandle
		 */
		while (locallist)
		{
			g_assert(locallist->data);
			tmpchunk = locallist->data;
			adfilehandle_unref(tmpchunk->adfh);
#ifdef DEBUG
			g_print("assigning proper adfh in cycle\n");
			g_print("referenciate the new file\n");
#endif
			tmpchunk->adfh = adfilehandle_ref(new_filename, NULL);	
			tmpchunk->offset = tmpchunk->offset - tmpbegin;
			locallist = g_list_remove_link(locallist, locallist);
		}

		g_free(new_filename);
	}

	/* let's really merge the chunks */

	/* let's truncate the file.
	 * audiofile doesn't provide any truncate function, so
	 * we must go in dirty mode */
	if (unlink(old_filename)==-1)
	{
		/* the file may have been already deleted */
	        if (errno != ENOENT) { 
	                g_error("error truncating the file!");
		        return NULL;
		}
	}
	new_chunk = adchunk_new();
#ifdef DEBUG
	g_print("referenciate the old file\n");
#endif
	/* now create the new filehandle for the copy */

	if ((new_chunk->adfh = adfilehandle_ref(old_filename, setup))==NULL)
	{
		g_error("unable to reference file %s", old_filename);
	}

	adfilehandle_set_to_be_deleted(new_chunk->adfh, original_to_be_deleted);

	new_chunk->offset = 0;
	new_chunk->framecount = first->framecount;
	new_chunk->virtual_offset = first->virtual_offset;
#ifdef DEBUG
	g_print("seek ret value: %d\n", afSeekFrame(adfilehandle_get_fh(new_chunk->adfh), AF_DEFAULT_TRACK, 0));
#endif
	afSeekFrame(adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK, first->offset);

	/* copy the first chunk (which by this time is relocated in a new file) 
	   into the new chunk.
	*/
	frames_to_copy = first->framecount;
	while (frames_to_copy > 0)
	{	
#ifdef DEBUG
		g_print("cycle! frames_to_copy: %d\n", frames_to_copy);
#endif
		read_frames = afReadFrames(adfilehandle_get_fh(first->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
		written_frames = afWriteFrames(adfilehandle_get_fh(new_chunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
		if (written_frames!=read_frames)
		{
			g_error("1error copying a chunk: %ld %ld", read_frames, written_frames);
			return NULL;
		}
		frames_to_copy-=read_frames;
	}

	/* now append the entire chunk list */

	while (to_merge_list) {
		ADChunk *merging_chunk;

		merging_chunk = (ADChunk*)to_merge_list->data;
		g_assert(merging_chunk);

		new_chunk->framecount += merging_chunk->framecount;
		
		frames_to_copy = merging_chunk->framecount;

		/* flush the readin file (be sure it's readable) */
		if (merging_chunk->adfh)
			adfilehandle_flush(merging_chunk->adfh);

		if (frames_to_copy> 0)
		{
			afSeekFrame(adfilehandle_get_fh(merging_chunk->adfh), AF_DEFAULT_TRACK, merging_chunk->offset);
			while (frames_to_copy > 0)
			{	
				read_frames = afReadFrames(adfilehandle_get_fh(merging_chunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
				written_frames = afWriteFrames(adfilehandle_get_fh(new_chunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
				if (written_frames!=read_frames)
				{
					g_error("2error copying a chunk: %ld %ld", read_frames, written_frames);
					return NULL;
				}
				frames_to_copy-=read_frames;
			}
		}
		to_merge_list = g_list_next(to_merge_list);
	}
	/* flush the new chunk and reopen it in readonly mode */
	adfilehandle_flush(new_chunk->adfh);
	afFreeFileSetup(setup);
	g_free(buffer);
	return new_chunk;
}

/**
 * adchunk_new_from_chunklist:
 * @chunklist: the list of chunks to convert into a new file based-chunk
 * @filename: the filename of the chunk. Can be NULL; in this case the file 
 *            is created as a temp file. 
 * @format: the format of the new file.
 *
 * Description: This functions build up a new chunk, based on a file,
 * from the given list of chunks.
 *
 * Returns: On success the function returns a valid audiodata, NULL otherwise.
 **/
ADChunk *       
adchunk_new_from_chunklist(GList *chunklist, const gchar *filename, gint format)
{
        GList *list = chunklist;
	gchar *real_filename;
	ADChunk *chunk, *tmp_chunk;
	AFfilesetup setup;
	gint new_rate, new_bits, new_channels, sampleFormat;
	gpointer buffer;
	gboolean set_delete_flag;

	g_assert( list );

	tmp_chunk = (ADChunk *)list -> data;

	afGetSampleFormat(adfilehandle_get_fh(tmp_chunk->adfh), AF_DEFAULT_TRACK, &sampleFormat, &new_bits);
	new_rate = afGetRate (adfilehandle_get_fh(tmp_chunk->adfh), AF_DEFAULT_TRACK);
	new_channels = afGetChannels(adfilehandle_get_fh(tmp_chunk->adfh), AF_DEFAULT_TRACK);
	buffer = g_malloc(AD_DEFAULT_BUFFER_SIZE *(new_bits/8) *new_channels);

	if (filename) {
	  real_filename = g_strdup(filename);
	  set_delete_flag = FALSE;
	}
	else {
	  real_filename = create_tmp_filename(format);
	  set_delete_flag = TRUE;
	}
	
	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
		return NULL;
	}
	afInitFileFormat(setup, format);
	afInitChannels(setup, AF_DEFAULT_TRACK, new_channels);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, new_bits);
	afInitRate(setup, AF_DEFAULT_TRACK, new_rate);

	chunk = adchunk_new();

	if ((chunk->adfh = adfilehandle_ref(real_filename, setup))==NULL)
	{
		g_warning("unable to reference file: %s", real_filename);
		adchunk_free(chunk);
		afFreeFileSetup(setup);
		g_free(real_filename);
		return NULL;
	}

	if (set_delete_flag) {
	        /* delete the temp file upon last unref */
	        adfilehandle_set_to_be_deleted(chunk->adfh, TRUE);
	}

	while (list) {
		glong write_ret;
		glong read_ret;
		glong frames_to_copy;

		tmp_chunk = (ADChunk *)list->data;
		g_assert(tmp_chunk);

		/* flush the readin file (be sure it's readable) */
		if (tmp_chunk->adfh)
			adfilehandle_flush(tmp_chunk->adfh);

		frames_to_copy = tmp_chunk -> framecount;
		afSeekFrame(adfilehandle_get_fh(tmp_chunk->adfh), AF_DEFAULT_TRACK, tmp_chunk->offset);

		while (frames_to_copy > 0) {
			read_ret = afReadFrames(adfilehandle_get_fh(tmp_chunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));
			write_ret = afWriteFrames(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, buffer, MIN(AD_DEFAULT_BUFFER_SIZE, frames_to_copy));

			if ((write_ret == AF_BAD_WRITE) || 
			    (write_ret == AF_BAD_LSEEK) || 
			    (write_ret == AF_BAD_TRACKID)) {
				g_warning("bad write");
				adchunk_free(chunk);
				g_free(real_filename);
				afFreeFileSetup(setup);
				return NULL;
			}
			if (write_ret != read_ret) {
				g_error("error copying a chunk: %ld %ld", read_ret, write_ret);
				adchunk_free(chunk);
				g_free(real_filename);
				afFreeFileSetup(setup);
				return NULL;
			}
		  
			chunk->framecount += write_ret;
			frames_to_copy -= read_ret;
		}
	  
		list = g_list_next( list );
	}
	
	//adfilehandle_unref(chunk->adfh);
	//chunk->adfh = adfilehandle_ref(filename, NULL);
	adfilehandle_flush(chunk->adfh);
	g_free(real_filename);
	afFreeFileSetup(setup);

	return chunk;
}


/**
 * adchunk_split_at:
 * @original: the original chunk, the one to be split
 * @position: the (absolute) position where to split
 * @first: the first resulting chunk
 * @second: the second resulting chunk
 *
 * Description: This functions take a chunk ("original") and split it
 * at position "position" in two new chunks.
 *
 * Returns: the function returns a TRUE value if no error occours,
 * FALSE otherwise.
 **/
gboolean
adchunk_split_at(ADChunk *original, 
			glong position, 
			ADChunk **first, 
			ADChunk **second)
{
	ADChunk *tmp1;
	ADChunk *tmp2;
	g_assert(original);
	g_assert(first);
	g_assert(second);

/*	g_assert(position > original->virtual_offset); */
	g_assert(position < (original->virtual_offset + original->framecount));
	
	if ((tmp1 = adchunk_clone(original))==NULL)
	{
		g_warning("unable to copy a chunk");
		return FALSE;
	}
	if ((tmp2 = adchunk_clone(original))==NULL)
	{
		g_warning("unable to copy a chunk");
		adchunk_free(tmp1);
		return FALSE;
	}
	
	tmp1->framecount = position - tmp1->virtual_offset;
	
	tmp2->offset = tmp2->offset + (position - tmp1->virtual_offset);
	tmp2->virtual_offset = tmp2->virtual_offset + (position - tmp1->virtual_offset);
	tmp2->framecount = original->framecount - (position - original->virtual_offset);

	*first = tmp1;
	*second = tmp2;

	return TRUE;
}

/**
 * adchunk_new_from_buffer:
 * @buffer: a pointer to some data
 * @framecount: the buffer size
 * @rate:
 * @bits:
 * @channels:
 * @format:
 *
 * Description: This functions build up a new chunk from the buffer "buffer".
 * TODO:
 * this function sucks a bit.
 * It could be better when RAW format will be implemented in libaudiofile.
 *
 *
 * Returns: On success the function returns a valid audiodata, NULL otherwise.
 **/
ADChunk *
adchunk_new_from_buffer (gpointer buffer, gulong framecount, guint rate, guint bits, guint channels, gint format)
{
	gchar *filename;
	ADChunk *chunk;
	AFfilesetup setup;
	gint write_ret;

	g_assert(buffer);

	filename = create_tmp_filename(format);

	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
		return NULL;
	}
	//	g_print("%d %d %d %d %d %p\n", framecount, rate, bits, channels, format, setup);
	afInitFileFormat(setup, format);
	afInitChannels(setup, AF_DEFAULT_TRACK, channels);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, bits);
	afInitRate(setup, AF_DEFAULT_TRACK, rate);

	chunk = adchunk_new();

	if ((chunk->adfh = adfilehandle_ref(filename, setup))==NULL)
	{
		g_warning("unable to reference file: %s", filename);
		adchunk_free(chunk);
		afFreeFileSetup(setup);
		g_free(filename);
		return NULL;
	}

	/* delete the temp file upon last unref */
	adfilehandle_set_to_be_deleted(chunk->adfh, TRUE);
	
	write_ret = afWriteFrames(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, buffer, framecount);

	if ((write_ret == AF_BAD_WRITE) || 
			(write_ret == AF_BAD_LSEEK) || 
			(write_ret == AF_BAD_TRACKID))
	{
		g_warning("bad write");
		adchunk_free(chunk);
		g_free(filename);
		afFreeFileSetup(setup);
		return NULL;
	}

	
	//adfilehandle_unref(chunk->adfh);
       	//chunk->adfh = adfilehandle_ref(filename, NULL);
	adfilehandle_delayed_flush(chunk->adfh);
	chunk->framecount = write_ret;
	g_free(filename);
	afFreeFileSetup(setup);

	return chunk;
}

/**
 * adchunk_append_to_chunk:
 * @chunk: a pointer to some chunk
 * @buffer: a pointer to some data
 * @framecount: the buffer size
 *
 * Description: This functions appends to a new chunk data from the
 * buffer "buffer".  
 *
 * Returns: On success the function returns TRUE, FALSE otherwise.
 **/
gboolean
adchunk_append_to_chunk(ADChunk *chunk, gpointer buffer, gulong framecount)
{
	gint write_ret;
	g_assert(chunk);
	g_assert(buffer);

	if (!adfilehandle_get_delayed_flush( chunk->adfh ))
		return FALSE;

	/* ok to append */
	write_ret = afWriteFrames(adfilehandle_get_fh(chunk->adfh), AF_DEFAULT_TRACK, buffer, framecount);

	if ((write_ret == AF_BAD_WRITE) || 
			(write_ret == AF_BAD_LSEEK) || 
			(write_ret == AF_BAD_TRACKID))
	{
		g_warning("bad append");
		return FALSE;
	}

	chunk->framecount += write_ret;
	return TRUE;
}


ADChunk *
adchunk_new_from_file (const gchar *filename)
{
	ADChunk *tmp;
	g_assert(filename);

	if (!file_exists(filename))
	{
		g_warning("the file have to exist");
		return NULL;
	}
	
	tmp = adchunk_new();

	tmp->adfh = adfilehandle_ref(filename, NULL);

	if (tmp->adfh == NULL) {
		/* Error opening the file */
		adchunk_free(tmp);
		return NULL;
	}

	tmp->offset = 0;
	tmp->framecount = afGetFrameCount (adfilehandle_get_fh(tmp->adfh), AF_DEFAULT_TRACK);

	tmp->virtual_offset = 0;
#ifdef DEBUG
	g_print("load file %s %ld\n", filename, tmp->framecount);
#endif
	return tmp;
}

ADChunk *       
adchunk_new (void)
{
	ADChunk *tmp;

	tmp = g_new(ADChunk, 1);
	
	tmp->adfh = NULL;

	tmp->offset = 0;
	tmp->framecount = 0;

	tmp->virtual_offset = 0;

	globalchunklist = g_list_prepend(globalchunklist, tmp);

	return tmp;
}

gboolean
adchunk_free (ADChunk* chunk)
{
	g_assert(chunk);

	globalchunklist = g_list_remove(globalchunklist, chunk);

	if (chunk->adfh)
	{
		if (!adfilehandle_unref(chunk->adfh))
		{
			g_warning("unable to unref filehandle");
			return FALSE;
		}
	}
	g_free(chunk);
	
	return TRUE;
}

