/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __AUDIODATA_FILEHANDLE_H_
#define __AUDIODATA_FILEHANDLE_H_


typedef struct _ADFileHandle ADFileHandle;

struct _ADFileHandle {
	AFfilehandle fh;
	gint 	     ref_count;
	gchar	     *filename;
        gboolean     to_be_deleted;
	gboolean     delayed_flush;
};

ADFileHandle*	adfilehandle_ref 	(const gchar *filename, AFfilesetup setup);
ADFileHandle*	adfilehandle_ref_by_adfh 	(ADFileHandle* adfh);
gboolean	adfilehandle_unref 	(ADFileHandle *adfh);
AFfilehandle	adfilehandle_get_fh	(ADFileHandle *adfh);
const gchar *	adfilehandle_get_filename (ADFileHandle *adfh);
gint            adfilehandle_get_ref_count(ADFileHandle *adfh);
void            adfilehandle_set_to_be_deleted  (ADFileHandle *adfh, 
						 gboolean to_be_deleted);
gboolean        adfilehandle_get_to_be_deleted  (ADFileHandle *adfh);
gboolean        adfilehandle_flush      (ADFileHandle *adfh);
gboolean        adfilehandle_delayed_flush      (ADFileHandle *adfh);
gboolean        adfilehandle_get_delayed_flush (ADFileHandle *adfh);

#endif /* __AUDIODATA_FILEHANDLE_H_ */
