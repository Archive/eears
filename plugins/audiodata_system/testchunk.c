/* Electric Ears Audiodata module test suite
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include "audiodata_filehandle.h"
#include "audiodata_utils.h"
#include "audiodata_chunks.h"

#define FREQ 440.0
#define RATE 8000

#define sign(a) ((a)>0.0 ? 1.0 : -1.0)


int main()
{
	gchar *fileA, *fileB;
	ADChunk *A, *B, *C, *D, *E, *F;
	GList *list;
	gushort sine[RATE*2], saw[RATE*2], output[RATE*4];
	gint i,j,k;
	
	fileA = create_tmp_filename(AF_FILE_AIFF);
	fileB = create_tmp_filename(AF_FILE_AIFF);

	/*	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
	}
	afInitFileFormat(setup, AF_FILE_AIFF);
	afInitChannels(setup, AF_DEFAULT_TRACK, 2);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, 16);
	afInitRate(setup, AF_DEFAULT_TRACK, RATE);
	*/

	/* init buffer */
	for (i=0; i< RATE*2; i+=2 ) {
	  sine[i] = 16000 * sin( ((double)i)*FREQ*M_PI/RATE);
	  sine[i+1] = 16000 * cos( ((double)i)*FREQ*M_PI/RATE);
	  saw[i] = 16000 * sign(sin( ((double)i)*FREQ*M_PI/RATE));
	  saw[i+1] = 16000 * sign(cos( ((double)i)*FREQ*M_PI/RATE));
	}

	A = adchunk_new_from_buffer(sine, RATE, RATE, 16, 2, AF_FILE_WAVE); 

	B = adchunk_new_from_buffer(saw, RATE, RATE, 16, 2, AF_FILE_WAVE); 

	/* TODO: fix audiofile bug and check the rest */

	list = NULL;;
	list = g_list_append(list,A);
	list = g_list_append(list,B);
	  
	C = adchunk_new_from_chunklist(list, NULL, AF_FILE_WAVE); 

	D = adchunk_merge(A,B);

	g_print("A %s %d %d ", A->adfh->filename, A->adfh->ref_count, A->adfh->to_be_deleted); adchunk_print(A);
	g_print("B %s %d %d ", B->adfh->filename, B->adfh->ref_count, B->adfh->to_be_deleted); adchunk_print(B);
	g_print("C %s %d %d ", C->adfh->filename, C->adfh->ref_count, C->adfh->to_be_deleted); adchunk_print(C);
	g_print("D %s %d %d ", D->adfh->filename, D->adfh->ref_count, D->adfh->to_be_deleted); adchunk_print(D);

	adchunk_split_at(D, 8000, &E, &F);
	g_print("E %s %d %d ", E->adfh->filename, E->adfh->ref_count, E->adfh->to_be_deleted); adchunk_print(E);
	g_print("F %s %d %d ", F->adfh->filename, F->adfh->ref_count, F->adfh->to_be_deleted); adchunk_print(F);

	adchunk_read(D, output, 0, 16000);
	for (i=0; i< RATE*2; i+=2 ) {
	  if (sine[i]!= output[i] || sine[i+1] != output[i+1] ||
	  saw[i] != output [i + 16000] ||  saw[i+1] != output [i+16001])
	    g_warning("not identical %d", i );
	}
	
	j = adchunk_read(D, output, 16000, 16000);

	g_print("read 0 = %d\n", j);
	
	getchar();

	adchunk_free(A);
	adchunk_free(B);
	adchunk_free(C);
	adchunk_free(D);
	adchunk_free(E);
	adchunk_free(F);

	return 0;
}
