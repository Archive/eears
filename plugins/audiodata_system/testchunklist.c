/* Electric Ears Audiodata module test suite
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include "audiodata_filehandle.h"
#include "audiodata_utils.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"

#define FREQ 440.0
#define RATE 8000

#define sign(a) ((a)>0.0 ? 1.0 : -1.0)


int main()
{
	gchar *fileA, *fileB;
	ADChunkList *A, *B, *C, *D, *E, *F;
	GList *list;
	gushort sine[RATE*2], saw[RATE*2], output[RATE*4], zero[RATE*2];
	gint i,j,k;
	
	fileA = create_tmp_filename(AF_FILE_WAVE);
	fileB = create_tmp_filename(AF_FILE_AIFF);

	/*	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_warning("unable to get a new file setup");
	}
	afInitFileFormat(setup, AF_FILE_AIFF);
	afInitChannels(setup, AF_DEFAULT_TRACK, 2);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, 16);
	afInitRate(setup, AF_DEFAULT_TRACK, RATE);
	*/

	/* init buffer */
	for (i=0; i< RATE*2; i+=2 ) {
	  sine[i] = 16000 * sin( ((double)i)*FREQ*M_PI/RATE);
	  sine[i+1] = 16000 * cos( ((double)i)*FREQ*M_PI/RATE);
	  saw[i] = 16000 * sign(sin( ((double)i)*FREQ*M_PI/RATE));
	  saw[i+1] = 16000 * sign(cos( ((double)i)*FREQ*M_PI/RATE));
	  zero[i] = 0;
	  zero[i+1] = 0;
	}

	A = adchunklist_new_with_settings(RATE, 16, 2); 
	g_print("A "); adchunklist_print(A);

	B = adchunklist_new_with_settings(RATE, 16, 2); 
	g_print("B "); adchunklist_print(B);

	adchunklist_seek(A,0);
	adchunklist_write(A, sine, RATE);
	adchunklist_write(A, saw, RATE);
	adchunklist_seek(A,4000);
	adchunklist_write(A, zero, RATE);
	adchunklist_seek(A,0);
	adchunklist_write(A, saw, RATE);

	adchunk_globalchunklist_print();

	adchunklist_save(A, fileA);
	g_print("saw sine zero sine saw chunk saved in %s\n",fileA);

	adchunk_globalchunklist_print();

	
	A = adchunklist_save_as(A, fileB, AF_FILE_AIFF);
	g_print("save as in %s\n",fileB);

	C = adchunklist_new_from_file(fileA);

	D = adchunklist_clone( C );
	
/*	for (i=0; i< RATE*2; i+=2 ) {
	  if (sine[i]!= output[i] || sine[i+1] != output[i+1] ||
	  saw[i] != output [i + 16000] ||  saw[i+1] != output [i+16001])
	    g_warning("not identical %d", i );
	}
	
	j = adchunk_read(D, output, 16000, 16000);

	g_print("read 0 = %d\n", j);
*/
	adchunk_globalchunklist_print();

	g_print("A "); adchunklist_print(A);
	g_print("B "); adchunklist_print(B);
	g_print("C "); adchunklist_print(C);
	g_print("D "); adchunklist_print(D);

	adchunklist_insert_chunklist_at(B, D, 0);
	adchunklist_insert_chunklist_at(B, C, 157);

	g_print("B "); adchunklist_print(B);

	adchunk_globalchunklist_print();
	
	adchunklist_cut(B, 0, 2000);
	g_print("B "); adchunklist_print(B);
	adchunklist_cut(B, 30000, 2000);
	g_print("B "); adchunklist_print(B);
/*	adchunklist_cut(B, 60000, 2000);
	g_print("B "); adchunklist_print(B);
	adchunklist_cut(B, 0, 80000);
	g_print("B "); adchunklist_print(B);
*/
	getchar();

	adchunklist_free(A);
	adchunklist_free(B);
	adchunklist_free(C);
	adchunklist_free(D);

	adchunk_globalchunklist_print();

	return 0;
}
