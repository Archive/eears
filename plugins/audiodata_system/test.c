#include <audiofile.h>
#include <gnome.h>

gint main()
{
	AFfilehandle fh;
	AFfilesetup setup;
	gchar *filename = "file.wav";

	if ((setup = afNewFileSetup()) == AF_NULL_FILESETUP)
	{
		g_error("unable to get a new file setup");
		return -1;
	}

	afInitFileFormat(setup, AF_FILE_WAVE);
	afInitChannels(setup, AF_DEFAULT_TRACK, 2);
	afInitSampleFormat(setup, AF_DEFAULT_TRACK, AF_SAMPFMT_TWOSCOMP, 16);
	afInitRate(setup, AF_DEFAULT_TRACK, 44100);

	if ((fh = afOpenFile(filename, "w+", setup))==AF_NULL_FILEHANDLE)
		                     /* ^^ */
	{
		g_error("unable to open file %s", filename);
		return -1;
	}


	afCloseFile(fh);
	return 0;
}

