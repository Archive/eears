#ifndef __AUDIODATA_UTILS_H_
#define __AUDIODATA_UTILS_H_

gchar *		create_tmp_filename	(gint format);
gboolean	file_exists 		(const gchar *filename);
gboolean	file_is_readable 	(const gchar *filename);
gboolean	file_is_writeable 	(const gchar *filename);
const gchar*	get_suffix_from_fmt	(gint fmt);
	
#endif /* __AUDIODATA_UTILS_H_ */
