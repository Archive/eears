/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _AUDIODATA_SYSTEM_H_
#define _AUDIODATA_SYSTEM_H_

#include "metadefs.h"

typedef struct _AudioData AudioData;

struct _AudioData {
	GList *redo_chunklists;
	ADChunkList *cur_chunklist;
	GList *undo_chunklists;

	volatile gchar *filename;

	volatile GTimer *lastmodify;
};


MetaAudioData   ad_new			(guint rate, guint bits, guint channels);
MetaAudioData 	ad_open			(const gchar *filename);
gboolean 	ad_close		(MetaAudioData mad);
gboolean 	ad_free			(MetaAudioData mad);
MetaAudioData   ad_clone                (MetaAudioData mad);


gboolean	ad_save			(MetaAudioData mad);
gboolean	ad_save_as		(MetaAudioData mad, const gchar *filename, const gint format);

glong		ad_read			(MetaAudioData mad, 
						gpointer buffer,
						glong framecount);
glong		ad_write		(MetaAudioData mad, 
                                                gpointer buffer,
						glong framecount);

glong		ad_seek			(MetaAudioData mad,
						glong offset);


gdouble 	ad_get_lastmodify 	(MetaAudioData mad);
void 		ad_reset_lastmodify 	(MetaAudioData mad);
const gchar *	ad_get_filename 	(MetaAudioData mad);
	
glong    	ad_get_framecount      	(MetaAudioData mad);
gint    	ad_get_channels        	(MetaAudioData mad);
gint    	ad_get_bits            	(MetaAudioData mad);
gint    	ad_get_rate		(MetaAudioData mad);
gint		ad_get_framesize 	(MetaAudioData mad);
void            ad_set_tmp_dir          (const gchar *tmpdir);

#endif /* _AUDIODATA_SYSTEM_H_ */
