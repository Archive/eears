/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "metadefs.h"
#include "audiodata_filehandle.h"
#include "audiodata_chunks.h"
#include "audiodata_list.h"
#include "audiodata_system.h"
#include "audiodata_clipboard.h"

#include <pthread.h>
extern pthread_mutex_t lock;

ADChunkList *clipboard = NULL;

void put_in_clipboard(AudioData *ad, glong beginframe, glong endframe);

void put_in_clipboard(AudioData *ad, glong beginframe, glong endframe)
{
	g_assert(ad);
	g_assert(beginframe <= endframe);
	g_assert(beginframe >= 0);
	g_assert(endframe < adchunklist_get_framecount(ad->cur_chunklist));

	if (clipboard)
		adchunklist_free(clipboard);

	clipboard = adchunklist_clone(ad->cur_chunklist);
	if (endframe != (adchunklist_get_framecount(ad->cur_chunklist) - 1))
		adchunklist_cut(clipboard, endframe, 
				(adchunklist_get_framecount(ad->cur_chunklist) - endframe));
	if (beginframe > 0)
		adchunklist_cut(clipboard, 0, (beginframe - 1));
}

glong
cb_cut (MetaAudioData mad, glong beginframe, glong endframe)
{
	AudioData *ad = (MetaAudioData) mad;
	pthread_mutex_lock(&lock);
	g_assert(ad);
	g_assert(beginframe <= endframe);
	g_assert(beginframe <= adchunklist_get_framecount(ad->cur_chunklist));
	g_assert(beginframe <= endframe);

	put_in_clipboard(ad, beginframe, endframe);
	adchunklist_cut(ad->cur_chunklist, beginframe, endframe-beginframe);

	pthread_mutex_unlock(&lock);
	return clipboard->framecount;
}


glong
cb_copy (MetaAudioData mad, glong beginframe, glong endframe)
{
	AudioData *ad = (MetaAudioData) mad;
	pthread_mutex_lock(&lock);
	g_assert(ad);
	g_assert(beginframe <= endframe);
	g_assert(beginframe <= adchunklist_get_framecount(ad->cur_chunklist));
	g_assert(beginframe <= endframe);

	put_in_clipboard(ad, beginframe, endframe);

	pthread_mutex_unlock(&lock);
	return clipboard->framecount;
}

/* paste: pastes clipboard in mad
 * @mad:
 * @beginframe:
 * @endframe:
 *
 * Description:
 *
 * Returns: the number of pasted frames 
 */
glong
cb_paste (MetaAudioData mad, glong beginframe, glong endframe)
{
	AudioData *ad = (MetaAudioData) mad;
	pthread_mutex_lock(&lock);
	g_assert(ad);
	g_assert(beginframe >= 0);
	g_assert(beginframe <= adchunklist_get_framecount(ad->cur_chunklist));
	
	if (beginframe!=endframe)
	{
		g_assert(beginframe < endframe);
		adchunklist_cut(ad->cur_chunklist, beginframe, endframe-beginframe);
	}
	adchunklist_insert_chunklist_at(ad->cur_chunklist, clipboard, beginframe);

	pthread_mutex_unlock(&lock);
	return clipboard->framecount;
}


MetaAudioData
cb_open (void)
{
	AudioData *ad ;
	pthread_mutex_lock(&lock);

	if (!clipboard) {
		pthread_mutex_unlock(&lock);
		return NULL;
	}
	
	ad = g_new(AudioData, 1);
	ad->redo_chunklists = NULL;
	ad->undo_chunklists = NULL;

	ad->lastmodify = g_timer_new();
	ad->filename = strdup( _("Untitled") );
	ad->cur_chunklist = adchunklist_clone( clipboard );

	pthread_mutex_unlock(&lock);
	return ad;
}


