/* Electric Ears Audiodata module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __AUDIODATA_CHUNKS_H_
#define __AUDIODATA_CHUNKS_H_

typedef struct _ADChunk ADChunk;

struct _ADChunk {
	ADFileHandle *adfh;

	gulong  offset;
	gulong  framecount;

	gulong  virtual_offset;
};

void		adchunk_print		(ADChunk *chunk);
void            adchunk_globalchunklist_print(void);
ADChunk *	adchunk_new		(void);
gboolean	adchunk_free		(ADChunk* chunk);
glong		adchunk_read 		(ADChunk *chunk, 
						gpointer buffer, 
						glong begin_frame, 
						glong framecount);
gboolean 	adchunk_split_at	(ADChunk *original, 
						glong position, 
						ADChunk **first, 
						ADChunk **second);
ADChunk 	*adchunk_new_from_file 	(const gchar *filename);
ADChunk 	*adchunk_new_from_buffer(gpointer buffer, 
						gulong framecount, 
						guint rate, 
						guint bits, 
						guint channels, 
						gint format);
gboolean        adchunk_append_to_chunk (ADChunk *chunk, gpointer buffer, 
					 gulong framecount);
ADChunk *       adchunk_new_from_chunklist      (GList *chunklist, const gchar *filename, 
						 gint format);
ADChunk *	adchunk_merge		(ADChunk *first, ADChunk *second);
ADChunk *       adchunk_merge_list      (ADChunk *first, GList *to_merge_list);
ADChunk *	adchunk_clone		(ADChunk *original);
	

#endif /* __AUDIODATA_CHUNKS_H_ */
