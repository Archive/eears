/* Electric Ears Audiodata module test suite
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "audiodata_utils.h"

int main ()
{
	gchar *tmp;

	g_print("Checking create_tmp_filename... ");
	tmp = create_tmp_filename(4);
	g_print("OK\n");

	g_print("Temp File: %s\n", tmp);
	if (creat(tmp, 0600)==-1)
	{
		g_error("Unable to create file: %s", tmp);
		return 1;
	}
	g_print("File %s created.\n", tmp);

	
	g_print("Checking file_exists... ");
	if (file_exists(tmp))
	{
		if (file_exists(g_strdup_printf("%s%s", tmp, "speriamochenonesistaunfiledelcazzochesichiamacosi`")))
		{
			g_print("error\n");
			return 2;
		} else {
			g_print("OK\n");
		}
	} else {
		g_print("error\n");
		return 3;
	}

	g_print("Checking file_is_readable... ");
	chmod(tmp, S_IRUSR);
	if (file_is_readable(tmp))
	{
		chmod (tmp, 0);
		if (file_is_readable(tmp)) {
			g_print("error\n");
			return 4;
		} else {
			g_print("OK\n");
		}
	} else {
		g_print("error\n");
		return 5;
	}

	g_print("Checking file_is_writeable... ");
	chmod(tmp, S_IWUSR);
	if (file_is_writeable(tmp))
	{
		chmod (tmp, 0);
		if (file_is_writeable(tmp)) {
			g_print("error\n");
			return 6;
		} else {
			g_print("OK\n");
		}
	} else {
		g_print("error\n");
		return 7;
	}

	/* TODO: test the rest */
	return 0;
}

