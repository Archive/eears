/* Electric Ears Sample Play subsystem
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello and Others
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#undef REAL_USE_GTHREADS

#include <config.h>
#include <gnome.h>
#include <esd.h>
#include "plugins.h"
#include "metadefs.h"
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"

#ifndef REAL_USE_GTHREADS
/* 
 * The pthread code is stolen from gears code, written by Rusty Chris Holleman.
 */
#include <pthread.h>
#endif

extern EEars *eears;

gboolean init_plugin (PluginData *pd);

static void play (MetaAudioData mad, glong beginframe, glong endframe);
static void real_play (gpointer data);
static void stop (void);

#ifdef REAL_USE_GTHREADS
static GThread *play_thread = NULL;
#else
static pthread_t play_tid = 0;
#endif

static gboolean must_stop = FALSE;

typedef struct _MadPlayerInfo MadPlayerInfo;
struct _MadPlayerInfo { 
	MetaAudioData mad;
	glong begin_frame;
	glong end_frame;
};

#ifndef REAL_USE_GTHREADS
typedef struct _PlayerInfo PlayerInfo;
struct _PlayerInfo {
	gint sock;
};
#endif

#ifndef REAL_USE_GTHREADS
static void *real_play_wrapper (void *mpi);
static void real_play_cleanup_handler (void *info);

static void *
real_play_wrapper (void *mpi)
{
	real_play((MadPlayerInfo *)mpi);
	return NULL;
}

static void 
real_play_cleanup_handler (void *info)
{
	gint sock;

	sock = ((PlayerInfo *)info)->sock;
	esd_audio_close();
	close (sock);
	return;
}
#endif


static void
real_play (gpointer data)
{
	AudioDataMethods *adm = eears->adm;
	MadPlayerInfo *mpi = (MadPlayerInfo *) data;
	MetaAudioData mad = mpi->mad;
	glong begin = mpi->begin_frame;
	glong end = mpi->end_frame;
	esd_format_t format = 0;
	gpointer buffer = NULL;
	glong read_frames = 0;
	glong read_frames_total = 0;
	gint sock = -1;
#ifndef REAL_USE_GTHREADS
	PlayerInfo info;
	void *void_info = &info;
#endif

	buffer = g_malloc(ESD_BUF_SIZE);

	switch (adm->get_bits(mad)) {
		case 8: format |= ESD_BITS8; break;
		case 16: format |= ESD_BITS16; break;
		default: g_warning("Unsupported sample width");
			 return;
	}

	switch (adm->get_channels(mad)) {
		case 1: format |= ESD_MONO; break;
		case 2: format |= ESD_STEREO; break;
		default: g_warning("Unsupported channel count");
			 return;
	}
	format |= ESD_STREAM | ESD_PLAY;

	sock = esd_play_stream_fallback(format, adm->get_rate(mad), NULL, NULL);
	if (sock <= 0)
	{
		g_warning("Error opening esd playstream");
		return;
	}
#ifndef REAL_USE_GTHREADS
	info.sock = sock;
	pthread_cleanup_push (real_play_cleanup_handler, void_info);
	pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
#endif

#ifdef DEBUG
	g_print("before the while\n");
	if (must_stop) g_print("must stop true\n");
#endif

	/* TODO: speed up the code */
	adm->seek(mad, begin);
	while ((!must_stop) && (read_frames_total<(end-begin)))
	{
#ifdef DEBUG
		g_print("\tcycle: read_frames_total: %d\n", read_frames_total);
#endif
		read_frames = adm->read(mad, buffer, ESD_BUF_SIZE / adm->get_framesize(mad));
		read_frames_total += read_frames;
		if (read_frames_total>(end-begin)) 
		  read_frames -= (read_frames_total - (end-begin));
#ifdef DEBUG
		g_print("\t\tread_frames: %d\n", read_frames);
#endif
		if (write(sock, (buffer), read_frames * adm->get_framesize(mad))==-1)
		{
			g_warning("Error writing to esd socket");
			g_free(buffer);
			adm->close(mad);
			g_free(mpi);
			return;
		}
	}
#ifdef DEBUG
	g_print("after the while\n");
#endif
	
#ifndef REAL_USE_GTHREADS
	pthread_cleanup_pop (TRUE);
#endif

	g_free(buffer);
	adm->close(mad);
	g_free(mpi);
}

static void play (MetaAudioData mad, glong beginframe, glong endframe)
{
	MadPlayerInfo *mpi;
#ifndef REAL_USE_GTHREADS
	gint status;
#endif
	
#ifdef DEBUG
	g_print("play, begin\n");
#endif
	stop(); 
	/* TODO: free mpi */
	mpi = g_new(MadPlayerInfo, 1);
	mpi->begin_frame = beginframe;
	mpi->end_frame = endframe;
	mpi->mad = eears->adm->clone(mad); // GI use cloned mad
#ifdef REAL_USE_GTHREADS
	play_thread = g_thread_create(real_play, mpi, 0, TRUE, TRUE, G_THREAD_PRIORITY_NORMAL);
#else
	status = pthread_create (&play_tid,
			NULL,
			real_play_wrapper,
			mpi);
	if (status) { 
		g_warning("Couldn't spawn thread for play!"); 
		play_tid = 0;
	} 
#endif
#ifdef DEBUG
	g_print("play, end\n");
#endif
}

static void stop (void)
{
#ifdef DEBUG
	g_print("begin stop\n");
#endif
	must_stop = TRUE;	
#ifdef REAL_USE_GTHREADS
	/* TODO: check: do I need this? */
	if (play_thread) g_thread_join(play_thread);
	play_thread = NULL;
#else 
	if (play_tid != 0)
	{
		/*
		if (pthread_join(play_tid, NULL)!=0)
		{
			g_warning("error joining threads\n");
		}
		*/
		pthread_cancel(play_tid);
		play_tid = 0;
	}
		
#endif
	must_stop = FALSE;	
#ifdef DEBUG
	g_print("end stop\n");
#endif
}

static PluginIdentifier ident[] = { 
	{ 0x00000100, "audiodata_player"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{ 0x00000100, "audiodata_system"},
	{0, NULL}
};

static PluginGate gates[] = {
	{ "play", play },
	{ "stop", stop }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Esound Player";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;

#ifdef REAL_USE_GTHREADS
	if (!g_thread_supported()) { 
		g_warning("thread support needed!");
		return FALSE; 
	}
#endif
	return TRUE;
}



