
#include <config.h>
#include <gnome.h>
#include "plugins.h"

gboolean init_plugin (PluginData *pd);

static PluginIdentifier ident[] = { 
	{ 0x00000100, "sample_module"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
/*	{ 0x00000100, "audiodata_system"},*/
	{0, NULL}
};

static PluginGate gates[] = {
/*	{ "func1", func1 },
	{ "func2", func2 }*/
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Sample Module";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;
	return TRUE;
}



