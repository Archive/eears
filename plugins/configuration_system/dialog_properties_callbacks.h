/* Electric Ears Configuration module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _DIALOG_PROPERTIES_CALLBACKS_H_
#define _DIALOG_PROPERTIES_CALLBACKS_H_

#include <gnome.h>


gint
on_dialogProperties_close              (GnomeDialog     *gnomedialog,
                                        gpointer         user_data);

void
on_dialogProperties_clicked            (GnomeDialog     *gnomedialog,
                                        gint             arg1,
                                        gpointer         user_data);

void
on_clistConfigurations_select_row      (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

#endif

