/* Electric Ears Configuration module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "dialog_properties_callbacks.h"
#include "dialog_properties_interface.h"
#include "support.h"


gint
on_dialogProperties_close              (GnomeDialog     *gnomedialog,
                                        gpointer         user_data)
{
	return FALSE;
}


void
on_dialogProperties_clicked            (GnomeDialog     *dialogProperties,
                                        gint             arg1,
                                        gpointer         user_data)
{
        GtkPaned *hpaned;
	GtkFrame *frame;
	GtkCList *clist;
	GList *l;
	gint i, row;
	KnownValue *kv;
	Configuration *c;
	GString *s;
	
	hpaned = GTK_PANED(lookup_widget(GTK_WIDGET(dialogProperties), "hpanedProperties"));
	g_assert(hpaned);

	clist = GTK_CLIST(lookup_widget(GTK_WIDGET(dialogProperties), "clistConfigurations"));
	g_assert(clist);

	row = (gint) clist->selection->data;
	
	frame = gtk_object_get_data(GTK_OBJECT(hpaned), "frameConfiguration");
	g_return_if_fail(frame);
	
	l = gtk_object_get_data(GTK_OBJECT(clist), "configuration_list");
	g_return_if_fail(l);

	for (i = 0 ; l && i < row ; i++) l = g_list_next(l);
	g_assert(l);
	g_assert(l->data);
	
	c =  (Configuration*) l -> data;

	switch ( arg1 ) {
	case 0: /* button ok */
	case 1: /* button apply */
	        /* copy values from widgets to data structures */
	        for ( l = c->known_value_list; l; l = g_list_next(l)) {
		  GtkWidget *widget;
		  kv = (KnownValue*) l -> data;
		  g_assert(kv);

		  widget = gtk_object_get_data(GTK_OBJECT(frame), kv->key );
		  
		  if ( kv -> text) {
		    switch (kv -> type) {
		    case cfg_int: {
		      g_assert(kv->data);
		      *(gint*) kv->data = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON(widget));
		      break;
		    }
		    case cfg_bool: {
		      g_assert(kv->data);
		      *(gboolean*) kv->data = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(widget));
		      break;
		    }
		    case cfg_string: {
		      g_free( kv->data );
		      kv->data = g_strdup( gtk_entry_get_text( GTK_ENTRY(widget)));
		      break;
		    }
		    case cfg_selection: {
		      g_free( kv->data );
		      kv->data = g_strdup( (gchar*)gtk_object_get_user_data( GTK_OBJECT(gtk_menu_get_active( GTK_MENU(gtk_option_menu_get_menu (GTK_OPTION_MENU(widget)))))));
		      break;
		    }
		    case cfg_float: {
		      g_assert(kv->data);
		      *(gdouble*)kv->data = atof( gtk_entry_get_text( GTK_ENTRY(widget)));
		      break;
		    }
		    case cfg_color: {
		      gchar buf[100];
		      gdouble r,g,b,a;
		      g_free( kv->data );
		      gnome_color_picker_get_d( GNOME_COLOR_PICKER(widget), &r, &g, &b, &a );
		      sprintf( buf, "%f %f %f %f", r, g, b, a);
		      kv->data = g_strdup( buf );
		      break;
		    }
		    case cfg_font: {
			    g_free( kv->data );
			    kv->data = g_strdup( gnome_font_picker_get_font_name( GNOME_FONT_PICKER(widget)));
			    break;
		    }
		    case cfg_file: {
			    gchar *temp;
			    
			    temp = gnome_file_entry_get_full_path( GNOME_FILE_ENTRY(widget),
								   TRUE);
			    if (temp) {
				    g_free( kv->data );
				    kv->data = g_strdup( temp );
			    } 
			    break;
		    }
		    default:
		    }
		    /* call update function */
		    /* and notify functions */
		    if ( kv->func )
		      (kv->func)( kv -> user_data );
		    if (arg1 == 0) {
		      /* save fields in config file */
		      s = g_string_new("/EEars/");
		      g_string_append(s, c->name);
		      g_string_append(s, "/");
		      g_string_append(s, kv->key);
		      switch (kv -> type) {
		      case cfg_int :
			gnome_config_set_int( s->str, *(gint*)kv->data );
			break;
		      case cfg_string:
		      case cfg_selection:
		      case cfg_font:
		      case cfg_color:
		      case cfg_file:
			gnome_config_set_string( s->str, (gchar*)kv->data);
			break;
		      case cfg_bool:
			gnome_config_set_bool( s->str, *(gboolean*)kv->data );
			break;
		      case cfg_float:
			gnome_config_set_float( s->str, *(gdouble*)kv->data );
			break;
		      default:
		      }
		      gnome_config_sync();
		    }
		  }
		}
		break;
	case 2: /* button cancel */
	        break;
	}
}

void
on_clistConfigurations_select_row      (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
        GnomeDialog *dialogProperties;
	GtkPaned *hpaned;
	GtkFrame *frame;
	GList *l;
	gint i;

	g_assert(clist);
	g_assert(user_data);

	dialogProperties = GNOME_DIALOG(user_data);
	g_assert(dialogProperties);

	hpaned = GTK_PANED(lookup_widget(GTK_WIDGET(dialogProperties), "hpanedProperties"));
	g_assert(hpaned);

	l = gtk_object_get_data(GTK_OBJECT(clist), "configuration_list");
	g_return_if_fail(l);

	frame = gtk_object_get_data(GTK_OBJECT(hpaned), "frameConfiguration");
  
	if (frame) {
	  //g_print("delete old frame");
		gtk_widget_destroy( GTK_WIDGET(frame) );
	}
	//	g_print("%d %d\n",row,column);

	for (i = 0 ; l && i < row ; i++) l = g_list_next(l);
	g_assert(l);
	g_assert(l->data);
  
	frame = GTK_FRAME(create_frameConfiguration((Configuration*)l->data));

	gtk_container_add (GTK_CONTAINER (hpaned), GTK_WIDGET(frame));
	gtk_object_set_data_full (GTK_OBJECT (hpaned), "frameConfiguration", frame,
				  (GtkDestroyNotify) gtk_widget_unref);

	//	g_print("ciao!\n");
}

