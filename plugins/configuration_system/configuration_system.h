/* Electric Ears Configuration module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _CONFIGURATION_SYSTEM_H_
#define _CONFIGURATION_SYSTEM_H_

#include "metaconfiguration_system.h"


enum ValueType
{
  cfg_int,
  cfg_string,
  cfg_bool,
  cfg_float,
  cfg_color,
  cfg_font,
  cfg_selection,
  cfg_file,
  cfg_number_of_types
};

typedef struct _KnownValue KnownValue;

struct _KnownValue {
  gchar *key;
  gchar *arg;
  gchar *text;
  gchar *help;
  gint type;
  gpointer data;

  ConfigurationSystemUpdateFunction func;
  gpointer user_data;
};


typedef struct _Configuration Configuration;

struct _Configuration {
        gchar * name, *text, *help;   /* the name, text and help of the registered plugin */
        GList * known_value_list;
};


MetaConfiguration   cfg_new		 (gchar *name, gchar* text, gchar *help);
gboolean        cfg_save                 (MetaConfiguration mc);
void            cfg_close                (MetaConfiguration mc);
/* A word of explanation.
 * The cfg_register_item function accepts as arguments:
 * MetaConfiguration         a pointer to an opened metaconfiguration
 * gchar* type               a pointer to a string indicating the type of element
 *                           ("string", "color", "font", "int"...)
 * gchar *key                the key for the customizable element (will be used 
 *                           for searching and storing in conf. files), 
 *                           accepts gnome_config '=default' constructs.
 * gchar *arg                optional arguments to allow customisation of ranges...
 *                           for "string"      arg = "%d", &max_lenght  
 *                           for "int"         arg = "%d %d %d", &min, &step, &max
 *                           for "float"       arg = "%lf %lf %lf", &min, &step, &max
 * gchar *text               the visible text in the customisation dialog
 * gchar *help               the help text in the customisation dialog
 * ConfigurationSystemUpdateFunction    a function which is called when the user changes a setting
 * gpointer user_data        a pointer to user data to be passed to 
 *                           ConfigurationSystemUpdateFunction
 */
gboolean        cfg_register_item        (MetaConfiguration mc, gchar* type,
					  gchar *key, gchar *arg, 
					  gchar *text, gchar *help, 
					  ConfigurationSystemUpdateFunction func, 
					  gpointer user_data);
        
gboolean        cfg_get_bool             (MetaConfiguration mc, gchar *key);
void            cfg_set_bool             (MetaConfiguration mc, gchar *key, gboolean value);
gint            cfg_get_int              (MetaConfiguration mc, gchar *key);
void            cfg_set_int              (MetaConfiguration mc, gchar *key, gint value);
gdouble         cfg_get_float            (MetaConfiguration mc, gchar *key);
void            cfg_set_float            (MetaConfiguration mc, gchar *key, gfloat value);
gchar*          cfg_get_color            (MetaConfiguration mc, gchar *key);
void            cfg_set_color            (MetaConfiguration mc, gchar *key, gchar* value);
gchar*          cfg_get_string           (MetaConfiguration mc, gchar *key);
void            cfg_set_string           (MetaConfiguration mc, gchar *key, gchar* value);

void            cfg_edit_preferences     (void);

#endif /* _CONFIGURATION_SYSTEM_H_ */
