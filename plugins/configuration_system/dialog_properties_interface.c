/* Electric Ears Configuration module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "dialog_properties_callbacks.h"
#include "dialog_properties_interface.h"
#include "configuration_system.h"
#include "support.h"

GtkWidget*
create_dialogProperties (GList *configuration_list)
{
        GtkWidget *dialogProperties;
	GtkWidget *dialog_vbox1;
	GtkWidget *hpanedProperties;
	GtkWidget *scrolledwindowConfigurations;
	GtkWidget *clistConfigurations;
	GtkWidget *dialog_action_area1;
	GtkWidget *buttonOK;
	GtkWidget *buttonApply;
	GtkWidget *buttonCancel;
	GtkTooltips *tooltips;
	GList *l=NULL;

	tooltips = gtk_tooltips_new ();

	dialogProperties = gnome_dialog_new (_("Electric Ears Properties"), NULL);
	gtk_object_set_data (GTK_OBJECT (dialogProperties), "dialogProperties", dialogProperties);
	//  GTK_WINDOW (dialogProperties)->type = GTK_WINDOW_DIALOG;
	gtk_window_set_default_size (GTK_WINDOW (dialogProperties), 600, 400);
	gtk_window_set_policy (GTK_WINDOW (dialogProperties), FALSE, TRUE, FALSE);

	dialog_vbox1 = GNOME_DIALOG (dialogProperties)->vbox;
	gtk_object_set_data (GTK_OBJECT (dialogProperties), "dialog_vbox1", dialog_vbox1);
	gtk_widget_show (dialog_vbox1);

	hpanedProperties = gtk_hpaned_new ();
	gtk_widget_ref (hpanedProperties);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "hpanedProperties", hpanedProperties,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hpanedProperties);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), hpanedProperties, TRUE, TRUE, 0);
	gtk_paned_set_gutter_size (GTK_PANED (hpanedProperties), 10);
	gtk_paned_set_position (GTK_PANED (hpanedProperties), 180);

	scrolledwindowConfigurations = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_ref (scrolledwindowConfigurations);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "scrolledwindowConfigurations", scrolledwindowConfigurations,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (scrolledwindowConfigurations);
	gtk_container_add (GTK_CONTAINER (hpanedProperties), scrolledwindowConfigurations);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindowConfigurations), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

	clistConfigurations = gtk_clist_new (1);
	gtk_widget_ref (clistConfigurations);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "clistConfigurations", clistConfigurations,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (clistConfigurations);
	gtk_container_add (GTK_CONTAINER (scrolledwindowConfigurations), clistConfigurations);
	gtk_clist_set_column_width (GTK_CLIST (clistConfigurations), 0, 80);
	gtk_clist_set_selection_mode (GTK_CLIST (clistConfigurations), GTK_SELECTION_BROWSE);
	gtk_clist_column_titles_hide (GTK_CLIST (clistConfigurations));
	gtk_clist_set_column_width( GTK_CLIST (clistConfigurations), 1, 200 );

	/*************************************************************************/
	/* set up the clist */

	gtk_clist_freeze(GTK_CLIST (clistConfigurations));

	for (l = configuration_list; l ; l = g_list_next(l))
	        gtk_clist_append(GTK_CLIST (clistConfigurations), 
				 &((Configuration*)l->data) -> text );
	
	gtk_clist_thaw(GTK_CLIST (clistConfigurations));
	
	gtk_object_set_data(GTK_OBJECT(clistConfigurations), "configuration_list", (gpointer)configuration_list);

	/*************************************************************************/


	dialog_action_area1 = GNOME_DIALOG (dialogProperties)->action_area;
	gtk_object_set_data (GTK_OBJECT (dialogProperties), "dialog_action_area1", dialog_action_area1);
	gtk_widget_show (dialog_action_area1);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area1), 8);

	gnome_dialog_append_button (GNOME_DIALOG (dialogProperties), GNOME_STOCK_BUTTON_OK);
	buttonOK = g_list_last (GNOME_DIALOG (dialogProperties)->buttons)->data;
	gtk_widget_ref (buttonOK);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "buttonOK", buttonOK,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (buttonOK);
	GTK_WIDGET_SET_FLAGS (buttonOK, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip (tooltips, buttonOK, _("Accept and save the changes"), NULL);

	gnome_dialog_append_button (GNOME_DIALOG (dialogProperties), GNOME_STOCK_BUTTON_APPLY);
	buttonApply = g_list_last (GNOME_DIALOG (dialogProperties)->buttons)->data;
	gtk_widget_ref (buttonApply);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "buttonApply", buttonApply,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (buttonApply);
	GTK_WIDGET_SET_FLAGS (buttonApply, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip (tooltips, buttonApply, _("Accept the changes but do not save"), NULL);

	gnome_dialog_append_button (GNOME_DIALOG (dialogProperties), GNOME_STOCK_BUTTON_CANCEL);
	buttonCancel = g_list_last (GNOME_DIALOG (dialogProperties)->buttons)->data;
	gtk_widget_ref (buttonCancel);
	gtk_object_set_data_full (GTK_OBJECT (dialogProperties), "buttonCancel", buttonCancel,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (buttonCancel);
	GTK_WIDGET_SET_FLAGS (buttonCancel, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip (tooltips, buttonCancel, _("Cancel the changes and close dialog"), NULL);
  
	gtk_signal_connect (GTK_OBJECT (dialogProperties), "close",
			    GTK_SIGNAL_FUNC (on_dialogProperties_close),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (dialogProperties), "clicked",
			    GTK_SIGNAL_FUNC (on_dialogProperties_clicked),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (clistConfigurations), "select_row",
			    GTK_SIGNAL_FUNC (on_clistConfigurations_select_row),
			    (gpointer)dialogProperties);

	gtk_object_set_data (GTK_OBJECT (dialogProperties), "tooltips", tooltips);

	gtk_clist_select_row (GTK_CLIST (clistConfigurations),0,0);

	return dialogProperties;
}

GtkWidget *
create_frameConfiguration(Configuration *c)
{
        GList *l;
	GtkWidget *frameConfiguration;
	GtkWidget *table;

	GtkTooltips *tooltips;
	gint row = 1;

	tooltips = gtk_tooltips_new ();
	  
	frameConfiguration = gtk_frame_new ( c->text );
	gtk_widget_ref (frameConfiguration);
	gtk_widget_show (frameConfiguration);


	table = gtk_table_new ( row, 2, FALSE);
	gtk_widget_ref (table);
	gtk_object_set_data_full (GTK_OBJECT (frameConfiguration), "table", table,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (frameConfiguration), table);
	gtk_container_set_border_width (GTK_CONTAINER (table), 8);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	  
	for ( l = c->known_value_list; l; l = g_list_next(l)) {
	        GtkObject *adjustment = NULL;
		GtkWidget *widget = NULL;
		GtkWidget *label = NULL;
		KnownValue *kv;
    
		kv = (KnownValue*) l -> data;
		g_assert(kv);
		    
		if ( kv -> text) {
		        gtk_table_resize(GTK_TABLE(table), row, 2);

			switch (kv -> type) {
			
			case cfg_int: {
				/* extracts arguments */
				gint lower=1, upper=100, step=1;
				if ( kv -> arg ) sscanf( kv->arg, "%d %d %d", &lower, &step, &upper );
			
				adjustment = gtk_adjustment_new (1, lower, upper, step, 0, 0);
				widget = gtk_spin_button_new (GTK_ADJUSTMENT (adjustment), 1, 0);
				gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (widget), TRUE);
				label = gtk_label_new ( kv->text );
			
				gtk_adjustment_set_value( GTK_ADJUSTMENT(adjustment), *(gint*) kv->data);
				break;
			}
			case cfg_string: {
				/* extracts arguments */
				gint len=100;
				if ( kv -> arg ) sscanf( kv->arg, "%d", &len );
			
				widget = gtk_entry_new_with_max_length ( len );
				label = gtk_label_new ( kv->text );
			
				gtk_entry_set_text( GTK_ENTRY(widget), (gchar*)kv->data );
				break;
			}
			case cfg_selection: {
				GtkWidget *widget_menu, *menuitem;
				/* extracts arguments */
				gchar *c, *n, *o;
				gint item=0,i=0;

				widget = gtk_option_menu_new ();

				widget_menu = gtk_menu_new ();
				if ( kv -> arg ) {
					o = g_strdup(kv->arg);
					for ( c = o; c; c = n, i++) {
						if (!(*c)) break;
						n = strchr(c,'\n');
						if (n) { *n = '\0'; n++; }
						menuitem = gtk_menu_item_new_with_label (c);
						gtk_object_set_user_data( GTK_OBJECT(menuitem), g_strdup(c) );
						gtk_widget_show (menuitem);
						gtk_menu_append (GTK_MENU (widget_menu), menuitem);
						if (strcmp (c , (gchar*)kv->data)==0) item=i;
					}
					g_free(o);
				}
			  
				gtk_option_menu_set_menu (GTK_OPTION_MENU (widget), widget_menu);
				gtk_option_menu_set_history(GTK_OPTION_MENU (widget), item);
				label = gtk_label_new ( kv->text );
			
				break;
			}
			case cfg_bool: {
				widget = gtk_check_button_new_with_label ( kv->text );
				label = NULL;
			
				gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(widget), *(gboolean*)kv->data);
				break;
			}
			case cfg_float: {
				gchar buf[100];
				widget = gtk_entry_new ();
				label = gtk_label_new ( kv->text );
			
				snprintf(buf, 100, "%f", *(gdouble*) kv->data);
				gtk_entry_set_text( GTK_ENTRY(widget), buf );	
				break;
			}
			case cfg_color: {
				gdouble r=0.0,g=0.0,b=0.0,a=0.0;
				widget = gnome_color_picker_new ();
				label = gtk_label_new ( kv->text );
			
				sscanf( (gchar*) kv->data, "%lf %lf %lf %lf", &r, &g, &b, &a);
				gnome_color_picker_set_d( GNOME_COLOR_PICKER(widget), r, g, b, a);
				break;
			}
			case cfg_font: {
				widget = gnome_font_picker_new ();
				label = gtk_label_new ( kv->text );
			
				gnome_font_picker_set_font_name( GNOME_FONT_PICKER(widget), 
								 (gchar*)kv->data);
				gnome_font_picker_set_mode( GNOME_FONT_PICKER(widget),
							    GNOME_FONT_PICKER_MODE_FONT_INFO);
				gnome_font_picker_fi_set_use_font_in_label( GNOME_FONT_PICKER(widget), TRUE,
									    14);
				gnome_font_picker_fi_set_show_size( GNOME_FONT_PICKER(widget), TRUE);
				break;
			}
			case cfg_file: {
				widget = gnome_file_entry_new (NULL, 
							       _("Select a file"));
				label = gtk_label_new ( kv->text );
				if (kv->arg) {
					if ( strcasecmp( kv->arg, "path") == 0) {
						gnome_file_entry_set_directory(GNOME_FILE_ENTRY(widget), 
									       TRUE);
					}
				}
				gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(widget))), 
						   (gchar*)kv->data);
				gnome_file_entry_set_modal( GNOME_FILE_ENTRY(widget),
							    TRUE);
				break;
			}
			default:
				break;
			}
			if (widget) {
				gtk_widget_ref (widget);
				gtk_object_set_data_full (GTK_OBJECT (frameConfiguration), kv->key , widget,
							  (GtkDestroyNotify) gtk_widget_unref);
				gtk_widget_show (widget);
				gtk_table_attach (GTK_TABLE (table), widget, 0, 1, row, row + 1,
						  (GtkAttachOptions) GTK_EXPAND,
						  (GtkAttachOptions) (0), 0, 0);
				if ( kv->help ) gtk_tooltips_set_tip (tooltips, widget, kv->help, NULL);
			}
			if (label) {
				gtk_widget_ref (label);
				gtk_widget_show (label);
				gtk_table_attach (GTK_TABLE (table), label, 1, 2, row, row +1,
						  (GtkAttachOptions) GTK_EXPAND|GTK_FILL,
						  (GtkAttachOptions) 0, 0, 0);
			}
			row ++;
		}
	}
	if ( c -> help ) {
	        GtkWidget *scrolledwindow;
		GtkWidget *text;
	    
		scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_ref (scrolledwindow);
		gtk_object_set_data_full (GTK_OBJECT (frameConfiguration), "scrolledwindow", scrolledwindow,
					  (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (scrolledwindow);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	    
		text = gtk_text_new (NULL, NULL);
		gtk_widget_ref (text);
		gtk_object_set_data_full (GTK_OBJECT (frameConfiguration), "text", text,
					  (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (text);
		gtk_container_add (GTK_CONTAINER (scrolledwindow), text);
	    
		gtk_table_resize(GTK_TABLE(table), row, 2);
		gtk_table_attach (GTK_TABLE (table), scrolledwindow, 0, 2, row, row + 1,
				  (GtkAttachOptions) GTK_FILL,
				  (GtkAttachOptions) GTK_FILL|GTK_EXPAND, 0, 0);
		gtk_text_set_editable(GTK_TEXT(text),FALSE);
		gtk_text_set_word_wrap(GTK_TEXT(text),TRUE);
		gtk_text_insert(GTK_TEXT(text),NULL,NULL,NULL,c->help, strlen(c->help));
	}
	  
	return frameConfiguration;
}
