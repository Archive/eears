/* Electric Ears Configuration module
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include "plugins.h"
#include "metadefs.h"
#include "configuration_system.h"
#include "dialog_properties_interface.h"

static void        cfg_print();

static KnownValue *cfg_find_kv(MetaConfiguration mc, gchar *key) ;


GList *global_configuration_list = NULL;


gboolean init_plugin (PluginData *pd);


static PluginIdentifier ident[] = { 
	{0x00000100, "configuration_system"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{0, NULL}
};

static PluginGate gates[] = {
	{ "new",	cfg_new },
	{ "save", cfg_save     },
	{ "close", cfg_close  },
	{ "register_item", cfg_register_item  },         
	{ "get_bool", cfg_get_bool   },
	{ "set_bool", cfg_set_bool },
	{ "get_int", cfg_get_int  },
	{ "set_int", cfg_set_int   },
	{ "get_float", cfg_get_float },
	{ "set_float", cfg_set_float },
	{ "get_color", cfg_get_color  },
	{ "set_color", cfg_set_color },
	{ "get_string", cfg_get_string    },  
	{ "set_string", cfg_set_string    },
	{ "edit_preferences", cfg_edit_preferences }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Configuration Management System";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;

	return TRUE;
}

/***************************************************************************/

static void 
cfg_print()
{
        GList *global, *l;

	for (global = global_configuration_list; global; global = g_list_next(global)) {

	        g_print("Configuration: %s\n", ((Configuration*)global->data)->name);
		
		for ( l = ((Configuration*)global->data)->known_value_list; 
		      l; l = g_list_next(l)) {
		        g_print("    key: %s type: %d\n", 
				((KnownValue*)l->data)->key,
				((KnownValue*)l->data)->type);
		}
	}
}

static KnownValue *
cfg_find_kv(MetaConfiguration mc, gchar *key) 
{
        Configuration *c;
	GList *l;
	KnownValue *kv = NULL;								
	g_assert (mc);								
	c = (Configuration*)mc;							
										
	g_assert (key);
	for ( l = c->known_value_list; l; l = l->next)
		if (strcmp(key, ((KnownValue*)l->data)->key)==0) {
			kv = (KnownValue*)l->data;
			break;
		}
	return kv;
}

/***************************************************************************/

MetaConfiguration   
cfg_new(gchar *name, gchar* text, gchar *help)
{
        Configuration *new;
        g_assert(name);

	new = g_new( Configuration, 1 );

	new -> name = g_strdup(name);
	if ( text ) new -> text = g_strdup(text); else new -> text = NULL;
	if ( help ) new -> help = g_strdup(help); else new -> help = NULL;
 
	new -> known_value_list = NULL;

	global_configuration_list = g_list_append(global_configuration_list,
						  new);

	return new;
}

void
cfg_close(MetaConfiguration mc)
{
}

gboolean
cfg_save(MetaConfiguration mc)
{
        Configuration *c;
	GList *list;
	g_assert (mc);
	c = (Configuration*)mc;
  
	list = c -> known_value_list;

	while (list) {
	        GString *s;
		KnownValue *kv;
	  
		kv = (KnownValue*) (list -> data);

		s = g_string_new("/EEars/");
		g_string_append(s, c->name);
		g_string_append(s, "/");
		g_string_append(s, kv->key);
		switch ( kv->type ) {
		case cfg_int:
		  gnome_config_set_int( s->str, *(gint*)(kv->data) );
		  break;
		case cfg_bool:
		  gnome_config_set_bool( s->str, *(gboolean*)(kv->data) );
		  break;
		case cfg_float:
		  gnome_config_set_float( s->str, *(gdouble*)(kv->data) );
		  break;
		case cfg_string:
		case cfg_font:
		case cfg_file:
		case cfg_selection:
		case cfg_color:
		  gnome_config_set_string( s->str, (gchar*)kv->data );
		  break;
	        }
		list = g_list_next(list);
	}
	
	return TRUE;
}


gchar *cfg_type_strings[] = { "int", "string", "bool", "float",
			      "color", "font", "selection", "file", NULL };

gboolean
cfg_register_item(MetaConfiguration mc, gchar* type,
		  gchar *key, gchar *arg, 
		  gchar *text, gchar *help, 
		  ConfigurationSystemUpdateFunction func, gpointer user_data)
{
        Configuration *c;							
	KnownValue *kv;								
	GString *s;
	gint i;

	g_assert (mc);								
	c = (Configuration*)mc;							
	g_assert (type);
	g_assert (key);								

	kv = g_new( KnownValue, 1 );					

	kv -> type = cfg_number_of_types;
	for (i =0; i< cfg_number_of_types; i++) {
	  if (strcmp(cfg_type_strings[i], type) == 0) 
	    kv -> type = i;
	}
	if (kv -> type == cfg_number_of_types) {
	  g_warning("Unknown registered type %s", type);
	  g_free( kv );
	  return FALSE;
	}

	kv -> key = g_strdup( key ); *strchr ( kv->key, '=') = '\0';		
	kv -> text = ( text ? g_strdup( text ) : NULL);
	kv -> arg = ( arg ? g_strdup( arg ) : NULL);
	kv -> help = ( help ? g_strdup( help ) : NULL);
	kv -> func = func;							
	kv -> user_data = user_data;
	kv -> data = NULL;	
       	c -> known_value_list = g_list_append( c -> known_value_list, kv );

	s = g_string_new("/EEars/");
	g_string_append(s, c->name);
	g_string_append(s, "/");
	g_string_append(s, key);

	switch (kv -> type) {
	case cfg_int :
	  kv->data = (gpointer)g_new(gint ,1 ); 
	  *(gint*)kv->data = gnome_config_get_int( s->str );
	  break;
	case cfg_string:
	  kv->data = (gpointer)g_strdup(gnome_config_get_string( s->str ));
	  break;
	case cfg_selection:
	  kv->data = (gpointer)g_strdup(gnome_config_get_string( s->str ));
	  break;
	case cfg_file:
	  kv->data = (gpointer)g_strdup(gnome_config_get_string( s->str ));
	  break;
	case cfg_bool:
	  kv->data = (gpointer)g_new(gboolean, 1); 
	  *(gboolean*)kv->data = gnome_config_get_bool( s->str );
	  break;
	case cfg_float:
	  kv->data = (gpointer)g_new(gdouble, 1); 
	  *(gdouble*)kv->data = gnome_config_get_float( s->str );
	  break;
	case cfg_color:
	  kv->data = (gpointer)g_strdup(gnome_config_get_string( s->str ));
	  break;
	case cfg_font:
	  kv->data = (gpointer)g_strdup(gnome_config_get_string( s->str ));
	  break;
	default:
	}

	g_string_free(s, TRUE);
	return TRUE;
}

/*************************************************************************/

        
gboolean        cfg_get_bool             (MetaConfiguration mc, gchar *key)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	return *(gboolean*)(kv -> data);
}

void            cfg_set_bool             (MetaConfiguration mc, gchar *key, gboolean value)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	*(gboolean*)(kv -> data) = value;
}

gint            cfg_get_int              (MetaConfiguration mc, gchar *key)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	return *(gint*)(kv -> data);
}

void            cfg_set_int              (MetaConfiguration mc, gchar *key, gint value)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	*(gint*)(kv -> data) = value;
}


gdouble         cfg_get_float            (MetaConfiguration mc, gchar *key)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	return *(gdouble*)(kv -> data);
}

void            cfg_set_float            (MetaConfiguration mc, gchar *key, gfloat value)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	*(gdouble*)(kv -> data) = value;
}

gchar*          cfg_get_color            (MetaConfiguration mc, gchar *key)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	return kv -> data;
}

void            cfg_set_color            (MetaConfiguration mc, gchar *key, gchar* value)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	g_free(kv -> data);
	kv -> data = g_strdup( value );
}

gchar*          cfg_get_string           (MetaConfiguration mc, gchar *key)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	return kv -> data;
}

void cfg_set_string(MetaConfiguration mc, gchar *key, gchar* value)
{
        KnownValue *kv;
  
	kv = cfg_find_kv(mc, key);
	g_assert( kv );

	g_free(kv -> data);
	kv -> data = g_strdup( value );
}


/*************************************************************************/


void 
cfg_edit_preferences(void)
{
        GnomeDialog *dialog;

	dialog = GNOME_DIALOG(create_dialogProperties(global_configuration_list));

	gnome_dialog_run_and_close(dialog);


}
