/* Electric Ears Sample Play subsystem
 * Copyright (C) 1998 - 1999 A. Bosio, G. Iachello and Others
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/soundcard.h>
#include "plugins.h"
#include "metadefs.h"
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"

#include <pthread.h>

#define BUFFER_SIZE 8192

extern EEars *eears;

gboolean init_plugin (PluginData *pd);

static void play (MetaAudioData mad, glong beginframe, glong endframe);
static void real_play (gpointer data);
static void stop (void);

static pthread_t play_tid = 0;

static volatile gboolean must_stop = FALSE;

typedef struct _MadPlayerInfo MadPlayerInfo;
struct _MadPlayerInfo { 
	MetaAudioData mad;
	glong begin_frame;
	glong end_frame;
};

typedef struct _PlayerInfo PlayerInfo;
struct _PlayerInfo {
	gint audio_fd;
	gpointer buffer;
};

static void *real_play_wrapper (void *mpi);
static void real_play_cleanup_handler (void *info);

static void *
real_play_wrapper (void *mpi)
{
	real_play((MadPlayerInfo *)mpi);
	return NULL;
}

static void 
real_play_cleanup_handler (void *info)
{
	g_free(((PlayerInfo *)info)->buffer);
	close (((PlayerInfo *)info)->audio_fd);
	return;
}

static void
real_play (gpointer data)
{
	AudioDataMethods *adm = eears->adm;
	MadPlayerInfo *mpi = (MadPlayerInfo *) data;
	MetaAudioData mad = mpi->mad;
	glong begin = mpi->begin_frame;
	glong end = mpi->end_frame;
	gpointer buffer = NULL;
	glong read_frames = 0;
	glong read_frames_total = 0;
	PlayerInfo info;
	void *void_info = &info;
	gint format;
	gint stereo;
	gint speed;
	gint audio_fd;	

	g_return_if_fail(adm);

	if ((audio_fd = open("/dev/dsp", O_WRONLY, 0)) == -1)  
	{ /* Opening device failed */ 
		g_warning("Unable to open /dev/dsp");
		return;
        }  
	info.audio_fd = audio_fd;

	buffer = g_malloc(BUFFER_SIZE);

	info.buffer = buffer;

	switch (adm->get_bits(mad)) {
		case 8: 
			format = AFMT_U8;
			if (ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format)==-1) {
				g_error("Unsupported sample width");
				return;
			}
			if (format != AFMT_U8) {
				g_warning("Unsupported sample width");
				return;
			}
			break;
		case 16: 
			format = AFMT_S16_LE;
			if (ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format)==-1) {
				g_error("Unsupported sample width");
				return;
			}
			if (format != AFMT_S16_LE) {
				g_warning("Unsupported sample width");
				return;
			}
			break;
		default: 
			 g_warning("Unsupported sample width");
			 return;
	}

	switch (adm->get_channels(mad)) {
		case 1: 
			stereo = 0;	
			if (ioctl(audio_fd, SNDCTL_DSP_STEREO, &stereo)==-1) { 
				g_error("Unsupported channel count");
				return;
			}
			if (stereo != 0) {
				g_warning("Unsupported channel count");
				return;
			}
			break;
		case 2: 
			stereo = 1;	
			if (ioctl(audio_fd, SNDCTL_DSP_STEREO, &stereo)==-1)
			{ 
				g_error("Unsupported channel count");
				return;
			}
			if (stereo != 1)
			{
				g_warning("Unsupported channel count");
				return;
			}
			break;
		default: 
			g_warning("Unsupported channel count");
			return;
	}
	speed = adm->get_rate(mad);
	if (ioctl(audio_fd, SNDCTL_DSP_SPEED, &speed)==-1)  
	{ /* Fatal error */  
		g_error("Error setting rate.");  
		return;
	}  

	pthread_cleanup_push (real_play_cleanup_handler, void_info);
	pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);

#ifdef DEBUG
	g_print("before the while\n");
	if (must_stop) g_print("must stop true\n");
#endif

	/* TODO: speed up the code */
	adm->seek(mad, begin);
	while ((!must_stop) && (read_frames_total<(end-begin)))
	{
#ifdef DEBUG
		g_print("\tcycle: read_frames_total: %d\n", read_frames_total);
#endif
		read_frames = adm->read(mad, buffer, BUFFER_SIZE / adm->get_framesize(mad));
		read_frames_total += read_frames;
		if (read_frames_total>(end-begin)) 
		  read_frames -= (read_frames_total - (end-begin));
#ifdef DEBUG
		g_print("\t\tread_frames: %d\n", read_frames);
#endif
		if (write(audio_fd, (buffer), read_frames * adm->get_framesize(mad))==-1)
		{
			g_warning("Error writing to /dev/dsp");
			g_free(buffer);
			adm->close(mad);
			g_free(mpi);
			return;
		}
	}
#ifdef DEBUG
	g_print("after the while\n");
#endif
	
	pthread_cleanup_pop (TRUE);

/*	g_free(buffer);	*/
	adm->close(mad);
	g_free(mpi);
}

static void play (MetaAudioData mad, glong beginframe, glong endframe)
{
	MadPlayerInfo *mpi;
	gint status;
#ifdef DEBUG
	g_print("play, begin\n");
#endif
	stop(); 
	/* TODO: free mpi */
	mpi = g_new(MadPlayerInfo, 1);
	mpi->begin_frame = beginframe;
	mpi->end_frame = endframe;
	mpi->mad = eears->adm->clone(mad); // GI use cloned mad
	status = pthread_create (&play_tid,
			NULL,
			real_play_wrapper,
			mpi);
	if (status) { 
		g_warning("Couldn't spawn thread for play!"); 
		play_tid = 0;
	} 
#ifdef DEBUG
	g_print("play, end\n");
#endif
}

static void stop (void)
{
#ifdef DEBUG
	g_print("begin stop\n");
#endif
	must_stop = TRUE;	
	if (play_tid != 0)
	{
		
		if (pthread_join(play_tid, NULL)!=0)
		{
			g_warning("error joining threads\n");
		}
		
		/*pthread_cancel(play_tid);*/
		play_tid = 0;
	}
		
	must_stop = FALSE;	
#ifdef DEBUG
	g_print("end stop\n");
#endif
}

static PluginIdentifier ident[] = { 
	{ 0x00000100, "audiodata_player"},
	{0, NULL}
};

static PluginIdentifier deps[] = { 
	{ 0x00000100, "audiodata_system"},
	{0, NULL}
};

static PluginGate gates[] = {
	{ "play", play },
	{ "stop", stop }
};

gboolean init_plugin (PluginData *pd)
{
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->title = "Open Sound System Player";
	pd->ident = ident;
	pd->dependencies = deps;
	pd->gates = gates;

	return TRUE;
}



