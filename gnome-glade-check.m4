dnl
dnl GNOME_GLADE_HOOK (script-if-glade-found, failflag)
dnl
dnl If failflag is "failure", script aborts due to lack of libglade
dnl 
dnl Check for availability of the libglade library
dnl the glade parser uses libz if available too
dnl

AC_DEFUN([GNOME_GLADE_HOOK],[
	AC_PATH_PROG(GNOME_CONFIG,gnome-config,no)
	if test "$GNOME_CONFIG" = no; then
		if test x$2 = xfailure; then
			AC_MSG_ERROR(Could not find gnome-config)
		fi
	fi
	AC_CHECK_LIB(glade-gnome, glade_gnome_init, [
		$1
		GNOME_GLADE_LIB=`$GNOME_CONFIG --libs libglade`
	], [
		if test x$2 = xfailure; then 
			AC_MSG_ERROR(Could not link sample glade program)
		fi
	], `$GNOME_CONFIG --libs libglade`)
	AC_SUBST(GNOME_GLADE_LIB)
])

AC_DEFUN([GNOME_GLADE_CHECK], [
	GNOME_GLADE_HOOK([],failure)
])
