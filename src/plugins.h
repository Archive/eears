/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _PLUGINS_H_
#define _PLUGINS_H_

#include <gmodule.h>


/* typedef enum { */
/* 	PLUGIN_UNKNOWN, */
/* 	PLUGIN_EXIST, */
/* 	PLUGIN_REGISTERED */
/* } PluginStatus; */


typedef struct _PluginIdentifier PluginIdentifier;

struct _PluginIdentifier
{
	glong version;
	gchar *type;
};

typedef struct _PluginGate PluginGate;

struct _PluginGate
{
	gchar    *name;
	gpointer func;
};



typedef struct _PluginData PluginData;

struct _PluginData 
{
	GModule* handle;
	gchar            *file;
	gboolean (*init_plugin) (PluginData *);

	/* the following fields are to be filled in by the plugin */
	gboolean (*configure)   (PluginData *);
	void 	 (*cleanup)     (PluginData *);
	
	gchar            *title;
	gchar            *author;
	PluginIdentifier *ident;
	PluginIdentifier *dependencies;
	PluginGate       *gates;

/* 	OLD gpointer         methods; */
};

GList *		plugins_init 			(void);
void		plugins_cleanup			(GList *all_plugins_list);
void            plugins_configure_all           (GList *all_plugins_list);
gpointer*       plugins_get_method              (GList *all_plugins_list, 
						 gchar* type, 
						 gchar* methodname, 
						 gchar* instance);

#endif /* _PLUGINS_H_ */
