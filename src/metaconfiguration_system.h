/* Electric Ears 
 * Copyright (C) 1999 A.Bosio and G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/* A word of explanation.
 * The register_item function accepts as arguments:
 * MetaConfiguration         a pointer to an opened metaconfiguration
 * gchar* type               a pointer to a string indicating the type of element
 *                           ("string", "color", "int"...)
 * gchar *key                the key for the customizable element (will be used 
 *                           for searching and storing in conf. files), 
 *                           accepts gnome_config '=default' constructs.
 * gchar *arg                optional arguments to allow customisation of ranges...
 *                           for "string"      arg = "%d", &max_lenght  
 *                           for "int"         arg = "%d %d %d", &min, &step, &max
 *                           for "float"       arg = "%lf %lf %lf", &min, &step, &max
 * gchar *text               the visible text in the customisation dialog
 * gchar *help               the help text in the customisation dialog
 * ConfigurationSystemUpdateFunction    a function which is called when the user changes a setting
 * gpointer user_data        a pointer to user data to be passed to 
 *                           ConfigurationSystemUpdateFunction
 */

#ifndef __META_CONFIGURATION_SYSTEM_H_
#define __META_CONFIGURATION_SYSTEM_H_

typedef void (*ConfigurationSystemUpdateFunction) (gpointer data);

typedef gpointer MetaConfiguration;

typedef struct _ConfigurationSystemMethods ConfigurationSystemMethods;

struct _ConfigurationSystemMethods {
        
        MetaConfiguration (*create)                (gchar *name, gchar* text, gchar *help);
        gboolean        (*save)                 (MetaConfiguration);
        void            (*close)                (MetaConfiguration);
  
        gboolean        (*register_item)        (MetaConfiguration, gchar* type,
					         gchar *key, gchar *arg, 
						 gchar *text, gchar *help, 
						 ConfigurationSystemUpdateFunction, gpointer user_data);
        
        gboolean        (*get_bool)             (MetaConfiguration, gchar *key);
        void            (*set_bool)             (MetaConfiguration, gchar *key, gboolean value);
        gint            (*get_int)              (MetaConfiguration, gchar *key);
        void            (*set_int)              (MetaConfiguration, gchar *key, gint value);
        gdouble         (*get_float)            (MetaConfiguration, gchar *key);
        void            (*set_float)            (MetaConfiguration, gchar *key, gfloat value);
        gchar*          (*get_color)            (MetaConfiguration, gchar *key);
        void            (*set_color)            (MetaConfiguration, gchar *key, gchar* value);
        gchar*          (*get_string)           (MetaConfiguration, gchar *key);
        void            (*set_string)           (MetaConfiguration, gchar *key, gchar* value);

        void            (*edit_preferences)     (void);
};

#endif /* __META_CONFIGURATION_SYSTEM_H_ */

