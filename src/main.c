/* Electric Ears
 * Copyright (C) 2000  A.Bosio, G.Iachello and Others
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include "metadefs.h"
#include "eears.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define SPLASH_HEIGHT 180
#define SPLASH_WIDTH 500
static void start_splash(void);

static gboolean play_mode = FALSE;

static struct poptOption options[] = {
	{
		"play",
		'p',
		POPT_ARG_NONE,
		&play_mode,
		0,
		N_("Play the files specified on the command line."),
		NULL
	},
	{NULL, '\0', 0, NULL, 0, NULL, NULL}
};

extern EEars *eears;
void parse_args (poptContext ctx);
static void session_die(GnomeClient* client, gpointer client_data);
static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data);

void 
parse_args (poptContext ctx)
{
	MetaAudioData mad;
	gchar **leftovers;
	gint i = 0;

	leftovers = (gchar **) poptGetArgs(ctx);
	if (leftovers)
	{
		for (; leftovers[i]; i++)
		{
			/* TODO: handle errors */
			mad = eears_open_file(leftovers[i], AF_FILE_UNKNOWN);
			if (play_mode && eears->mpm && mad && eears->adm) {
				eears->mpm->play(mad, 0, eears->adm->get_framecount(mad));
			}
		}
	}
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
	gchar *prefix = gnome_client_get_config_prefix (client); 
	gchar *argv[] = { "rm", "-r", NULL }; 

	argv[2] = gnome_config_get_real_path (prefix); 
	gnome_client_set_discard_command (client, 3, argv); 

	argv[0] = (gchar *) client_data;

	gnome_client_set_clone_command (client, 1, argv);
	gnome_client_set_restart_command (client, 1, argv);

	return TRUE;
}


static void
session_die(GnomeClient* client, gpointer client_data)
{
	  gtk_main_quit ();
}

static void
create_eears_dirs ()
{
	gint fd;
	gchar *home_dir = getenv("HOME");
	gchar *doteears;
	gchar *doteearsplugins;
	
	doteears =  g_strconcat (home_dir ? home_dir : "", "/.eears/", NULL);
		
	if ((fd = open (doteears, O_RDONLY)) == -1)
	{
		if (errno == ENOENT) {
			mkdir(doteears, 0777);
		} else {
			g_warning("Unable to open directory: %s", doteears);
		}
	} else {
		close(fd);
	}

	doteearsplugins = g_strconcat (doteears, "plugins/", NULL);

	if ((fd = open (doteearsplugins, O_RDONLY)) == -1)
	{
		if (errno == ENOENT) {
			mkdir(doteearsplugins, 0777);
		} else {
			g_warning("Unable to open directory: %s", doteearsplugins);
		}
	} else {
		close(fd);
	}


	g_free(doteears);
	g_free(doteearsplugins);
}

int main (int argc, char *argv[])
{
	poptContext ctx;
	EEars *eears;
	GnomeClient *client;
	gboolean restored;

	create_eears_dirs();
	
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	gnome_init_with_popt_table("EEars", VERSION, argc, argv, options, 0, &ctx);
#if 0 && defined(G_THREADS_ENABLED) && ! defined(G_THREADS_IMPL_NONE)
/* ^^^ remove this when gthread will work */
	g_thread_init (NULL);
#endif
	/* enable glade support */
	glade_gnome_init();
	
	client = gnome_master_client ();

	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
	               GTK_SIGNAL_FUNC (save_session), (gpointer) argv[0]);
	gtk_signal_connect (GTK_OBJECT (client), "die",
	               GTK_SIGNAL_FUNC (session_die), NULL);


	if (GNOME_CLIENT_CONNECTED (client) && (gnome_client_get_flags (client) & GNOME_CLIENT_RESTORED)) {
		restored = TRUE;
	} else {
		restored = FALSE;
	}

	start_splash();

	gnome_config_push_prefix (gnome_client_get_config_prefix (client));
	eears = EEARS(eears_new(restored));
	gnome_config_pop_prefix ();

	parse_args(ctx);	
		
	gtk_main();

	return 0;
}


static void
start_splash(void)
{
	GdkImlibImage *im;
	gchar *splash_filename;
	GdkWindow *window;
	GtkWidget *widget; 
	GtkPixmap *pixmap;
	/* Load the splash image and put it as default background. As
           soon as the user creates an mdi view or resizes the window,
           set the background again to the default color. */
	
	

	splash_filename = g_strconcat (PREFIX, "/share/pixmaps/eears/eears_splash.png", NULL);
  	im = gdk_imlib_load_image(splash_filename);

	if (im) {
		gchar buf[100];
		strcpy (buf, _("Electric Ears "));
		strcat (buf, VERSION );

		widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_window_set_position(widget, GTK_WIN_POS_CENTER);
		gtk_window_set_title(widget, buf);

//		gtk_widget_set_usize(widget, SPLASH_WIDTH, SPLASH_HEIGHT );
		gdk_imlib_render(im,SPLASH_WIDTH,SPLASH_HEIGHT);
		pixmap = gtk_pixmap_new(gdk_imlib_move_image(im), gdk_imlib_move_mask(im));
		gtk_container_add (GTK_CONTAINER (widget),
                             pixmap);

		gtk_widget_show (pixmap);
		gtk_widget_show (widget);

		gtk_timeout_add(2000,
				(GtkFunction)gtk_widget_destroy,
				widget);
	}

}

