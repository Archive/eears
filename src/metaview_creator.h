#ifndef __META_VIEW_CREATOR_H__
#define __META_VIEW_CREATOR_H__

typedef struct _ViewCreatorMethods ViewCreatorMethods;

struct _ViewCreatorMethods {
	GtkWidget*	(*view_creator)		(GnomeMDIChild *, gpointer);
	GtkWidget*	(*cur_view_update)	(void);
};

#endif /* __META_VIEW_CREATOR_H__  */
