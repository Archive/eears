#ifndef __META_UNDO_SYSTEM_H_
#define __META_UNDO_SYSTEM_H_

typedef struct _UndoMethods UndoMethods;

struct _UndoMethods {
	void (*new_undo_level)	(MetaAudioData);
	void (*undo)		(MetaAudioData);
	void (*redo)		(MetaAudioData);
};

#endif __META_UNDO_SYSTEM_H_
