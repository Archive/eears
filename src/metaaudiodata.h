/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __METAAUDIODATA_H
#define __METAAUDIODATA_H

#ifdef __cplusplus
extern              "C"
{
#endif				/* __cplusplus */


typedef gpointer MetaAudioData; 
typedef struct _AudioDataMethods AudioDataMethods;

struct _AudioDataMethods {
	MetaAudioData   (*create)                  (guint rate, guint bits, guint channels);
	MetaAudioData	(*open)         	(const gchar *); 
	gboolean	(*close)         	(MetaAudioData); 
	MetaAudioData	(*clone)         	(MetaAudioData); 
	glong           (*read)         	(MetaAudioData,	gpointer, gulong);
	glong           (*write)         	(MetaAudioData,	gpointer, gulong);
	gboolean        (*save)        		(MetaAudioData);
	gboolean        (*save_as)        	(MetaAudioData, const gchar *, const gint format);
	glong           (*seek)        		(MetaAudioData, gulong);
	
	void		(*rewind) 		(MetaAudioData);
		
  /* TODO should this be here? */
	void            (*free)         	(MetaAudioData);
	void            (*set_filename) 	(MetaAudioData,	const gchar *);
	void            (*set_tmp_dir)          (const gchar *);
	const gchar*    (*get_filename) 	(MetaAudioData);

	void            (*set_rate)		(MetaAudioData, gint);
	gint            (*get_rate)		(MetaAudioData);

	void            (*set_bits)		(MetaAudioData, gint);
	gint            (*get_bits)		(MetaAudioData);

	void            (*set_channels)		(MetaAudioData, gint);
	gint            (*get_channels)		(MetaAudioData);

	void            (*set_framecount)	(MetaAudioData,	glong);
	glong		(*get_framecount)	(MetaAudioData);

	gint            (*get_framesize)        (MetaAudioData);

	void 		(*reset_lastmodify) 	(MetaAudioData);

	gdouble 	(*get_lastmodify) 	(MetaAudioData);
};

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif /* __METAAUDIODATA_H */
