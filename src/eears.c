/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <glade/glade.h>

#include "plugins.h"
#include "metadefs.h"
#include "eears.h"
#include "../pixmaps/toolbar_rec.xpm"
#include "../pixmaps/toolbar_stop.xpm"

#define DEFAULT_HEIGHT 280
#define DEFAULT_WIDTH 640


void eears_class_init (EEarsClass *klass);	
void eears_init (EEars *ee);
void hook_methods(void);

static void cleanup_cb(void);
static void app_drop_cb(GtkWidget *widget, GdkDragContext *context,
		gint x, gint y, GtkSelectionData *selection_data,
		guint info, guint time, gpointer data);
void app_created_handler(GnomeMDI *mdi, GnomeApp *app);
void reply_handler(gint reply, gpointer data);
static gint remove_child_handler(GnomeMDI *mdi, GnomeMDIChild *child);
void eears_update_menus_and_toolbar_for_app(GnomeApp *app);
void eears_update_menus_and_toolbar(void);

void eears_stop_cb (void);
void eears_play_cb (void);
void eears_record_cb (void);
void eears_undo_cb (void);
void eears_redo_cb (void);
void eears_cut_cb (void);
void eears_copy_cb (void);
void eears_paste_cb (void);
void eears_crop_cb (void);
void eears_open_file_cb (void);
void eears_new_from_selection_cb (void);
void eears_open_mixer_cb (void);
void eears_preferences_cb (void);
void eears_plugins_cb (void);
void eears_save_file_cb (void);
void eears_save_as_file_cb (void);
void eears_close_file_cb (void);
void eears_select_all_cb (void);
void eears_clear_cb (void);
void eears_plugin_player_changed_cb (gpointer data);

EEars *eears;

enum {
	   TARGET_URI_LIST,
};

enum {
	TOOLBAR_OPEN,
	TOOLBAR_SAVE,
	TOOLBAR_SAVE_AS,
	TOOLBAR_SEP,
	TOOLBAR_PLAY,
	TOOLBAR_STOP,
	TOOLBAR_RECORD
};

static GnomeUIInfo eears_toolbar[] = {
	{ GNOME_APP_UI_ITEM, N_("Open"), N_("Open an existing file"), eears_open_file_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("Save"), N_("Save the current file"), eears_save_file_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("Save as"), N_("Save the current file with a new name"), eears_save_as_file_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE_AS, 0, 0, NULL },
	GNOMEUIINFO_SEPARATOR,
	{ GNOME_APP_UI_ITEM, N_("Play"), N_("Play the current selection or the sample from the insertion point"), eears_play_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_FORWARD, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("Stop"), N_("Stop palying"), eears_stop_cb, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, toolbar_stop_xpm, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("Record"), N_("Record a new sample"), eears_record_cb, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, toolbar_rec_xpm, 0, 0, NULL },
	GNOMEUIINFO_SEPARATOR,
	{ GNOME_APP_UI_ITEM, N_("Mixer"), N_("Open the mixer"), eears_open_mixer_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_VOLUME, 0, 0, NULL },

	GNOMEUIINFO_END
};

enum {
	NEW_MENU_SELECTION,
	NEW_MENU_RECORD
};

static GnomeUIInfo eears_new_subtree[] = {
	GNOMEUIINFO_MENU_NEW_ITEM(N_("From clipboard"), N_("Create a new file from the clipboard"),
				  eears_new_from_selection_cb, NULL),
	GNOMEUIINFO_MENU_NEW_ITEM(N_("Record sample"), N_("Record a new sample"),
				  eears_record_cb, NULL),
	GNOMEUIINFO_END
};

enum {
	FILE_MENU_NEW,
	FILE_MENU_OPEN,
	FILE_MENU_SAVE,
	FILE_MENU_SAVE_AS,
        FILE_MENU_SEP1,
	FILE_MENU_CLOSE,
        FILE_MENU_SEP2,
	FILE_MENU_PREF,
        FILE_MENU_SEP3,
	FILE_MENU_EXIT
};

static GnomeUIInfo eears_file_menu[] = {
	GNOMEUIINFO_MENU_NEW_SUBTREE(eears_new_subtree),
	GNOMEUIINFO_MENU_OPEN_ITEM(eears_open_file_cb, NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM(eears_save_file_cb, NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM(eears_save_as_file_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_ITEM(eears_close_file_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(eears_preferences_cb, NULL),
/* plugins menu item */
	{ GNOME_APP_UI_ITEM, N_("Plugins..."), N_("Open the active plugin list"),
          (gpointer)eears_plugins_cb, NULL, NULL,
          GNOME_APP_PIXMAP_NONE, NULL,
          0, (GdkModifierType) 0, NULL },
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM(eears_quit, NULL),
	GNOMEUIINFO_END
};

enum {
	EDIT_MENU_UNDO,
	EDIT_MENU_REDO,
	EDIT_MENU_SEPARATOR1,
	EDIT_MENU_CUT,
	EDIT_MENU_COPY,
	EDIT_MENU_PASTE,
	EDIT_MENU_CROP,
	EDIT_MENU_SEPARATOR2,
	EDIT_MENU_SELECT_ALL,
	EDIT_MENU_CLEAR
};

static GnomeUIInfo eears_edit_menu[] = {
	GNOMEUIINFO_MENU_UNDO_ITEM(eears_undo_cb, NULL),
	GNOMEUIINFO_MENU_REDO_ITEM(eears_redo_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CUT_ITEM(eears_cut_cb, NULL),
	GNOMEUIINFO_MENU_COPY_ITEM(eears_copy_cb, NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM(eears_paste_cb, NULL),
	{ GNOME_APP_UI_ITEM_CONFIGURABLE, N_("_Crop"), N_("Crops the current selection"),
		(gpointer) eears_crop_cb, (gpointer) (NULL), NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 
		0, (GdkModifierType) 0, NULL }, 
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_SELECT_ALL_ITEM (eears_select_all_cb, NULL),
	GNOMEUIINFO_MENU_CLEAR_ITEM (eears_clear_cb, NULL),
	GNOMEUIINFO_END
};
GnomeUIInfo eears_empty_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo eears_help_menu[] = {
        GNOMEUIINFO_MENU_ABOUT_ITEM(eears_about, NULL),
        GNOMEUIINFO_END
};

enum {
	FILE_MENU,
	EDIT_MENU,
	VIEW_MENU,
	HELP_MENU
};

static GnomeUIInfo eears_main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(eears_file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE(eears_edit_menu),
	GNOMEUIINFO_MENU_VIEW_TREE(eears_empty_menu),
	GNOMEUIINFO_MENU_HELP_TREE(eears_help_menu),
	GNOMEUIINFO_END
};

GtkType
eears_get_type (void)
{
	static GtkType type = 0;

	if (!type) 
	{
		static const GtkTypeInfo type_info =
		{
			"EEars",
			sizeof (EEars),
			sizeof (EEarsClass),
			(GtkClassInitFunc) eears_class_init,
			(GtkObjectInitFunc) eears_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			NULL
		};
		type = gtk_type_unique (gtk_object_get_type (), &type_info);
	}

	return type;
}

void
eears_class_init (EEarsClass *klass)
{
	GtkObjectClass *object_class;
	object_class = (GtkObjectClass *) klass;
	return;
}


void
eears_init (EEars *ee)
{
	ee->mc_plugins = NULL;

	ee->vcm = NULL;
	ee->ssm = NULL;
	ee->csm = NULL;
	ee->fsm = NULL;
	ee->adm = NULL;
	ee->cbm = NULL;
	ee->udm = NULL;
	ee->mpm = NULL;
	ee->mrm = NULL;

	ee->mad_players = NULL;

	ee->mdi = GNOME_MDI(gnome_mdi_new("EEars", "Electric Ears"));
	gnome_mdi_set_menubar_template(ee->mdi, eears_main_menu);
	gnome_mdi_set_toolbar_template(ee->mdi, eears_toolbar);

	gnome_mdi_set_child_menu_path(ee->mdi, _("File"));
	gnome_mdi_set_child_list_path(ee->mdi, GNOME_MENU_VIEW_PATH);

	gtk_signal_connect(GTK_OBJECT(ee->mdi), "destroy",
				GTK_SIGNAL_FUNC(cleanup_cb), NULL);
	gtk_signal_connect(GTK_OBJECT(ee->mdi), "remove_child",
				GTK_SIGNAL_FUNC(remove_child_handler), NULL);
	gtk_signal_connect(GTK_OBJECT(ee->mdi), "app_created",
				GTK_SIGNAL_FUNC(app_created_handler), NULL);
	
	return;
}

GtkObject *
eears_new (gboolean restored)
{
	GString *temp_path;
	gboolean restart_ok;
	eears = gtk_type_new(EEARS_TYPE);

	eears->plugins = plugins_init();
	hook_methods();
	plugins_configure_all(eears->plugins);

	if (eears->csm) {
		GList *tmp;
		gchar *Players = "\0";
		eears->mc_plugins = eears-> csm -> create( "Electric Ears", _("Plugins Options"), _("You may change here the default startup options for the plugins."));
		
		tmp = g_list_first(eears->plugins);
		while (tmp) {
			PluginData *plugin;
			plugin = (PluginData *) tmp->data;
			if (strcmp(plugin->ident->type,"audiodata_player")==0) {
				Players = g_strdup_printf("%s%s\n", Players, plugin->title);
			}
			eears->mad_players = g_list_append(eears->mad_players, plugin);
			tmp = tmp->next;
		}
		eears->csm->register_item(eears->mc_plugins, "selection", "OutputPlugin=Default AudioData Player", Players, _("Default Output Plugin"), _("Here you can customize the default plugin for playing samples"), eears_plugin_player_changed_cb, NULL);
		}
	
	if (restored)
	{
		restart_ok = gnome_mdi_restore_state (eears->mdi, "MDI Session", NULL);
	} else {
		gnome_mdi_open_toplevel(eears->mdi); 
	}

	// register configuration values
	eears -> mc_paths = eears-> csm -> create( "General", _("General Options"), _("Here you can choose general options for Electric Ears. These options will not take effect until you restart Electric Ears."));

	eears -> csm -> register_item( eears -> mc_paths, "file", "mixer_program=gmix", NULL,
					_("The path to the\nmixer program"), 
					NULL,
				       NULL, NULL );


	temp_path = g_string_new("temp_file_path=");
	if (getenv("TEMP"))
		temp_path = g_string_append(temp_path, getenv("TEMP"));
	else 
		temp_path = g_string_append(temp_path, "/tmp");
	//g_print("%s\n", temp_path->str);
	
	eears -> csm -> register_item( eears -> mc_paths, "file", temp_path->str, "path",
				       _("The path to the\ntemporary file directory"), 
				       NULL,
				       NULL, NULL );
	g_string_free(temp_path,TRUE);

	eears->adm->set_tmp_dir(eears -> csm -> get_string(eears -> mc_paths, "temp_file_path"));

	return GTK_OBJECT(eears);
}

void
eears_plugin_player_changed_cb (gpointer data)
{	
	gchar *player;
	GList *tmp;
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	player = eears->csm->get_string(eears->mc_plugins, "OutputPlugin");
	tmp = g_list_first (eears->mad_players);
	while (tmp) {
		if (strcmp(((PluginData *)(tmp->data))->title, player)==0)
		{
			eears->mpm->play = plugins_get_method(eears->plugins, "audiodata_player", "play", player);
			eears->mpm->stop = plugins_get_method(eears->plugins, "audiodata_player", "stop", player);
			return;
		}
		tmp = tmp->next;
	}
	g_assert_not_reached();
	eears->mpm = NULL;
	return;
}

void
eears_undo_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	g_assert(eears);
	g_assert(IS_EEARS(eears));
	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->udm);	
	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		eears->udm->undo(mad);
		eears->adm->reset_lastmodify(mad);
		eears->ssm->set_bounds(ms, 0, MAX(eears->adm->get_framecount(mad) - 1, 0 ) );
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void
eears_redo_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	g_assert(eears);
	g_assert(IS_EEARS(eears));
	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->udm);	
	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)(gtk_object_get_user_data(GTK_OBJECT(child)))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		eears->udm->redo(mad);
		eears->adm->reset_lastmodify(mad);
		eears->ssm->set_bounds(ms, 0, MAX ( eears->adm->get_framecount(mad) - 1, 0));
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void
eears_cut_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	MetaMarkers mm;
	glong begin_frame, end_frame;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->cbm);	
	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->vcm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		mm = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mm;
		if (eears->udm)
			eears->udm->new_undo_level(mad);
		
		eears->ssm->get_selection(ms, &begin_frame, &end_frame);
		eears->cbm->cut(mad, begin_frame, end_frame);
		eears->adm->reset_lastmodify(mad);
		eears->ssm->set_bounds(ms, 0, MAX( eears->adm->get_framecount(mad) - 1, 0) );
		eears->ssm->set_selection(ms, -1, -1);
		eears->msm->cut_range(mm, begin_frame, end_frame);
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void
eears_crop_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	MetaMarkers mm;
	glong begin_frame, end_frame;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->cbm);	
	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->vcm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		mm = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mm;
		if (eears->udm)
			eears->udm->new_undo_level(mad);
		
		eears->ssm->get_selection(ms, &begin_frame, &end_frame);
		eears->cbm->cut(mad, 0, begin_frame - 1);
		eears->cbm->cut(mad, end_frame, eears->adm->get_framecount(mad)-1 );
		eears->adm->reset_lastmodify(mad);
		eears->ssm->set_bounds(ms, 0, MAX( eears->adm->get_framecount(mad) - 1, 0) );
		eears->ssm->set_selection(ms, -1, -1);
		eears->msm->cut_range(mm, begin_frame, end_frame);
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void
eears_copy_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	glong begin_frame, end_frame;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->cbm);	
	g_return_if_fail(eears->vcm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		if (eears->udm)
			eears->udm->new_undo_level(mad);
		
		eears->ssm->get_selection(ms, &begin_frame, &end_frame);
		eears->cbm->copy(mad, begin_frame, end_frame);	
	}
	eears_update_menus_and_toolbar();
}

void
eears_paste_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;
	MetaMarkers mm;
	glong begin_frame, end_frame, inserted_frames;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->cbm);	
	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->vcm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		mm = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mm;
		if (eears->udm)
			eears->udm->new_undo_level(mad);
		
		eears->ssm->get_selection(ms, &begin_frame, &end_frame);
		if (begin_frame == -1 && end_frame == -1) {
			/* if there's no selection, use the insertion point */
			begin_frame = end_frame = eears->msm->get_marker(mm, "");
			inserted_frames = 0;
		} else {
			inserted_frames = begin_frame - end_frame;
		}
		inserted_frames += eears->cbm->paste(mad, begin_frame, end_frame);
		eears->adm->reset_lastmodify(mad);
		eears->ssm->set_bounds(ms, 0, MAX(eears->adm->get_framecount(mad) - 1, 0));
		eears->msm->insert_range(mm, begin_frame, inserted_frames);
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void eears_new_from_selection_cb (void)
{
	EEarsViewInfo *evi;
	GnomeMDIGenericChild *child;

	g_assert(eears);
	g_assert(IS_EEARS(eears));
	g_return_if_fail(eears->mrm);

	evi = g_new(EEarsViewInfo, 1);
	
	evi -> mad = eears->cbm->open();
	
	if (evi -> mad == NULL) {
/* 		g_warning("record failed"); */
		g_free( evi );
		return;
	}

	evi -> ms  = eears->ssm->create();
	if (evi -> ms == NULL) {
		g_warning("unable to create selection");
		eears->adm->close( evi -> mad );
		g_free( evi );
		return;
	} 

	evi -> mm  = eears->msm->create();
/* TODO should check */

	if ((child = gnome_mdi_generic_child_new(_("Untitled"))) != NULL)
	{
		if (eears->vcm)
		{
			gnome_mdi_generic_child_set_view_creator(child, eears->vcm->view_creator, NULL);
		}
		gtk_object_set_user_data(GTK_OBJECT(child), evi);
		gnome_mdi_add_child(eears->mdi, GNOME_MDI_CHILD(child));
		gnome_mdi_add_view(eears->mdi, GNOME_MDI_CHILD(child));
	}	
	eears_update_menus_and_toolbar();
}

void eears_select_all_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	MetaSelection ms;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->ssm);
	g_return_if_fail(eears->adm);
	g_return_if_fail(eears->vcm);

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		g_return_if_fail(ms);
		g_return_if_fail(mad);
		
		eears->ssm->set_selection(ms, 0, eears->adm->get_framecount(mad) - 1);
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}

void eears_clear_cb (void)
{
	GnomeMDIChild *child;
	MetaSelection ms;

	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->ssm);
	g_return_if_fail(eears->adm);
	g_return_if_fail(eears->vcm);

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		g_return_if_fail(ms);
		
		eears->ssm->set_selection(ms, -1, -1);
		eears->vcm->cur_view_update();
	}
	eears_update_menus_and_toolbar();
}


void 
eears_preferences_cb (void)
{
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->csm);

	eears->csm->edit_preferences();
}

void 
eears_plugins_cb (void)
{
	GladeXML *xml;
	gchar *filename;
	GtkCList *clistPlugins;
	GList *p;
	
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->csm);

	filename = g_strconcat (PREFIX, 
				"/share/eears/glade/plugins_dialog.glade", 
				NULL);

	xml = glade_xml_new(filename, NULL);

	if (!xml) {
		g_warning("Can't load the interface module.");
		return;
	}

	/* write the plugins */
	clistPlugins = GTK_CLIST(glade_xml_get_widget(xml,"clistPlugins"));
	gtk_clist_freeze(clistPlugins);

	for ( p = eears->plugins; p; p = g_list_next(p)) {
		PluginData *pd;
		PluginIdentifier *pi;
		gchar *columns[6];
		gchar *type = NULL, *version = NULL, *deps = NULL;

		pd = (PluginData*) p->data;
		
		columns[0] = pd->title;
		for (pi = pd->ident; pi->type; pi++) {
			gchar buf[32];
			gchar *temp;
			temp = type;
			type = g_strjoin( ", ",  pi->type, type, NULL);
			g_free(temp);

			temp = version;
			snprintf(buf, 32, "%u.%u",
				 pi->version && 0xffffff00,
				 pi->version && 0xff);
			version = g_strjoin( ", ", buf , version, NULL);
			g_free(temp);
		}
		columns[1] = type;
		columns[2] = version;
		columns[3] = pd->author;
		columns[4] = pd->file;
		for (pi = pd->dependencies; pi->type; pi++) {
			gchar *temp;
			temp = deps;
			deps = g_strjoin( ", ",  pi->type, deps, NULL);
			g_free(temp);
		}
		columns[5] = deps;
		
		gtk_clist_append(clistPlugins, columns);
		g_free(type);
		g_free(version);
		g_free(deps);
	}
	gtk_clist_thaw(clistPlugins);

	glade_xml_signal_autoconnect(xml);
}


void 
eears_open_file_cb (void)
{
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->fsm);	
	g_return_if_fail(eears->adm);	

	eears->fsm->file_selection(eears_open_file);
}

void 
eears_open_mixer_cb (void)
{
	int pid;
	gchar *mixerpath;
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	mixerpath = eears->csm->get_string(eears->mc_paths, "mixer_program");
	gnome_execute_async(NULL, 1, &mixerpath );
}

void 
eears_save_file_cb (void)
{
	GnomeMDIChild *child;
	MetaAudioData mad;
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->adm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		if ( strlen(eears->adm->get_filename(mad)) == 0) {
		        /* the mad does not have a name */
		        g_return_if_fail(eears->fsm);	

			eears->fsm->file_selection(eears_save_as_file);
		} else
		        eears->adm->save(mad);
	}
	eears_update_menus_and_toolbar();
}

void 
eears_save_as_file_cb (void)
{
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->fsm);	
	g_return_if_fail(eears->adm);	

	eears->fsm->file_selection(eears_save_as_file);
}

void 
eears_close_file_cb (void)
{
	GnomeMDIChild *child;
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->fsm);	
	g_return_if_fail(eears->adm);	

	if ((child = gnome_mdi_get_active_child(eears->mdi))==NULL)
	{	
	  /* if no child do nothing */
		return;
	}
	if (!gnome_mdi_remove_child(GNOME_MDI(eears->mdi), child, FALSE))
	{
	        /* g_error("removing child"); */
		return;
	}
	eears_update_menus_and_toolbar();
}

void eears_play_cb (void)
{
	MetaAudioData mad;
	MetaSelection ms;
	MetaMarkers mm;
	GnomeMDIChild *child;
	glong begin, end;
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->adm);	
	g_return_if_fail(eears->mpm);	

	if (eears->csm) {
		gchar *player;
		GList *tmp;

		player = eears->csm->get_string(eears->mc_plugins, "OutputPlugin");
		tmp = g_list_first (eears->mad_players);
		while (tmp) {
			if (strcmp(((PluginData *)(tmp->data))->title, player)==0)
			{
				eears->mpm->play = plugins_get_method(eears->plugins, "audiodata_player", "play", player);
				eears->mpm->stop = plugins_get_method(eears->plugins, "audiodata_player", "stop", player);
				break;
			}
			tmp = tmp->next;
		}
	}

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
		mm = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mm;
		if (eears->ssm) {
			eears->ssm->get_selection(ms, &begin, &end);
			g_assert(begin <= end);
			g_assert(end <= (eears->adm->get_framecount(mad)));
			if ((begin<0) && (end<0)) {
				/* start at the cursor position */
				begin = eears->msm->get_marker(mm, "");
				end = eears->adm->get_framecount(mad);
			}
			g_assert(begin>=0);
		} else {
			begin = 0; end = eears->adm->get_framecount(mad); 
		}

		eears->mpm->play(mad, begin, end);
	}
	eears_update_menus_and_toolbar();
}

void eears_record_cb (void)
{
	EEarsViewInfo *evi;
	GnomeMDIGenericChild *child;

	g_assert(eears);
	g_assert(IS_EEARS(eears));
	g_return_if_fail(eears->mrm);

	evi = g_new(EEarsViewInfo, 1);
	
	evi -> mad = eears->mrm->produce();
	
	if (evi -> mad == NULL) {
/* 		g_warning("record failed"); */
/* 		GtkWidget *dialog; */
/* 		gchar *message = g_strdup_printf(_("Error recording sample"), file); */
/* 		dialog = gnome_error_dialog(message); */
		g_free( evi );
		return;
	}

	evi -> ms  = eears->ssm->create();
	if (evi -> ms == NULL) {
		g_warning("unable to create selection");
		eears->adm->close( evi -> mad );
		g_free( evi );
		return;
	} 
	
	eears->ssm->set_bounds( evi->ms, 0, MAX( eears->adm->get_framecount(evi -> mad) - 1, 0));

	evi -> mm  = eears->msm->create();
/* TODO should check */

	if ((child = gnome_mdi_generic_child_new(_("Untitled"))) != NULL)
	{
		if (eears->vcm)
		{
			gnome_mdi_generic_child_set_view_creator(child, eears->vcm->view_creator, NULL);
		}
		gtk_object_set_user_data(GTK_OBJECT(child), evi);
		gnome_mdi_add_child(eears->mdi, GNOME_MDI_CHILD(child));
		gnome_mdi_add_view(eears->mdi, GNOME_MDI_CHILD(child));
	}	
	eears_update_menus_and_toolbar();
}

void eears_stop_cb (void)
{
	g_assert(eears);
	g_assert(IS_EEARS(eears));
	g_return_if_fail(eears->mpm);
	eears->mpm->stop();
	eears_update_menus_and_toolbar();
}

MetaAudioData eears_open_file (gchar *file, gint format)
{
	EEarsViewInfo *evi;
	GnomeMDIGenericChild *child;
	g_assert(file);
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_val_if_fail(eears->adm, NULL);

	evi = g_new(EEarsViewInfo, 1);
	
	evi -> mad = eears->adm->open(file);
	
	if (evi -> mad == NULL) {
		GtkWidget *dialog;
		gchar *message = g_strdup_printf(_("Unable to open file\n%s"), file);
		dialog = gnome_error_dialog(message);
		g_free(message);
		g_free( evi );
		return NULL;
	}

	evi -> ms  = eears->ssm->create();
	evi -> mm  = eears->msm->create();

	if (evi -> ms == NULL) {
		g_warning("unable to create selection");
		eears->adm->close( evi -> mad );
		g_free( evi );
		return NULL;
	} 

	eears->ssm->set_bounds( evi->ms, 0, MAX( eears->adm->get_framecount(evi -> mad) - 1, 0));

	if((child = gnome_mdi_generic_child_new(eears->adm->get_filename(evi -> mad))) != NULL)
	{
		if (eears->vcm)
		{
			gnome_mdi_generic_child_set_view_creator(child, eears->vcm->view_creator, NULL);
		}
		gtk_object_set_user_data(GTK_OBJECT(child), evi);
		gnome_mdi_add_child(eears->mdi, GNOME_MDI_CHILD(child));
		gnome_mdi_add_view(eears->mdi, GNOME_MDI_CHILD(child));
	}
	eears_update_menus_and_toolbar();
	return evi->mad;
}

void eears_save_as_file (gchar *file, gint format)
{
	MetaAudioData mad;
	GnomeMDIChild *child;
	g_assert(file);
	g_assert(eears);
	g_assert(IS_EEARS(eears));

	g_return_if_fail(eears->adm);

	if ((child = gnome_mdi_get_active_child(eears->mdi))!=NULL)
	{
		mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
		eears->adm->save_as(mad, file, format);
		gnome_mdi_child_set_name( child, eears->adm->get_filename(mad));
	}
	eears_update_menus_and_toolbar();
}

void
eears_about (void)
{
	static GtkWidget *about = NULL;
	gchar *logo_filename;
	
	const gchar *authors [] = {
		"A. Bosio <st970751@educ.di.unito.it>",
		"Giovanni Iachello <g.iachello@iol.it>",
		NULL
	};

	logo_filename = g_strconcat (PREFIX, "/share/pixmaps/eears/eears_icon.png", NULL);
	
	//g_print(logo_filename);

	about = gnome_about_new (
			PACKAGE, VERSION,
			"(C) 1999-2000 A. Bosio and Giovanni Iachello",
			authors,
			_("A simple wave player and editor.\nhttp://www.gdev.net/~torello/news.html"),
			logo_filename);
	gtk_window_set_modal (GTK_WINDOW (about), TRUE);
	gtk_widget_show (about);

	return;	
}

void
eears_quit(void)
{
	g_return_if_fail(IS_EEARS(eears));
	if (gnome_mdi_remove_all(GNOME_MDI(eears->mdi), FALSE))
	{
		gtk_object_destroy(GTK_OBJECT(eears->mdi));
	}
}

void eears_update_menus_and_toolbar_for_app(GnomeApp *app)
{
	GnomeUIInfo *ui_info_menubar;
	GnomeUIInfo *ui_info_menu;
	GnomeUIInfo *ui_info_toolbar;
	GnomeMDIChild *child;
	glong tmp1, tmp2;
	gboolean active = FALSE, selection = FALSE;
	
	ui_info_menubar = gnome_mdi_get_menubar_info(app);

	gtk_menu_item_right_justify(GTK_MENU_ITEM(ui_info_menubar[HELP_MENU].widget));
	
	ui_info_toolbar = gnome_mdi_get_toolbar_info(app);

	ui_info_menu = (GnomeUIInfo *) ui_info_menubar[FILE_MENU].moreinfo;

	/* get information related to menu */
	
	if (( child = gnome_mdi_get_active_child(eears->mdi))) {
		active = TRUE;
		if (eears->ssm) {
			eears->ssm->get_selection( ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms, 
					   &tmp1, &tmp2);
			if (tmp1 != -1 || tmp2 != -1) selection = TRUE;
		}
	}

	if (eears->adm)
	{
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_OPEN].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_OPEN].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_OPEN].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_OPEN].widget, FALSE);
	}

	if (eears->adm && active) {
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_SAVE].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_SAVE_AS].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_CLOSE].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_SAVE].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_SAVE_AS].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_SAVE].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_SAVE_AS].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[FILE_MENU_CLOSE].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_SAVE].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_toolbar[TOOLBAR_SAVE_AS].widget, FALSE);
	}

	if (eears->mpm && active)
	{
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_PLAY].widget, TRUE);
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_STOP].widget, TRUE);
	} else {
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_PLAY].widget, FALSE);
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_STOP].widget, FALSE);
	}

	if (eears->mrm)
	{
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_RECORD].widget, TRUE);
	} else {
		gtk_widget_set_sensitive(ui_info_toolbar[TOOLBAR_RECORD].widget, FALSE);
	}
	
	ui_info_menu = (GnomeUIInfo *) ui_info_menubar[EDIT_MENU].moreinfo;

	if (eears->cbm && active)
	{
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CUT].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_COPY].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_PASTE].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CROP].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_PASTE].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_COPY].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CUT].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CROP].widget, FALSE);
	} /* eears->cbm */

	if (eears->ssm && active)
	{
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CLEAR].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_CLEAR].widget, FALSE);
	} /* eears->ssm */

	if (eears->ssm && active)
	{
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_SELECT_ALL].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_SELECT_ALL].widget, FALSE);
	} /* eears->ssm */

	if (eears->cbm && active)
	{
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_UNDO].widget, TRUE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_REDO].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_UNDO].widget, FALSE);
		gtk_widget_set_sensitive (ui_info_menu[EDIT_MENU_REDO].widget, FALSE);
	} /* eears->ssm */
}

void eears_update_menus_and_toolbar(void)
{
	GList *app_node;
	GnomeApp *app;

	app_node = eears->mdi->windows;
	
	while (app_node)
	{
		app = GNOME_APP(app_node->data);

		eears_update_menus_and_toolbar_for_app(app);

		app_node = app_node->next;
	}
}

static void 
cleanup_cb(void) 
{
	/* if (eears->csm) eears->csm->save(eears->mc_plugins); */
	plugins_cleanup(eears->plugins);
	gtk_main_quit();
}

static void app_drop_cb(GtkWidget *widget, GdkDragContext *context,
		gint x, gint y, GtkSelectionData *selection_data,
		guint info, guint time, gpointer data)
{
	GList *names, *list;

	g_return_if_fail(eears->adm);
	
	switch (info) {
		case TARGET_URI_LIST:
			list = names = gnome_uri_list_extract_filenames (selection_data->data);
			while (names) {
				/* TODO: handle errors */
				eears_open_file(names->data, AF_FILE_UNKNOWN);
				names = names->next;
			}
			gnome_uri_list_free_strings (list);
			break;
		default:
	}
}


void app_created_handler(GnomeMDI *mdi, GnomeApp *app) 
{
	GtkWidget *appbar;

	static GtkTargetEntry drop_types [] = {
		{ "text/uri-list", 0, TARGET_URI_LIST}
	};
	static gint n_drop_types = sizeof (drop_types) / sizeof(drop_types[0]);

	gtk_drag_dest_set (GTK_WIDGET (app),
			GTK_DEST_DEFAULT_MOTION |
			GTK_DEST_DEFAULT_HIGHLIGHT |
			GTK_DEST_DEFAULT_DROP,
			drop_types, n_drop_types,
			GDK_ACTION_COPY);
	gtk_signal_connect (GTK_OBJECT (app), "drag_data_received",
			GTK_SIGNAL_FUNC(app_drop_cb), NULL);

	
	eears_update_menus_and_toolbar_for_app(app);

	appbar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar(app, appbar);
	gnome_appbar_set_default(GNOME_APPBAR(appbar), _("Welcome in Electric Ears!"));
	gnome_app_install_menu_hints(app, gnome_mdi_get_menubar_info(app));
}



void reply_handler(gint reply, gpointer data) 
{
	gint *int_data = (gint *)data;
	*int_data = reply;
	gtk_main_quit();
}

gint
remove_child_handler(GnomeMDI *mdi, GnomeMDIChild *child)
{
	gchar *question;
	const gchar *filename;
	gint reply;
	MetaAudioData mad = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> mad;
	MetaSelection ms = ((EEarsViewInfo*)gtk_object_get_user_data(GTK_OBJECT(child))) -> ms;
	
	filename = eears->adm->get_filename(mad);
	if ( strlen(filename) == 0 )
		filename = _("Untitled");
	question = g_strdup_printf(_("Do you really want to end editing %s?"), filename);
	
	gnome_app_question_modal(gnome_mdi_get_active_window(mdi), question,
						reply_handler, &reply);

	g_free(question);
	gtk_main();

	if (reply == GNOME_YES)
	{
		eears->adm->close(mad);
		eears->ssm->free(ms);
		return TRUE;
	}
	return FALSE;
}


void hook_methods()
{
	if (eears->adm) g_free(eears->adm);
	eears->adm = g_malloc(sizeof(AudioDataMethods));
	eears->adm->create = plugins_get_method(eears->plugins, "audiodata_system", "new", NULL);
	eears->adm->open = plugins_get_method(eears->plugins, "audiodata_system", "open", NULL);
	eears->adm->close = plugins_get_method(eears->plugins, "audiodata_system", "close", NULL);
	eears->adm->clone = plugins_get_method(eears->plugins, "audiodata_system", "clone", NULL);
	eears->adm->read = plugins_get_method(eears->plugins, "audiodata_system", "read", NULL);
	eears->adm->write = plugins_get_method(eears->plugins, "audiodata_system", "write", NULL);
	eears->adm->save = plugins_get_method(eears->plugins, "audiodata_system", "save", NULL);
	eears->adm->save_as = plugins_get_method(eears->plugins, "audiodata_system", "save_as", NULL);
	eears->adm->seek = plugins_get_method(eears->plugins, "audiodata_system", "seek", NULL);
//	eears->adm->rewind = plugins_get_method(eears->plugins, "audiodata_system", "rewind", NULL);
	eears->adm->free = plugins_get_method(eears->plugins, "audiodata_system", "free", NULL);
//	eears->adm->set_filename = plugins_get_method(eears->plugins, "audiodata_system", "set_filename", NULL);
	eears->adm->set_tmp_dir = plugins_get_method(eears->plugins, "audiodata_system", "set_tmp_dir", NULL);
	eears->adm->get_filename = plugins_get_method(eears->plugins, "audiodata_system", "get_filename", NULL);
//	eears->adm->set_rate = plugins_get_method(eears->plugins, "audiodata_system", "set_rate", NULL);
	eears->adm->get_rate = plugins_get_method(eears->plugins, "audiodata_system", "get_rate", NULL);
//	eears->adm->set_bits = plugins_get_method(eears->plugins, "audiodata_system", "set_bits", NULL);
	eears->adm->get_bits = plugins_get_method(eears->plugins, "audiodata_system", "get_bits", NULL);
//	eears->adm->set_channels = plugins_get_method(eears->plugins, "audiodata_system", "set_channels", NULL);
	eears->adm->get_channels = plugins_get_method(eears->plugins, "audiodata_system", "get_channels", NULL);
//	eears->adm->set_framecount = plugins_get_method(eears->plugins, "audiodata_system", "set_framecount", NULL);
	eears->adm->get_framecount = plugins_get_method(eears->plugins, "audiodata_system", "get_framecount", NULL);
	eears->adm->get_framesize = plugins_get_method(eears->plugins, "audiodata_system", "get_framesize", NULL);
	eears->adm->reset_lastmodify = plugins_get_method(eears->plugins, "audiodata_system", "reset_lastmodify", NULL);
	eears->adm->get_lastmodify = plugins_get_method(eears->plugins, "audiodata_system", "get_lastmodify", NULL);

	if (eears->vcm) g_free(eears->vcm);
	eears->vcm = g_malloc(sizeof(ViewCreatorMethods));
	eears->vcm->view_creator = plugins_get_method(eears->plugins, "view_creator", "view_creator", NULL);
	eears->vcm->cur_view_update = plugins_get_method(eears->plugins, "view_creator", "cur_view_update", NULL);

	if (eears->fsm) g_free(eears->fsm);
	eears->fsm = g_malloc(sizeof(FileSelectionMethods));
	eears->fsm->file_selection = plugins_get_method(eears->plugins, "file_selection", "file_selection", NULL);

	if (eears->mpm) g_free(eears->mpm);
	eears->mpm = g_malloc(sizeof(MadPlayerMethods));
	eears->mpm->play = plugins_get_method(eears->plugins, "audiodata_player", "play", NULL);
	eears->mpm->stop = plugins_get_method(eears->plugins, "audiodata_player", "stop", NULL);

	if (eears->mrm) g_free(eears->mrm);
	eears->mrm = g_malloc(sizeof(MadProducerMethods));
	eears->mrm->produce = plugins_get_method(eears->plugins, "audiodata_producer", "produce", NULL);

	if (eears->ssm) g_free(eears->ssm);
	eears->ssm = g_malloc(sizeof(SelectionSystemMethods));
	eears->ssm->set_selection = plugins_get_method(eears->plugins, "selection_system", "set_selection", NULL);
	eears->ssm->get_selection = plugins_get_method(eears->plugins, "selection_system", "get_selection", NULL);
	eears->ssm->set_bounds = plugins_get_method(eears->plugins, "selection_system", "set_bounds", NULL);
	eears->ssm->get_bounds = plugins_get_method(eears->plugins, "selection_system", "get_bounds", NULL);
	eears->ssm->create = plugins_get_method(eears->plugins, "selection_system", "new", NULL);
	eears->ssm->free = plugins_get_method(eears->plugins, "selection_system", "free", NULL);

	if (eears->msm) g_free(eears->msm);
	eears->msm = g_malloc(sizeof(MarkersSystemMethods));
	eears->msm->set_unique_marker = plugins_get_method(eears->plugins, "markers_system", "set_unique_marker", NULL);
	eears->msm->set_marker = plugins_get_method(eears->plugins, "markers_system", "set_marker", NULL);
	eears->msm->get_marker = plugins_get_method(eears->plugins, "markers_system", "get_marker", NULL);
	eears->msm->remove_marker = plugins_get_method(eears->plugins, "markers_system", "remove_marker", NULL);
	eears->msm->insert_range = plugins_get_method(eears->plugins, "markers_system", "insert_range", NULL);
	eears->msm->cut_range = plugins_get_method(eears->plugins, "markers_system", "cut_range", NULL);
	eears->msm->get_markers_in_range = plugins_get_method(eears->plugins, "markers_system", "get_markers_in_range", NULL);
	eears->msm->create = plugins_get_method(eears->plugins, "markers_system", "new", NULL);
	eears->msm->free = plugins_get_method(eears->plugins, "markers_system", "free", NULL);

	if (eears->csm) g_free(eears->csm);
	eears->csm = g_malloc(sizeof(ConfigurationSystemMethods));
	eears->csm->create = plugins_get_method(eears->plugins, "configuration_system", "new", NULL);
	eears->csm->register_item = plugins_get_method(eears->plugins, "configuration_system", "register_item", NULL);
	eears->csm->get_string = plugins_get_method(eears->plugins, "configuration_system", "get_string", NULL);
	eears->csm->get_int = plugins_get_method(eears->plugins, "configuration_system", "get_int", NULL);
	eears->csm->get_bool = plugins_get_method(eears->plugins, "configuration_system", "get_bool", NULL);
	eears->csm->get_float = plugins_get_method(eears->plugins, "configuration_system", "get_float", NULL);
	eears->csm->get_color = plugins_get_method(eears->plugins, "configuration_system", "get_color", NULL);
	eears->csm->set_string = plugins_get_method(eears->plugins, "configuration_system", "set_string", NULL);
	eears->csm->set_int = plugins_get_method(eears->plugins, "configuration_system", "set_int", NULL);
	eears->csm->set_bool = plugins_get_method(eears->plugins, "configuration_system", "set_bool", NULL);
	eears->csm->set_float = plugins_get_method(eears->plugins, "configuration_system", "set_float", NULL);
	eears->csm->set_color = plugins_get_method(eears->plugins, "configuration_system", "set_color", NULL);
	eears->csm->save = plugins_get_method(eears->plugins, "configuration_system", "save", NULL);
	eears->csm->close = plugins_get_method(eears->plugins, "configuration_system", "close", NULL);
	eears->csm->edit_preferences = plugins_get_method(eears->plugins, "configuration_system", "edit_preferences", NULL);

	if (eears->cbm) g_free(eears->cbm);
	eears->cbm = g_malloc(sizeof(ClipboardMethods));
	eears->cbm->cut = plugins_get_method(eears->plugins, "clipboard_system", "cb_cut", NULL);
	eears->cbm->copy = plugins_get_method(eears->plugins, "clipboard_system", "cb_copy", NULL);
	eears->cbm->paste = plugins_get_method(eears->plugins, "clipboard_system", "cb_paste", NULL);
	eears->cbm->open = plugins_get_method(eears->plugins, "clipboard_system", "cb_open", NULL);

	if (eears->udm) g_free(eears->udm);
	eears->udm = g_malloc(sizeof(UndoMethods));
	eears->udm->undo = plugins_get_method(eears->plugins, "undo_system", "undo", NULL);
	eears->udm->redo = plugins_get_method(eears->plugins, "undo_system", "redo", NULL);
	eears->udm->new_undo_level = plugins_get_method(eears->plugins, "undo_system", "new_undo_level", NULL);
}
