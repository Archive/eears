/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __EEARS_H
#define __EEARS_H

#include "metadefs.h"

/* the following information is associated with each view. */

typedef struct _EEarsViewInfo            EEarsViewInfo;

struct _EEarsViewInfo {
	MetaAudioData mad;
	MetaSelection ms;
	MetaMarkers   mm;
/* MetaMarkerSet mms; */
};


#define EEARS_TYPE		(eears_get_type())
#define EEARS(obj)		(GTK_CHECK_CAST((obj), EEARS_TYPE, EEars))
#define EEARS_CLASS(klass)	(GTK_CHECK_CLASS_CAST((klass), EEARS_TYPE, EEarsClass))
#define IS_EEARS(obj)		(GTK_CHECK_TYPE((obj),EEARS_TYPE))
#define IS_EEARS_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), EEARS_TYPE))

typedef struct _EEars                     EEars;
typedef struct _EEarsClass                EEarsClass;

struct _EEars {
	GtkObject *object;	
	
	GnomeMDI *mdi;

	GList *plugins;

	MetaConfiguration mc_plugins;
 	MetaConfiguration mc_paths; 

	ViewCreatorMethods 	        *vcm;
        SelectionSystemMethods          *ssm;
	FileSelectionMethods	        *fsm;
	AudioDataMethods 	        *adm;
	ClipboardMethods	        *cbm;
	UndoMethods		        *udm;
	MadPlayerMethods       		*mpm;
	MadProducerMethods	        *mrm;
        ConfigurationSystemMethods      *csm;
	MarkersSystemMethods            *msm;
	GList 				*mad_players;
};

struct _EEarsClass {
	GtkObjectClass	parent_class;
};

GtkType         eears_get_type		(void);
GtkObject *	eears_new		(gboolean restored);

void 		eears_about 		(void);
void 		eears_quit 		(void);
MetaAudioData	eears_open_file 	(gchar *file, gint format);
void 		eears_save_as_file 	(gchar *file, gint format);

extern          EEars *eears;

#endif /* __EEARS_H */


