/* Electric Ears
 * Copyright (C) 2000  A.Bosio, G.Iachello and Others
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
 * A lot of code stolen from Gnumeric.
 * Original Author: Tom Dyas (tdyas@romulus.rutgers.edu)
 * Greatly modified by G. Iachello to support named module types and deps.
 */

#include <config.h>
#include <gnome.h>
#include <audiofile.h>
#include <dirent.h>
#include "plugins.h"
#include "metadefs.h"
#include "eears.h"

//extern EEars *eears;

static PluginData*	load_plugin 		(const gchar *modfile);
static GList *		load_plugins_in_dir 	(GList *all_plugins_list, char *directory);
static GList *		load_all_plugins 	(void);
static void		process_plugin_list	(GList *all_plugins_list);
static void             plugin_data_free        (PluginData *pd);
static GList *          check_modules_dependencies      (GList *all_plugins_list);

gboolean hook_plugin_audiodata_system (PluginData *pd, gboolean test_only);
gboolean hook_plugin_view_creator (PluginData *pd, gboolean test_only);
gboolean hook_plugin_file_selection (PluginData *pd, gboolean test_only);
gboolean hook_plugin_mad_player (PluginData *pd, gboolean test_only);
gboolean hook_plugin_mad_producer (PluginData *pd, gboolean test_only);
gboolean hook_plugin_markers_system (PluginData *pd, gboolean test_only);
gboolean hook_plugin_selection_system (PluginData *pd, gboolean test_only);
gboolean hook_plugin_configuration_system (PluginData *pd, gboolean test_only);
gboolean hook_plugin_clipboard (PluginData *pd, gboolean test_only);
gboolean hook_plugin_undo_system (PluginData *pd, gboolean test_only);
void hook_func (GModule* handle, gchar *funcname, gpointer *destination, gchar *file, gboolean *status, gboolean test_only);
GList *hook_all_plugins (GList *all_plugins_list, gboolean test_only);


static PluginData *
load_plugin (const gchar *modfile)
{
	PluginData *pd;
	g_assert(modfile);	

	pd = g_new(PluginData, 1);
	pd->handle = NULL;
	pd->init_plugin = NULL;
	pd->cleanup = NULL;
	pd->configure = NULL;
	pd->ident = NULL;
	pd->dependencies = NULL;
	pd->title = NULL;
	pd->author = NULL;
	pd->file = NULL;

	pd->handle = g_module_open (modfile, 0);
	if (!(pd->handle))
	{
		g_warning("%s", g_module_error());
		g_warning("unable to open module file: %s", modfile);
		g_free(pd);
		return NULL;
	}

	if (!g_module_symbol (pd->handle, "init_plugin", (gpointer *) &pd->init_plugin)){
		g_warning("Plugin must contain init_plugin function: %s", modfile);
		g_module_close(pd->handle);
		g_free (pd);
		return NULL;
	}


	if (!(pd->init_plugin(pd)))
	{
		g_warning("init_plugin returned error: %s", modfile);
		g_module_close(pd->handle);
		g_free (pd);
		return NULL;
	}

	pd->file = g_strdup(modfile);

	return pd;
}

static GList *
load_plugins_in_dir (GList *all_plugins_list, char *directory)
{
	DIR *d;
	struct dirent *e;

	if ((d = opendir (directory)) == NULL)
	{
		g_warning("unable to open directory: %s", directory);
		return all_plugins_list;
	}

	while ((e = readdir (d)) != NULL)
	{
		if (strncmp (e->d_name + strlen (e->d_name) - 3, ".so", 3) == 0)
		{
			PluginData *pd;
			char *plugin_name;

			pd = NULL;

			plugin_name = g_strconcat (directory, e->d_name, NULL);
			if ((pd = load_plugin (plugin_name))==NULL)
			{
				g_warning("Error loading module: %s", plugin_name);
			} else {
				all_plugins_list = g_list_prepend(all_plugins_list, pd);
			}
			g_free (plugin_name);
		}
	}
	closedir (d);

	return all_plugins_list;
}

static GList *
load_all_plugins (void)
{
	GList *all_plugins_list = NULL;
	char *plugin_dir;
	char *home_dir = getenv ("HOME");

	/* Load the user plugins */
	plugin_dir = g_strconcat (home_dir ? home_dir : "", "/.eears/plugins/", NULL);
	all_plugins_list = load_plugins_in_dir (all_plugins_list, plugin_dir);
	g_free (plugin_dir);

	/* Load the system plugins */
	/*	plugin_dir = gnome_unconditional_libdir_file ("eears/plugins/");*/
	plugin_dir = g_strconcat (PREFIX, "/lib/eears/plugins/", NULL);
	all_plugins_list = load_plugins_in_dir (all_plugins_list, plugin_dir);
	g_free (plugin_dir);
	return all_plugins_list;
}



/* Call configure_plugin for each plugin, if it exists */
void plugins_configure_all (GList *all_plugins_list)
{
	GList *tmp = g_list_first(all_plugins_list);
	PluginData *pd = NULL;
	
	while (tmp)
	{
		pd = (PluginData *) tmp->data;
		if (pd->configure){
		        if (!((pd->configure)(pd))) 
			        g_warning("configure_plugin returned error: %s", pd->file);
		}
		tmp = g_list_next(tmp);
	}
}




static GList *
check_modules_dependencies (GList *all_plugins_list)
{
	/* remove all modules with unmet deps */
	gboolean unmet_deps;

	do
	{
		GList *tmp;
		unmet_deps = FALSE;
		/* Find unmet deps. Scan all modules and see if some dep is unmet. 
		 * In this case, delete the module and repeat process, until there
		 * are no more unmet deps.
		 **/
		tmp = g_list_first(all_plugins_list);
		while (tmp)
		{
			PluginIdentifier *i;
			PluginData *pd = (PluginData *) tmp->data;

			
			for ( i = pd->dependencies; i->type; i++) {
				PluginIdentifier *j;
				GList *scan = g_list_first(all_plugins_list);
				unmet_deps = TRUE;
				while (scan) {
					for ( j = ((PluginData *)scan->data)->ident; j->type; j++)
					if (strcmp(j->type, i->type)==0 && 
					    j->version >= i->version)
						unmet_deps = FALSE;
					scan = scan->next;
				}
			}
			if (unmet_deps) { 
				all_plugins_list = g_list_remove(all_plugins_list, pd);
				plugin_data_free(pd);
				tmp = g_list_first(all_plugins_list);
			} else {
				tmp = tmp->next;
			}
		}
		
	} while (unmet_deps);

	return all_plugins_list;
}

static void
process_plugin_list(GList *all_plugins_list)
{
	all_plugins_list = check_modules_dependencies(all_plugins_list);
}

GList *
plugins_init(void)
{
	GList *all_plugins_list = NULL;
	if (!g_module_supported())
	{
		g_error(_("EEars needs dynamic module loading support"));
		return NULL;
	}
	all_plugins_list = load_all_plugins();
	process_plugin_list(all_plugins_list);
	return all_plugins_list;
}

static void 
plugin_data_free(PluginData *pd)
{
	g_assert(pd);
	if (pd->cleanup) pd->cleanup(pd);
	g_module_close(pd->handle);
	g_free(pd->file);
	g_free(pd);
}

void
plugins_cleanup(GList *all_plugins_list)
{
	GList *tmp = NULL;	
	tmp = g_list_first(all_plugins_list);
	while (tmp)
	{
		plugin_data_free(tmp->data);
		tmp = g_list_remove_link(tmp, tmp);
	}
}

/**
 * plugins_get_method:
 * @all_plugins_list: the list of all plugins
 * @type: the type of plugin from which to get the method
 * @methodname: the name of the method
 * @instance: the name of the plugin for disambiguating
 *
 */
gpointer* 
plugins_get_method(GList *all_plugins_list, gchar* type, gchar* methodname, gchar* instance)
{
	if (instance==NULL) {
		GList *tmp;
		tmp = g_list_first(all_plugins_list);
		
		while (tmp) {
			PluginIdentifier *i;
			PluginData *pd = (PluginData *) tmp->data;
			
			for ( i = pd->ident; i->type; i++) {
				if (strcmp(i->type, type)==0) {
					PluginGate* g;
					for ( g = pd->gates; g->name; g++) {
						if (strcmp(g->name, methodname)==0) 
							return g->func;
					}
				}
			}
			tmp = tmp->next;
		}
	} else {
		GList *tmp;
		tmp = g_list_first(all_plugins_list);
		
		while (tmp) {
			PluginData *pd = (PluginData *) tmp->data;
			
			if (strcmp(pd->title, instance)==0) {
				PluginGate* g;
				for ( g = pd->gates; g->name; g++) {
					if (strcmp(g->name, methodname)==0) 
						return g->func;
				}
			}
			tmp = tmp->next;
		}

	}
	g_warning("Method %s %s not found",  type, methodname );
	return NULL;
}
