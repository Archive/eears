/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __METADEFS_H
#define __METADEFS_H
#include "metaaudiodata.h"
#include "metaaudiodata_player.h"
#include "metaaudiodata_producer.h"
#include "metaclipboard.h"
#include "metafileselection.h"
#include "metaselection_system.h"
#include "metamarkers_system.h"
#include "metaconfiguration_system.h"
#include "metaundo_system.h"
#include "metaview_creator.h"
#endif /* __METADEFS_H */
