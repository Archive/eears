/* Electric Ears
 * Copyright (C) 1999 A.Bosio, G.Iachello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *             
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *                         
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __META_SELECTION_SYSTEM_H_
#define __META_SELECTION_SYSTEM_H_

typedef gpointer MetaSelection; 

typedef struct _SelectionSystemMethods SelectionSystemMethods;

struct _SelectionSystemMethods {
	MetaSelection   (*create)                  (void);
	void            (*free)                 (MetaSelection);

	void            (*get_selection)        (MetaSelection, glong *, glong *);
	void            (*set_selection)        (MetaSelection, glong, glong);
/* this function forces the selection to stay in the defined bounds */
	void            (*set_bounds)           (MetaSelection, glong, glong);
	void            (*get_bounds)           (MetaSelection, glong*, glong*);
};

#endif /* __META_SELECTION_SYSTEM_H_ */

